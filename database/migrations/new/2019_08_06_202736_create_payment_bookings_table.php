<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('currency_code')->nullable();
            $table->text('payment_reference')->nullable();
            $table->string('amount');
            $table->unsignedInteger('user_id')->nullable();
            $table->date('datePaid')->nullable();
            $table->date('expirationDate')->nullable();
            $table->enum('booking_type', ['7 Days', '14 Days', '30 Days'])->nullable();
            $table->boolean('isPaid')->default(false);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_bookings');
    }
}
