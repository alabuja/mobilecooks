<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('booking_id')->nullable();
            $table->string('name');
            $table->string('phone_number');
            $table->string('email_address');
            $table->string('location');
            $table->string('chef_type');
            $table->string('meal_type');
            $table->text('description')->nullable();
            $table->integer('number_of_person')->nullable();
            $table->string('engagement_type');
            $table->string('package_type');
            $table->boolean('has_paid')->default(false);
            $table->boolean('is_matched')->default(false);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
