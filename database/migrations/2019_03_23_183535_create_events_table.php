<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->string('name');
            $table->string('phone_number');
            $table->string('email');
            $table->string('event_type');
            $table->string('budget');
            $table->string('date_of_event');
            $table->string('number_of_person');
            $table->string('decoration_service');
            $table->text('decoration_description');
            $table->string('mobility_service');
            $table->text('mobility_description');
            $table->string('feeding_service');
            $table->text('feeding_description');
            $table->string('location_service');
            $table->text('location_description');
            $table->string('event_location_service');
            $table->text('event_location_description');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
