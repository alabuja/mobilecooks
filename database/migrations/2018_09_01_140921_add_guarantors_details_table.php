<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGuarantorsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chefs', function (Blueprint $table) {
            $table->string('guarantor1_address')->after('guarantor1_phone_number')->nullable();
            $table->string('guarantor1_occupation')->after('guarantor1_address')->nullable();
            $table->string('guarantor2_address')->after('guarantor2_phone_number')->nullable();
            $table->string('guarantor2_occupation')->after('guarantor2_address')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chefs', function (Blueprint $table) {
            //
        });
    }
}
