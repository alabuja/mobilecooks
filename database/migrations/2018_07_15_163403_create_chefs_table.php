<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChefsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chefs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('slug');
            $table->string('address')->nullable();
            $table->string('email')->unique();
            $table->string('phone_number')->nullable();
            $table->string('state_residence')->nullable();
            $table->string('state_origin')->nullable();
            $table->string('nationality')->nullable();
            $table->string('guarantor1_name')->nullable();
            $table->string('guarantor1_phone_number')->nullable();
            $table->string('guarantor2_name')->nullable();
            $table->string('guarantor2_phone_number')->nullable();
            $table->enum('meal_type', ['Domestic Meals', 'Intercontinental Meals', 'Both (Intercontinental & Domestic Meals)'])->nullable();
            $table->text('specialty')->nullable();
            $table->string('client_name')->nullable();
            $table->string('client_phone_number')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->integer('number_of_years')->nullable();
            $table->text('image_url');
            $table->string('bvn')->unique()->nullable();
            $table->string('password');
            $table->boolean('verified')->default(0);
            $table->decimal('review_avg', 5, 2)->default(5.00);
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chefs');
    }
}
