<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('admins')->insert([
            'username' => 'admin',
            'email' => 'daniel@gmail.com',
            'password' => bcrypt('password'),
            'avatar_url' => 'https://res.cloudinary.com/dhcuadgjz/image/upload/v1533846074/pz44mewa3kpvfaifjgfq.png',
        ]);

        $states = [
          [ 'name'  => 'Abia',],
          [ 'name'  => 'Adamawa',],
          [ 'name'  => 'Akwa Ibom',],
          [ 'name'  => 'Anambra',],
          [ 'name'  => 'Bauchi',],
          [ 'name'  => 'Bayelsa',],
          [ 'name'  => 'Benue',],
          [ 'name'  => 'Borno',],
          [ 'name'  => 'Cross River',],
          [ 'name'  => 'Delta',],
          [ 'name'  => 'Ebonyi',],
          [ 'name'  => 'Edo',],
          [ 'name'  => 'Ekiti',],
          [ 'name'  => 'Enugu',],
          [ 'name'  => 'Abuja',],
          [ 'name'  => 'Gombe',],
          [ 'name'  => 'Imo',],
          [ 'name'  => 'Jigawa',],
          [ 'name'  => 'Kaduna',],
          [ 'name'  => 'Kano',],
          [ 'name'  => 'Katsina',],
          [ 'name'  => 'Kebbi',],
          [ 'name'  => 'Kogi',],
          [ 'name'  => 'Kwara',],
          [ 'name'  => 'Lagos',],
          [ 'name'  => 'Nasarawa',],
          [ 'name'  => 'Niger',],
          [ 'name'  => 'Ogun',],
          [ 'name'  => 'Ondo',],
          [ 'name'  => 'Osun',],
          [ 'name'  => 'Oyo',],
          [ 'name'  => 'Plateau',],
          [ 'name'  => 'Rivers'],
          [ 'name'  => 'Sokoto',],
          [ 'name'  => 'Taraba',],
          [ 'name'  => 'Yobe',],
          [ 'name'  => 'Zamfara',],
        ];
        DB::table('states')->insert($states);

    }
}
