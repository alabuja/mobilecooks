<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentBooking extends Model
{
	use SoftDeletes;
	
    protected $table = "payment_bookings";

    protected $fillable = ['currency_code', 'payment_reference', 'amount', 'user_id', 'datePaid', 'expirationDate', 'booking_type', 'isPaid'];

    
    public function allPaymentsBookings()
    {
    	$payments  	=	self::whereNotNull('payment_reference')->get();

    	return $payments;
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function userPaymentsBooking($userId)
    {
        $payment   =   self::whereNotNull('payment_reference')
                                ->where('user_id', $userId)
                                ->first();

        return $payment;
    }

    public function paymentsBookingCount()
    {
        $count_payments     =   self::whereNotNull('payment_reference')->count();

        return $count_payments;
    }
}
