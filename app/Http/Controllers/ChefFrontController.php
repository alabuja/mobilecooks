<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chef;
use App\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Review;
use App\State;
use Cloudder;
use Illuminate\Pagination\LengthAwarePaginator;

class ChefFrontController extends Controller
{

	public function __construct(Chef $chef, Review $review, Order $order, State $state)
	{
		$this->chef 	=	$chef;
		$this->review 	=	$review;
        $this->order    =   $order;
        $this->state    =   $state;

	}

    public function review()
    {
    	$chef           =   $this->chef::where('id', Auth::guard('chef')->id())->first();
    	$reviews        =   $this->review->chef_reviews(Auth::guard('chef')->id());
    	$paginations    =   $this->review::paginate(10);

    	$total_reviews  =   Review::where('chef_id', Auth::guard('chef')->id())->count();

    	return view("chef.reviews", compact('chef', 'reviews', 'total_reviews', 'paginations'));
    }

    public function unboarding()
    {
        return view('chef.unboarding');
    }

    public function update()
    {
    	return view("chef.update-password");
    }

    public function profile()
    {
        $states     =       $this->state->getStates();
    	return view("chef.profile", compact('states'));
    }

    public function dashboard()
    {
        return view("chef.dashboard");
    }

    public function bookings()
    {
        $orders         =   $this->order->chefOrders();
        $allOrders      =   $this->order->getAllOrders();
        $paginations    =   $this->order::paginate(20);
        return view("chef.bookings", compact('orders', 'allOrders', 'paginations'));
    }

    public function updatePassword(Request $request)
    {
    	$this->validate($request, [
            'old' => 'required',
            'password' => 'required|confirmed',
        ]);

    	$chef = $this->chef->find(Auth::guard('chef')->id());
        $hashedPassword = $chef->password;

 
    	if (Hash::check($request->old, $hashedPassword)) 
    	{
            $chef->fill([
                'password' => Hash::make($request->password)
            ])->save();
 
            $request->session()->flash('success', 'Your password has been changed.');
 
            return back();
        }
 
        $request->session()->flash('failure', 'Your password has not been changed.');
 
        return back();
    }

    public function deliver(Request $request, $id)
    {
        $this->validate($request,[
            'deliver'  => 'required',
        ]);

        $this->order->deliver  =   $request->deliver;


        $updateBookings   =   Order::where('deliver', 0)->where('id', $id)->update(['deliver' => $this->order->deliver]);

        $request->session()->flash('success', 'Order has been marked as delivered.');

        return back();
    }

    public function updateProfile(Request $request)
    {
    	$this->validate($request, [
            'firstname'                         =>  'required',
            'lastname'                          =>  'required',
            'address'                           =>  'required',
            'phone_number'                      =>  'required',
            'state_residence'                   =>  'required',
            'state_origin'                      =>  'required',
            'nationality'                       =>  'required',
            'guarantor1_name'                   =>  'required',
            'guarantor1_phone_number'           =>  'required',
            'guarantor1_address'                =>  'required',
            'guarantor2_name'                   =>  'nullable',
            'guarantor2_phone_number'           =>  'nullable',
            'guarantor2_address'                =>  'nullable',
            'guarantor2_occupation'             =>  'nullable',
            'dish_type'                         =>  'required',
            'specialty'                         =>  'required',
            'client_name'                       =>  'required',
            'client_phone_number'               =>  'required',
            'date_of_birth'                     =>  'required|date',
            'number_of_years'                   =>  'required|numeric',  
            'image'                             =>  'required|mimes:jpeg,jpg,png'
            // 'bvn'                               =>  'required|numeric|digits:11'
        ]);

        $this->chef->firstname                        =   $request->firstname;
        $this->chef->lastname                         =   $request->lastname;
        $this->chef->address                          =   $request->address;
        $this->chef->phone_number                     =   $request->phone_number;
        $this->chef->state_residence                  =   $request->state_residence;
        $this->chef->state_origin                     =   $request->state_origin;
        $this->chef->nationality                      =   $request->nationality;
        $this->chef->guarantor1_name                  =   $request->guarantor1_name;
        $this->chef->guarantor1_phone_number          =   $request->guarantor1_phone_number;
        $this->chef->guarantor1_address               =   $request->guarantor1_address;
        $this->chef->guarantor1_occupation            =   $request->guarantor1_occupation;
        $this->chef->guarantor2_name                  =   $request->guarantor2_name;
        $this->chef->guarantor2_phone_number          =   $request->guarantor2_phone_number;
        $this->chef->guarantor2_address               =   $request->guarantor2_address;
        $this->chef->guarantor2_occupation            =   $request->guarantor2_occupation;
        $this->chef->meal_type                        =   $request->dish_type;
        $this->chef->specialty                        =   $request->specialty;
        $this->chef->client_name             		  =   $request->client_name;
        $this->chef->client_phone_number              =   $request->client_phone_number;
        $this->chef->date_of_birth                    =   $request->date_of_birth;
        $this->chef->number_of_years                  =   $request->number_of_years;
        // $this->chef->bvn                              =   $request->bvn;

        $firstname 		                              =	$this->chef->firstname;

        $chef_image                               =   $request->file('image')->getRealPath();

        Cloudder::upload($chef_image, null);
        list($width, $height) = getimagesize($chef_image);

        $chefUrl = Cloudder::secureShow(Cloudder::getPublicId(), [
            "crop" => "fit", "width" => 125, "height" => 125
        ]);

        $this->chef->image_url                        =   $chefUrl;

        $update   =   $this->chef::where('id', Auth::guard('chef')->user()->id)->update(
        			[ 'firstname'   => $this->chef->firstname, 'lastname' => $this->chef->lastname,
        			  'address'      => $this->chef->address, 
        			  'phone_number' => $this->chef->phone_number, 
        			  'state_residence' => $this->chef->state_residence, 
        			  'state_origin' => $this->chef->state_origin, 
        			  'nationality' => $this->chef->nationality, 
        			  'guarantor1_name' => $this->chef->guarantor1_name, 
        			  'guarantor1_phone_number' => $this->chef->guarantor1_phone_number,
                      'guarantor1_address'      => $this->chef->guarantor1_address,
                      'guarantor1_occupation'   =>  $this->chef->guarantor1_occupation,
        			  'guarantor2_name' => $this->chef->guarantor2_name, 
                      'guarantor2_phone_number' => $this->chef->guarantor2_phone_number,
                      'guarantor2_address' => $this->chef->guarantor2_address,
        			  'guarantor2_occupation' => $this->chef->guarantor2_occupation,
        			  'meal_type' => $this->chef->meal_type, 
        			  'specialty' => $this->chef->specialty, 
        			  'client_name' => $this->chef->client_name, 
        			  'client_phone_number' => $this->chef->client_phone_number, 
        			  'date_of_birth' => $this->chef->date_of_birth, 
        			  'number_of_years' => $this->chef->number_of_years, 
        			  // 'bvn' => $this->chef->bvn, 
        			  'image_url' => $this->chef->image_url
        			]);
        $request->session()->flash('success', 'Your profile has been updated.');

    	return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstname'                         =>  'required',
            'lastname'                          =>  'required',
            'address'                           =>  'required',
            'state_residence'                   =>  'required',
            'state_origin'                      =>  'required',
            'nationality'                       =>  'required',
            'guarantor1_name'                   =>  'required',
            'guarantor1_phone_number'           =>  'required',
            'guarantor1_address'                =>  'required',
            'guarantor1_occupation'             =>  'required',
            'guarantor2_name'                   =>  'nullable',
            'guarantor2_phone_number'           =>  'nullable',
            'guarantor2_address'                =>  'nullable',
            'guarantor2_occupation'             =>  'nullable',
            'dish_type'                         =>  'required',
            'specialty'                         =>  'required',
            'client_name'                       =>  'required',
            'client_phone_number'               =>  'required',
            'date_of_birth'                     =>  'required',
            'number_of_years'                   =>  'required|numeric', 
            'image'                             =>  'required|mimes:jpeg,jpg,png'
            // 'bvn'                               =>  'required|unique:chefs'
        ]);

        $this->chef->firstname                        =   $request->firstname;
        $this->chef->lastname                         =   $request->lastname;
        $this->chef->address                          =   $request->address;
        $this->chef->phone_number                     =   $request->phone_number;
        $this->chef->state_residence                  =   $request->state_residence;
        $this->chef->state_origin                     =   $request->state_origin;
        $this->chef->nationality                      =   $request->nationality;
        $this->chef->guarantor1_name                  =   $request->guarantor1_name;
        $this->chef->guarantor1_phone_number          =   $request->guarantor1_phone_number;
        $this->chef->guarantor1_address               =   $request->guarantor1_address;
        $this->chef->guarantor1_occupation            =   $request->guarantor1_occupation;
        $this->chef->guarantor2_name                  =   $request->guarantor2_name;
        $this->chef->guarantor2_phone_number          =   $request->guarantor2_phone_number;
        $this->chef->guarantor2_address               =   $request->guarantor2_address;
        $this->chef->guarantor2_occupation            =   $request->guarantor2_occupation;
        $this->chef->meal_type                        =   $request->dish_type;
        $this->chef->specialty                        =   $request->specialty;
        $this->chef->client_name                      =   $request->client_name;
        $this->chef->client_phone_number              =   $request->client_phone_number;
        $this->chef->date_of_birth                    =   $request->date_of_birth;
        $this->chef->number_of_years                  =   $request->number_of_years;
        // $this->chef->bvn                              =   $request->bvn;

        $chef_image                                    =   $request->file('image')->getRealPath();

        Cloudder::upload($chef_image, null);
        list($width, $height) = getimagesize($chef_image);

        $chefUrl = Cloudder::secureShow(Cloudder::getPublicId(), [
            "crop" => "fit", "width" => 125, "height" => 125
        ]);
        
        $this->chef->image_url                        =   $chefUrl;

        $update   =   $this->chef::where('id', Auth::guard('chef')->user()->id)->update([
            'firstname' => $this->chef->firstname, 'lastname' => $this->chef->lastname, 
            'address' => $this->chef->address, 
            'phone_number' => $this->chef->phone_number, 
            'state_residence' => $this->chef->state_residence, 
            'state_origin' => $this->chef->state_origin, 
            'nationality' => $this->chef->nationality, 
            'guarantor1_name' => $this->chef->guarantor1_name, 
            'guarantor1_phone_number' => $this->chef->guarantor1_phone_number, 
            'guarantor2_name' => $this->chef->guarantor2_name, 
            'guarantor2_phone_number' => $this->chef->guarantor2_phone_number, 
            'guarantor2_address' => $this->chef->guarantor2_address,
            'guarantor2_occupation' => $this->chef->guarantor2_occupation,
            'meal_type' => $this->chef->meal_type, 
            'specialty' => $this->chef->specialty, 'client_name' => $this->chef->client_name, 
            'client_phone_number' => $this->chef->client_phone_number, 
            'date_of_birth' => $this->chef->date_of_birth, 
            'number_of_years' => $this->chef->number_of_years, 
            // 'bvn' => $this->chef->bvn, 
            'image_url' => $this->chef->image_url
        ]);

        $request->session()->flash('success', 'Congratulations! You have successfully registered as a Mobilecook Professional.');

        return redirect()->intended('chef/unboarding');
    }
}
