<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Cart;
use App\Chef;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
	public function __construct(Cart $cart)
	{
		$this->cart 	=	$cart;
	}

	public function addToCart(Request $request, $chef_id)
	{
		$user_id 		=	Auth::user()->id;

		$cart_item 		=	$this->cart::where('user_id', $user_id)->where('chef_id', $chef_id)->first();

		$chef			= 	Chef::findOrFail($chef_id);	
 
		if(empty($cart_item)){

			$this->cart->user_id 		=	$user_id;
			$this->cart->chef_id		=	$chef->id;
			$this->cart->amount 		=	'150000';

			$this->cart->save();
		}
		else{
			$request->session()->flash('failure', 'Sorry, Chef has already been added by you.');
			return back();
		}

		$request->session()->flash('success', 'Chef Added to Cart');
		return back();
	}

	public function viewCart()
	{
		$cart_items 	=	$this->cart->singleUser(Auth::user()->id);
    	$sum 			=	$this->cart->userSum(Auth::user()->id);
    	$user  			=  User::findOrFail(Auth::user()->id);
		return view('pages.cart', compact('cart_items', 'sum', 'user'));
	}

	public function deleteCart($cart_id)
	{
		$deleteCart   =   Cart::where('id', $cart_id)->delete();

		return back();
	}

    public function order_details()
    {	
    	$cart_items 	=	$this->cart->singleUser(Auth::user()->id);
    	$sum 			=	$this->cart->userSum(Auth::user()->id);
    	$amount 			=	$this->cart->realAmount(Auth::user()->id);
    	$user  			=  User::findOrFail(Auth::user()->id);
    	return view('pages.order_details', compact('user', 'cart_items', 'sum', 'amount'));
    }
}
