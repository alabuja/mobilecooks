<?php

namespace App\Http\Controllers\Chef\Auth;

use App\Chef;
use App\State;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Cloudder;

class ChefRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'chef/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(State $state)
    {
        $this->middleware('guest');

        $this->state    =   $state;
    }


    public function guard()
    {
        return Auth::guard('chef');
    }

    public function showRegistrationForm()
    {
        $states     =       $this->state->getStates();
        return view('chef.auth.register', compact('states'));
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname'                 =>  'required|string|max:255',
            'lastname'                  =>  'required|string|max:255',
            'email'                     =>  'required|string|email|max:255|unique:chefs',
            'password'                  =>  'required|string|min:6',
            'phone_number'              =>  'required|string|unique:chefs',
            'guarantor1_name'           =>  'required|string',
            'guarantor1_phone_number'   =>  'required|string',
            'guarantor2_name'           =>  'required|string',
            'guarantor2_phone_number'   =>  'required|string',
            'state_residence'           =>  'required|string',
            'state_origin'              =>  'required|string',
            'specialty'                 =>  'required',
            'image'                     =>  'required|mimes:jpeg,jpg,png',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $request                             =   request();
        $chef_image                          =   $request->file('image')->getRealPath();

        Cloudder::upload($chef_image, null);
        list($width, $height)                = getimagesize($chef_image);

        $chefUrl = Cloudder::secureShow(Cloudder::getPublicId(), [
            "crop" => "fit", "width" => 125, "height" => 125
        ]);

        //$data['image_url']              =   $chefUrl;
        return Chef::create([
            'firstname'                 =>  $data['firstname'],
            'lastname'                  =>  $data['lastname'],
            'email'                     =>  $data['email'],
            'password'                  =>  Hash::make($data['password']),
            'phone_number'              =>  $data['phone_number'],
            'guarantor1_name'           =>  $data['guarantor1_name'],
            'guarantor2_name'           =>  $data['guarantor2_name'],
            'guarantor1_phone_number'   =>  $data['guarantor1_phone_number'],
            'guarantor2_phone_number'   =>  $data['guarantor2_phone_number'],
            'state_residence'           =>  $data['state_residence'],
            'state_origin'              =>  $data['state_origin'],
            'specialty'                 =>  $data['specialty'],
            'slug'                      =>  str_slug(strtolower($data['lastname']), '-'),
            'image_url'                 =>  $chefUrl,
            'confirm_terms'             =>  1,
        ]);
    }
}
