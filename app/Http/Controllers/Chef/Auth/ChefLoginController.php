<?php

namespace App\Http\Controllers\Chef\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Chef;
use Illuminate\Http\Request;

class ChefLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'chef/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function getLoginForm()
    {
        return view('chef.auth.login');
    }

    public function authenticate(Request $requests)
    {
        $email = $requests->input('email');
        $password = $requests->input('password');

        if(auth()->guard('chef')->attempt(['email' => $email, 'password' => $password], $requests->has('remember') )){
            return redirect()->intended('chef/dashboard');
        }
        
        $requests->session()->flash('failure', 'Invalid Login Credentials');
        return redirect()->intended('login');
    }

    public function getLogout()
    {
        auth()->guard('chef')->logout();
        return redirect()->intended('login');
    }
}
