<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use App\Chef;
use Auth;

class ReviewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request, $id, $slug)
    {

    	$this->validate($request, [
    		'chef_id' 		=> 	'required|numeric',
    		'review_text'	=>	'required',
    		'expertise'		=>	'required|numeric',
    		'punctuality'	=>	'required|numeric',
    		'courtesy'		=>	'required|numeric',
    	]);

        $reviewModel   =    new Review;

    	$reviewModel->chef_id         =  $request->chef_id;
    	$reviewModel->user_id         =  Auth::user()->id;
    	$reviewModel->review_text     =  $request->review_text;
    	$reviewModel->expertise       =  $request->expertise;
    	$reviewModel->punctuality     =  $request->punctuality;
    	$reviewModel->courtesy        =  $request->courtesy;

    	$chef_id                      =   $request->chef_id;

    	$reviewModel->save();

    	$total_average   	          =   $reviewModel->postChefAvgReview($chef_id);

        Chef::where('id', $chef_id)->update(['review_avg' => $total_average]);

        $request->session()->flash('success', 'Your review was Successful. Thank you once again.');

    	return back();

    }
}
