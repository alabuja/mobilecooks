<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;
use App\Chef;

class PackageController extends Controller
{
    public function __construct(Chef $chef)
    {
        $this->chefs    =   $chef;
    }

    public function subscription()
    {
        $results =     [ 
                   'chefs'  =>  $this->chefs->allChef(),
                   'chefs'  => Chef::where('verified', '=', 1)->orderBy('review_avg', 'desc')->paginate(30),
               ];
        return view('pages.subscribe', $results);
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' 				=> 	'required',
    		'amount' 			=>	'required',
    		'number_of_cooks'	=>	'required',
    	]);

    	$packageModel 					=	new Package;

    	$packageModel->name 			= 	$request->name;
    	$packageModel->amount			=	$request->amount;
    	$packageModel->number_of_cooks	=	$request->number_of_cooks;

    	$packageModel->save();

    	return response()->json($packageModel);
    }
}
