<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Chef;
use App\Cart;
use App\Review;
use Cloudder;
use Illuminate\Pagination\LengthAwarePaginator;
use Session;
use Illuminate\Support\Facades\DB;

class ChefController extends Controller
{

    public function __construct(Chef $chefs, Review $review, Cart $cart)
    {
        //$this->middleware('chef');
        $this->chefs     =   $chefs;
        $this->review    =   $review;
        $this->cart      =   $cart;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('chef.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllChef(Request $request)
    {   
        $results =     [ 
                   'chefs'  =>  $this->chefs->allChef(),
                   'chefs'  => Chef::where('verified', '=', 1)->orderBy('review_avg', 'desc')->paginate(30),
               ];


        // $cart_items     =   $this->cart->singleUser(Auth::user()->id);
        // $sum            =   $this->cart->userSum(Auth::user()->id);

        // $paginations    =   Chef::paginate(30);
        //$paginations         =   DB::table('chefs')->paginate(30);

        return view('chef.showall', $results);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show()
    // {
    //     return view('chef.auth.register');
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function chef_rating(Request $request, $rating)
    {
        $chefs          =   Chef::select('*')->where('review_avg', $rating)->orderBy('id', 'desc')->get();

        // $paginations    =   Chef::paginate(30);
        //$paginations         =   DB::table('chefs')->paginate(30);

        return view('chef.showall', compact('chefs'));
    }

    public function chef_details($chef_id, $slug, Request $request)
    {
        $chef           =   $this->chefs::where('id', $chef_id)->first();
        $reviews        =   $this->review->chef_reviews($chef_id);

        $paginations    =   $this->review::paginate(10);

        $total_reviews  =   Review::where('chef_id', $chef_id)->count();
        return view('chef.chef_details', compact('chef', 'reviews', 'total_reviews'));
    }

    public function chefSearch(Request $request)
    {   
        $q        =   $request->input('q');

        $chefs  =  Chef::latest()->search($q)->paginate(30);
        return view("search", compact('chefs', 'q'));
    }

    public function getChef($chefId)
    {
        $chef = $thischefs::where('id', $chefId)
                                ->first();

        if ($chef) {
            $return =   [
                            "status"    =>  200,
                            "response"  =>  $chef
                        ];

            return json_encode($return);
        }
    }
}
