<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\User;
use App\Cart;
use App\Payment;
use App\PaymentBooking;
use App\Jobs\AdminOrder;
use App\Jobs\UserOrder;
use App\Jobs\ChefOrder;
use App\Jobs\AdminPaymentBookingJob;
use App\Jobs\PaymentBookingJob;
use Illuminate\Support\Facades\Auth;
use Paystack;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OrderController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

    public function remove($id)
    {
        $deleteOrder   =   Order::where('id', $id)->delete();

        if($deleteOrder){
         $return   =     [
                             "status"    =>  200,
                             "response"  =>  $deleteOrder
                         ];

         return json_encode($return, true);
        }
    }

    public function thankyou()
    {
        return view('pages.thankyou');
    }

    public function store(Request $request)
    {

        $data = [];

        if($request->hire_type == "payment_booking")
        {
            $this->validate($request, [
                'booking_type'   =>     'required',
                'email'          =>     'required'
            ]);

            $booking_type   =   $request->booking_type;
            $email          =   $request->email;

            $data = [
                'booking_type'  =>  $booking_type,
                'email'         =>  $email
            ];

            $amount = 0;

            if($booking_type == '7 Days')
            {
                $amount = 510000;

            }
            elseif ($booking_type == '14 Days') 
            {
                $amount = 1100000;
            }
            elseif($booking_type == '30 Days')
            {
                $amount = 2100000;
            }
            else
            {
                $amount = 2100000;
            }

            $request->merge(['amount' => $amount]);
        }
        else
        {
            $this->validate($request, [
                'chef_id'            =>     'required',
                'firstname'          =>     'required',
                'lastname'           =>     'required',
                'email'              =>     'required',
            ]);

            $user_id        =   Auth::user()->id;
            $email          =   $request->email;
            $firstname      =   $request->firstname;
            $lastname       =   $request->lastname;
            $chef_id        =   $request->chef_id;

            $data   =  [
                'user_id'           =>  $user_id,
                'chef_id'           =>  $chef_id,
                'lastname'          =>  $lastname,
                'firstname'         =>  $firstname,
                'email'             =>  $email,
            ];
        }

    	
        $request->session()->put('data', $data);
        return Paystack::getAuthorizationUrl()->redirectNow()->with($request->session()->get('data'));
        
	}

    public function verify(Request $request)
    {
        $this->validate($request, [
            'reference'         =>  'required',
        ]);

        $paymentDetails     =   Paystack::getPaymentData();
        $paymentDetail      =   $paymentDetails['data'];

        $paymentData        =   $paymentDetails['data']['metadata'];
        $hire_type          =   $paymentData['hire_type'];
        $currency           =   $paymentDetail['currency'];
        $reference          =   $paymentDetail['reference'];
        $amount             =   $paymentDetail['amount'];
        $data               =   $request->session()->get('data');
        $userId             =   Auth::user()->id;

        if($hire_type == "payment_booking")
        {
            DB::transaction(function () use ($request, $data, $currency, $reference, $userId, $amount)    {
            
                $paymentBookingModel  =   new PaymentBooking;

                $amount = 0;

                $currentDate = Carbon::now();
                $expirationDate = '';

                $booking_type = $data['booking_type'];

                if($booking_type == '7 Days')
                {
                    $amount = 510000;
                    $expirationDate = $currentDate->addDays(7);

                }
                elseif ($booking_type == '14 Days') 
                {
                    $amount = 1100000;
                    $expirationDate = $currentDate->addDays(14);
                }
                elseif($booking_type == '30 Days')
                {
                    $amount = 2100000;
                    $expirationDate = $currentDate->addDays(30);
                }
                else
                {
                    $amount = 2100000;
                    $expirationDate = $currentDate->addDays(1);
                }

                $paymentBookingModel->datePaid             =   Carbon::now();
                $paymentBookingModel->expirationDate       =   $expirationDate;
                $paymentBookingModel->currency_code        =   $currency;
                $paymentBookingModel->payment_reference    =   $reference;
                $paymentBookingModel->amount               =   $amount;
                $paymentBookingModel->booking_type         =   $booking_type;
                $paymentBookingModel->user_id              =   $userId;
                $paymentBookingModel->isPaid               =   true;

                $paymentBookingModel->save();

                $user = User::find($userId);

                if($user->verified == 0)
                {
                    User::where('id', $userId)->update(['verified' => true]);
                }

                AdminPaymentBookingJob::dispatch($paymentBookingModel)->delay(now()->addSeconds(1));
                PaymentBookingJob::dispatch($paymentBookingModel)->delay(now()->addSeconds(3));
                
            }, 5);

            $message = "Your Payment is successful. You can view all chefs for only ". $data['booking_type'];

            return redirect()->intended('payment/chefs')->with('success', $message);
        }
        else
        {
            DB::transaction(function () use ($request, $data, $currency, $reference, $userId, $amount)    {

                $orderModel                     =   new Order;
                $cart_items                     =     Cart::where('user_id', $data['user_id'])->orderBy('id')->get();


                foreach ($cart_items as $n => $cart_item) 
                {
     
                    $orderModel->user_id           =   $cart_item['user_id'];
                    $orderModel->chef_id           =   $cart_item['chef_id'];
                    $orderModel->payment_id        =   $paymentDetails['data']['id'];
                    $orderModel->deliver           =   0;
                    $orderModel->save();

                }

                $paymentModel                       =   new Payment;
                $paymentModel->currency_code        =   $currency;
                $paymentModel->payment_reference    =   $reference;
                $paymentModel->amount               =   $amount;
                $paymentModel->user_id              =   $userId;

                $paymentModel->save();

                $user = User::find($userId);

                if($user->verified == 0)
                {
                    User::where('id', $userId)->update(['verified' => true]);
                }

                UserOrder::dispatch($orderModel)->delay(now()->addSeconds(5));
                ChefOrder::dispatch($orderModel)->delay(now()->addSeconds(10));
                AdminOrder::dispatch($orderModel)->delay(now()->addSeconds(15));

            }, 5);

            return redirect()->intended('thank-you');
        }
        
    }
}
