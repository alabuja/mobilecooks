<?php

namespace App\Http\Controllers;

use App\PaymentBooking;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class PaymentBookingController extends Controller
{
	public function __construct(User $user)
	{
		$this->user 	=	$user;
	}

	public function getBookingsPage()
	{
		return view('pages.payment-bookings');
	}

	public function bookNow(Request $request)
	{
		$this->validate($request, [
            'booking_type' 	=>  'required',
        ]);

        $amount = 0;

        if($request->booking_type == '7 Days')
        {
            $amount = 5000;
        }

        if ($request->booking_type == '14 Days') 
        {
            $amount = 10000;
        }

        if($request->booking_type == '30 Days')
        {
        	$amount = 20000;
        }

        $email = '';

        if(Auth::check()){
        	$email = Auth::user()->email;
        }

        //dd($email);
        

        return view('pages.paymentbookings-page', ['booking_type' => $request->booking_type, 'amount' => $amount, 'email' => $email ]);
	}

    public function payForBooking(Request $request)
    {
    	$this->validate($request, [
    		'booking_type'		=>	'required',
    		'payment_reference' =>  'required',
    		'currency_code'		=>	'required',	
    	]);

    	DB::transaction(function () use ($request)    {
            
            $paymentModel  =   new PaymentBooking;

            $userId = Auth::user()->id;

            $amount = 0;

            $currentDate = Carbon::now();
            $expirationDate = '';

            if($request->booking_type == '7 Days')
            {
            	$amount = 500000;
            	$expirationDate = $currentDate->addDays(7);

            }
            elseif ($request->booking_type == '14 Days') 
            {
            	$amount = 1000000;
            	$expirationDate = $currentDate->addDays(14);
            }
            elseif($request->booking_type == '30 Days')
            {
            	$amount = 2000000;
            	$expirationDate = $currentDate->addDays(30);
            }
            else
            {
            	$amount = 2000000;
            	$expirationDate = $currentDate->addDays(1);
            }

            dd($paymentModel);

            $paymentModel->datePaid				=	$currentDate;
            $paymentModel->expirationDate		=	$expirationDate;
            $paymentModel->currency_code        =   $request->currency_code;
            $paymentModel->payment_reference    =   $request->payment_reference;
            $paymentModel->amount               =   $amount;
            $paymentModel->booking_type         =   $request->booking_type;
            $paymentModel->user_id              =   $userId;

            $paymentModel->save();

            $user = User::find($userId);

            if($user->verified == 0)
            {
                $this->user->where('user_id', $userId)->update(['verified' => true]);
            }

            //AdminOrder::dispatch($this->order)->delay(now()->addSeconds(1));
        	//UserOrder::dispatch($this->order)->delay(now()->addSeconds(3));
            
        }, 5);

    }
}
