<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use App\Order;
use App\User;
use App\Booking;
use App\Jobs\AdminOrder;
use App\Jobs\UserOrder;
use App\Jobs\ChefOrder;
use App\Mail\AdminSubscription;
use App\Mail\UserSubscription;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Mail;

class PayController extends Controller
{
	public function __construct(Order $order, User $user)
	{
        $this->order    =   $order;
		$this->user 	=	$user;
	}
    public function paynow(Request $request)
    {
    	$this->validate($request, [
    		'chef_id'			=>	'required',
    		'payment_reference' =>  'required',
    		'amount'			=>	'required',
    		'payment_id'		=>	'required',
    		'currency_code'		=>	'required',	
    	]);

    	DB::transaction(function () use ($request)    {
            
            $userId = Auth::user()->id;

            $this->order->user_id           =   $userId;
            $this->order->chef_id           =   $request->chef_id;
            $this->order->payment_id        =   $request->payment_id;
            $this->order->deliver           =   0;
            $this->order->save();

            $paymentModel                       =   new Payment;
            $paymentModel->currency_code        =   $request->currency_code;
            $paymentModel->payment_reference    =   $request->payment_reference;
            $paymentModel->amount               =   $request->amount;
            $paymentModel->user_id              =   Auth::user()->id;

            $paymentModel->save();

            $user = User::find($userId);

            if($user->verified == 0)
            {
                $this->user->where('user_id', $userId)->update(['verified' => true]);
            }

            AdminOrder::dispatch($this->order)->delay(now()->addSeconds(1));
        	ChefOrder::dispatch($this->order)->delay(now()->addSeconds(2));
        	UserOrder::dispatch($this->order)->delay(now()->addSeconds(3));
            
        }, 5);

    }

    public function subscription(Request $request)
    {
        $this->validate($request, [
            'name'              =>  'required',
            'payment_reference' =>  'required',
            'amount'            =>  'required',
            'payment_id'        =>  'required',
            'currency_code'     =>  'required',
            'email'             =>  'required',
            'package_type'      =>  'required',
            'phone_number'      =>  'required',
            'location'          =>  'required',
            'chef_type'         =>  'required',
            'meal_type'         =>  'required',
            'description'       =>  'required',
            'number_of_person'  =>  'required',
            'engagement_type'   =>  'required',
            'booking_id'        =>  'required',
            'user_id'           =>  'required',
        ]);


        DB::transaction(function () use ($request)    {

            $paymentModel                       =   new Payment;
            $paymentModel->currency_code        =   $request->currency_code;
            $paymentModel->payment_reference    =   $request->payment_reference;
            $paymentModel->amount               =   $request->amount;
            $paymentModel->name                 =   $request->name;

            $userEmail  =   $request->email;

            $paymentModel->save();

            $booking                    =   new Booking;

            $booking->name              =   $request->name;
            $booking->email             =   $request->email;
            $booking->package_type      =   $request->package_type;
            $booking->phone_number      =   $request->phone_number;
            $booking->location          =   $request->location;
            $booking->chef_type         =   $request->chef_type;
            $booking->meal_type         =   $request->meal_type;
            $booking->description       =   $request->description;
            $booking->number_of_person  =   $request->number_of_person;
            $booking->engagement_type   =   $request->engagement_type;
            $booking->booking_id        =   $request->booking_id;
            $booking->user_id           =   $request->user_id;


            $email = new UserSubscription($booking);
            Mail::to($userEmail)->send($email);

            $recipients = ['danielaudueshiekpaobo@gmail.com','daniel.audu@driversng.com'];
            foreach ($recipients as $recipient) {
                $email2 = new AdminSubscription($booking);
                Mail::to($recipient)->send($email2);
            }
            
        }, 5);
    }
}
