<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GuestController extends Controller
{
    public function payment()
    {
        return view('payment');
    }

    public function faq()
    {
        return view('faq');
    }

    public function apply()
    {
        return view('apply');
    }

    public function term()
    {
        return view('term');
    }

    public function location($location)
    {
        return view('pages.location-listing');
    }

    public function pricing()
    {
        return view('pricing');
    }

    public function investors()
    {
        return view('investors');
    }

    public function support()
    {
        return view('support');
    }
}
