<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Chef;

class IndexController extends Controller
{
	public function __construct(Chef $chef)
	{
		$this->chef  	=	$chef;
	}

	public function index()
	{
		
		//$locations 	=	$this->chef->filterByLocation()->take(8);

		$allChefs 		=	$this->chef->allChef();


		return view('pages.index', compact('allChefs'));
	}
    
}
