<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use Illuminate\Support\Facades\Auth;
use App\Mail\AdminEvent;
use App\Mail\UserEvent;
use Illuminate\Support\Facades\DB;
use Mail;

class EventController extends Controller
{
	public function __construct(Event $event)
	{
		$this->event 	=	$event;
	}

    public function index()
    {
    	return view('pages.events');
    }

    public function store(Request $request)
    {
    	$this->validate($request, [
            'name'                         	=>  'required',
            'phone_number'                  =>  'required',
            'email'                         =>  'required',
            'event_type'                    =>  'nullable',
            'budget'                   		=>  'nullable',
            'date_of_event'                 =>  'required',
            'number_of_person'              =>  'required',
            'decoration_service'            =>  'required',
            'decoration_description'        =>  'nullable',
            'mobility_service'              =>  'required',
            'mobility_description'          =>  'nullable',
            'feeding_service'           	=>  'required',
            'feeding_description'           =>  'nullable',
            'location_service'             	=>  'required',
            'location_description'          =>  'nullable',
            'event_location_service'        =>  'nullable',
            'event_location_description'	=>  'nullable',
        ]);

        $userId = null;
        if(Auth::check())
        {
            $userId = Auth::user()->id;
        }

    	$this->event->user_id 				     	 = 	 $userId;
        $this->event->name                      	 =   $request->name;
        $this->event->phone_number                   =   $request->phone_number;
        $this->event->email                          =   $request->email;
        $this->event->event_type                     =   $request->event_type;
        $this->event->budget                  	     =   $request->budget;
        $this->event->date_of_event                  =   $request->date_of_event;
        $this->event->number_of_person               =   $request->number_of_person;
        $this->event->decoration_service             =   $request->decoration_service;
        $this->event->decoration_description         =   $request->decoration_description;
        $this->event->mobility_service               =   $request->mobility_service;
        $this->event->mobility_description           =   $request->mobility_description;
        $this->event->feeding_service                =   $request->feeding_service;
        $this->event->feeding_description            =   $request->feeding_description;
        $this->event->location_service               =   $request->location_service;
        $this->event->location_description           =   $request->location_description;
        $this->event->event_location_service         =   $request->event_location_service;
        $this->event->event_location_description     =   $request->event_location_description;

        $message = "One of our representatives would contact you shortly.";

        $userEmail  =   $request->email;

        if($this->event->save())
        {
           	$email = new UserEvent($this->event);
            Mail::to($userEmail)->send($email);

            $recipients = ['danielaudueshiekpaobo@gmail.com','daniel.audu@driversng.com'];
            foreach ($recipients as $recipient) {
                $email2 = new AdminEvent($this->event);
                Mail::to($recipient)->send($email2);
            }

           	$request->session()->flash('success', $message);

    		return back();
        }
    }
}
