<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use App\Chef;
use App\User;
use App\Package;
use App\Order;
use App\Payment;
use App\PaymentBooking;
use App\Booking;
use App\State;
use App\Event;
use Cloudder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;

class AdminController extends Controller
{
	public function __construct(Chef $chefs, User $users, Admin $admin, Package $package, Order $order, Payment $payment, State $state, Booking $booking, Event $event, PaymentBooking $paymentBooking)
	{
		$this->chefs 	=	$chefs;
		$this->users 	=	$users;
        $this->admin    =   $admin;
        $this->payment  =   $payment;
        $this->order    =   $order;
        $this->state    =   $state;
        $this->booking  =   $booking;
		$this->event 	=	$event;
        $this->paymentBooking = $paymentBooking;
	}

    public function index()
    {
    	$count_verified_users   	=	$this->users->countVerifiedUsers();
    	$count_unverified_users   	=	$this->users->countUnverifiedUsers();
    	$count_verified_chefs   	=	$this->chefs->countVerifiedChefs();
        $count_unverified_chefs     =   $this->chefs->countUnverifiedChefs();
        $payments                   =   $this->payment->paymentsCount();
        $orders                     =   $this->order->ordersCount();
        $bookings                   =   $this->booking->bookingsCount();
    	$events                 	=	$this->event->eventsCount();
        $paymentsBooking            =   $this->paymentBooking->paymentsBookingCount();

        return view('admin.dashboard', compact('count_verified_users', 'count_unverified_users', 'count_verified_chefs', 'count_unverified_chefs', 'payments', 'orders', 'bookings', 'events', 'paymentsBooking'));
    }

    public function profile()
    {
    	return view('admin.profile');
    }

    public function verifiedChefs()
    {
    	$verified_chefs 	=	$this->admin->verifiedChefs();

    	return view('admin.verified-chefs', compact('verified_chefs'));
    }

    public function verifiedUsers()
    {
    	$verified_users 	=	$this->admin->verifiedUsers();

    	return view('admin.verified-users', compact('verified_users'));
    }

    public function unVerifiedChefs()
    {
    	$unverified_chefs 	=	$this->admin->unVerifiedChefs();
    	return view('admin.unverified-chefs', compact('unverified_chefs'));
    }

    public function unVerifiedUsers()
    {
    	$unverified_users 	=	$this->admin->unVerifiedUsers();
    	return view('admin.unverified-users', compact('unverified_users'));
    }

    public function payment()
    {
        $payments       =   $this->payment->allPayments();

        return view('admin.payment', compact('payments'));
    }

    public function subscriptions()
    {
        $bookings       =   $this->booking->allBookings();

        return view('admin.subscriptions', compact('bookings'));
    }

    public function events()
    {
        $events         =   $this->event->allEvents();

        return view('admin.events', compact('events'));
    }

    public function order()
    {
        $orders         =   $this->order->getAllOrders();

        return view('admin.order', compact('orders'));   
    }

    public function paymentBooking()
    {
        $payments     =   $this->paymentBooking->allPaymentsBookings();

        return view('admin.payment-bookings', compact('payments'));
    }


    public function update()
    {
    	return view('admin.update');
    }

    public function getNewChef()
    {
        $states     =       $this->state->getStates();
        return view('admin.new-chef', compact('states'));
    }

    public function getChef($chef_id)
    {
        $chef   =   Chef::where('id', $chef_id)->first();

        return view('admin.single-chef', compact('chef'));
    }

    public function getUpdateChef($chef_id)
    {
        $chef       =       Chef::where('id', $chef_id)->first();
        $states     =       $this->state->getStates();

        return view('admin.update-chef-profile', compact('chef', 'states'));
    }

    public function updatePassword(Request $request)
    {
    	$this->validate($request, [
            'old' => 'required',
            'password' => 'required|confirmed',
        ]);

    	$admin = $this->admin->find(Auth::guard('admin')->id());
        $hashedPassword = $admin->password;

 
    	if (Hash::check($request->old, $hashedPassword)) {
            //Change the password
            $admin->fill([
                'password' => Hash::make($request->password)
            ])->save();
 
            $request->session()->flash('success', 'Your password has been changed.');
 
            return back();
        }
 
        $request->session()->flash('failure', 'Your password was not been changed.');
 
        return back();	
    }

    public function verifyChef(Request $request, $chef_id, $slug)
    {
        $this->validate($request,[
            'verify'  => 'required',
        ]);

        $this->chefs->verified  =   $request->verify;


        $updateChef   =   Chef::where('verified', 0)->where('id', $chef_id)->update(['verified' => $this->chefs->verified]);

        $request->session()->flash('success', 'Chef has been Verified.');

        return back();
    }

    public function verifyUser(Request $request, $user_id)
    {
        $this->validate($request,[
            'verify'  => 'required',
        ]);

        $this->users->verified  =   $request->verify;

        $updateUser   =   User::where('verified', 0)->where('id', $user_id)->update(['verified' => $this->users->verified]);

        $request->session()->flash('success', 'User has been Verified.');
        return back();
    }

    public function unverifyChef(Request $request, $chef_id, $slug)
    {
        $this->validate($request,[
            'verify'  => 'required',
        ]);

        $this->chefs->verified  =   $request->verify;


        $updateChef   =   Chef::where('verified', 1)->where('id', $chef_id)->update(['verified' => $this->chefs->verified]);

        $request->session()->flash('success', 'Chef has been Unverified.');

        return back();
    }

    public function deleteChef($chef_id)
    {
        $deletechef   =   Chef::where('id', $chef_id)->delete();

        if($deletechef)
        {
            $return     =   [
                                "status"    => 200,
                                "response"  => $deletechef
                            ];

            return json_encode($return, true);
        }

    }

    public function updateChef(Request $request, $chef_id)
    {
        $this->validate($request, [
            'firstname'                         =>  'required',
            'lastname'                          =>  'required',
            'address'                           =>  'required',
            'phone_number'                      =>  'required',
            'state_residence'                   =>  'required',
            'state_origin'                      =>  'required',
            'nationality'                       =>  'required',
            'guarantor1_name'                   =>  'required',
            'guarantor1_phone_number'           =>  'required',
            'guarantor1_address'                =>  'required',
            'guarantor2_name'                   =>  'nullable',
            'guarantor2_phone_number'           =>  'nullable',
            'guarantor2_address'                =>  'nullable',
            'guarantor2_occupation'             =>  'nullable',
            'dish_type'                         =>  'required',
            'specialty'                         =>  'required',
            'client_name'                       =>  'required',
            'client_phone_number'               =>  'required',
            'date_of_birth'                     =>  'required|date',
            'number_of_years'                   =>  'required|numeric',  
            'image'                             =>  'required|mimes:jpeg,jpg,png'
            // 'bvn'                               =>  'required|numeric|digits:11'
        ]);

        $this->chefs->firstname                        =   $request->firstname;
        $this->chefs->lastname                         =   $request->lastname;
        $this->chefs->address                          =   $request->address;
        $this->chefs->phone_number                     =   $request->phone_number;
        $this->chefs->state_residence                  =   $request->state_residence;
        $this->chefs->state_origin                     =   $request->state_origin;
        $this->chefs->nationality                      =   $request->nationality;
        $this->chefs->guarantor1_name                  =   $request->guarantor1_name;
        $this->chefs->guarantor1_phone_number          =   $request->guarantor1_phone_number;
        $this->chefs->guarantor1_address               =   $request->guarantor1_address;
        $this->chefs->guarantor1_occupation            =   $request->guarantor1_occupation;
        $this->chefs->guarantor2_name                  =   $request->guarantor2_name;
        $this->chefs->guarantor2_phone_number          =   $request->guarantor2_phone_number;
        $this->chefs->guarantor2_address               =   $request->guarantor2_address;
        $this->chefs->guarantor2_occupation            =   $request->guarantor2_occupation;
        $this->chefs->meal_type                        =   $request->dish_type;
        $this->chefs->specialty                        =   $request->specialty;
        $this->chefs->client_name                      =   $request->client_name;
        $this->chefs->client_phone_number              =   $request->client_phone_number;
        $this->chefs->date_of_birth                    =   $request->date_of_birth;
        $this->chefs->number_of_years                  =   $request->number_of_years;
        // $this->chef->bvn                              =   $request->bvn;

        $chef_image                               =   $request->file('image')->getRealPath();

        Cloudder::upload($chef_image, null);
        list($width, $height) = getimagesize($chef_image);

        $chefUrl = Cloudder::secureShow(Cloudder::getPublicId(), [
            "crop" => "fit", "width" => 125, "height" => 125
        ]);

        $this->chefs->image_url                        =   $chefUrl;

        $update   =   $this->chefs::where('id', $chef_id)->update(
                    [ 'firstname'   => $this->chefs->firstname, 'lastname' => $this->chefs->lastname,
                      'address'      => $this->chefs->address, 
                      'phone_number' => $this->chefs->phone_number, 
                      'state_residence' => $this->chefs->state_residence, 
                      'state_origin' => $this->chefs->state_origin, 
                      'nationality' => $this->chefs->nationality, 
                      'guarantor1_name' => $this->chefs->guarantor1_name, 
                      'guarantor1_phone_number' => $this->chefs->guarantor1_phone_number,
                      'guarantor1_address'      => $this->chefs->guarantor1_address,
                      'guarantor1_occupation'   =>  $this->chefs->guarantor1_occupation,
                      'guarantor2_name' => $this->chefs->guarantor2_name, 
                      'guarantor2_phone_number' => $this->chefs->guarantor2_phone_number,
                      'guarantor2_address' => $this->chefs->guarantor2_address,
                      'guarantor2_occupation' => $this->chefs->guarantor2_occupation,
                      'meal_type' => $this->chefs->meal_type, 
                      'specialty' => $this->chefs->specialty, 
                      'client_name' => $this->chefs->client_name, 
                      'client_phone_number' => $this->chefs->client_phone_number, 
                      'date_of_birth' => $this->chefs->date_of_birth, 
                      'number_of_years' => $this->chefs->number_of_years, 
                      // 'bvn' => $this->chef->bvn, 
                      'image_url' => $this->chefs->image_url
                    ]);

        $request->session()->flash('success', 'You just updated a chef\'s profile.');

        return back();
    }


    public function addChef(Request $request)
    {
        $this->validate($request, [
            'firstname'                   =>  'required',
            'lastname'                    =>  'required',
            'address'                     =>  'required',
            'email'                       =>  'required|string|email|max:255|unique:chefs',
            'password'                    =>  'required|string|min:6',
            'phone_number'                =>  'required|string|unique:chefs',
            'state_residence'             =>  'required',
            'state_origin'                =>  'required',
            'nationality'                 =>  'required',
            'guarantor1_name'             =>  'required',
            'guarantor1_phone_number'     =>  'required',
            'guarantor1_address'          =>  'required',
            'guarantor1_occupation'       =>  'required',
            'guarantor2_name'             =>  'nullable',
            'guarantor2_phone_number'     =>  'nullable',
            'guarantor2_address'          =>  'nullable',
            'guarantor2_occupation'       =>  'nullable',
            'dish_type'                   =>  'required',
            'specialty'                   =>  'required',
            'client_name'                 =>  'required',
            'client_phone_number'         =>  'required',
            'date_of_birth'               =>  'required',
            'number_of_years'             =>  'required|numeric', 
            'image'                       =>  'required|mimes:jpeg,jpg,png'
        ]);

        $this->chef->firstname                        =   $request->firstname;
        $this->chef->lastname                         =   $request->lastname;
        $this->chef->address                          =   $request->address;
        $this->chef->phone_number                     =   $request->phone_number;
        $this->chef->state_residence                  =   $request->state_residence;
        $this->chef->state_origin                     =   $request->state_origin;
        $this->chef->nationality                      =   $request->nationality;
        $this->chef->guarantor1_name                  =   $request->guarantor1_name;
        $this->chef->guarantor1_phone_number          =   $request->guarantor1_phone_number;
        $this->chef->guarantor1_address               =   $request->guarantor1_address;
        $this->chef->guarantor1_occupation            =   $request->guarantor1_occupation;
        $this->chef->guarantor2_name                  =   $request->guarantor2_name;
        $this->chef->guarantor2_phone_number          =   $request->guarantor2_phone_number;
        $this->chef->guarantor2_address               =   $request->guarantor2_address;
        $this->chef->guarantor2_occupation            =   $request->guarantor2_occupation;
        $this->chef->meal_type                        =   $request->dish_type;
        $this->chef->specialty                        =   $request->specialty;
        $this->chef->client_name                      =   $request->client_name;
        $this->chef->client_phone_number              =   $request->client_phone_number;
        $this->chef->date_of_birth                    =   $request->date_of_birth;
        $this->chef->number_of_years                  =   $request->number_of_years;
        $this->chef->confirm_terms                    =   1;
        $chef_image                                   =   $request->file('image')->getRealPath();

        Cloudder::upload($chef_image, null);
        list($width, $height) = getimagesize($chef_image);

        $chefUrl = Cloudder::secureShow(Cloudder::getPublicId(), [
            "crop" => "fit", "width" => 125, "height" => 125
        ]);
        
        $this->chef->image_url                        =   $chefUrl;

        $this->chef->save();

        $request->session()->flash('success', 'Congratulations! You have successfully registered a Mobilecook Professional.');

        return back();
    }

}
