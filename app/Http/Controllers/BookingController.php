<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Booking;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{
	public function __construct(Booking $booking)
	{
		$this->booking = $booking;
	}

    public function store(Request $request)
    {
    	$this->validate($request, [
    		'name'					=>	'required',
    		'phone_number'			=>	'required',
    		'email'       			=>	'required',
    		'location'				=>	'required',
    		'chef_type'				=>	'required',
    		'meal_type'				=>	'required',
    		'description'			=>	'nullable',
    		'number_of_person'		=>	'nullable',
    		'engagement_type'		=>	'required'
    	]);

        $userId = null;
        if(Auth::check())
        {
            $userId = Auth::user()->id;
        }

    	$this->booking->user_id 			= 	$userId;
    	$this->booking->name  				= 	$request->name;
    	$this->booking->phone_number  		= 	$request->phone_number;
    	$this->booking->email_address  		= 	$request->email;
    	$this->booking->location  			= 	$request->location;
    	$this->booking->chef_type  			= 	$request->chef_type;
    	$this->booking->meal_type  			= 	$request->meal_type;
    	$this->booking->description  		= 	$request->description;
    	$this->booking->number_of_person  	= 	$request->number_of_person;
    	$this->booking->engagement_type  	= 	$request->engagement_type;

    		// List of all the pricing type
    	return view("pages.pricing", ['request' => $request]);
    }

    // 2 forms of payment
    public function getPayment(Request $request)
    {
        $this->booking->user_id             =   $request->user_id;
        $this->booking->package_type        =   $request->package;
        $this->booking->booking_id          =   'Subscription'.rand(1111,9999);
        $this->booking->name                =   $request->name;
        $this->booking->phone_number        =   $request->phone_number;
        $this->booking->email_address       =   $request->email;
        $this->booking->location            =   $request->location;
        $this->booking->chef_type           =   $request->chef_type;
        $this->booking->meal_type           =   $request->meal_type;
        $this->booking->description         =   $request->description;
        $this->booking->number_of_person    =   $request->number_of_person;
        $this->booking->engagement_type     =   $request->engagement_type;

        $bookingID  =   $this->booking->booking_id;


        $message = "Your cook will be available within 24 - 72 Hours after confirmation of payment.";

        if($this->booking->save())
        {
    	   return view("pages.payment", ['request' => $request, 'bookingID' => $bookingID, 'amount' => $request->price ])
           ->with('success', $message);
        }
    }

    public function getPackage(Request $request)
    {
        $this->validate($request, [
            'package'                   =>  'required',
            'price'                     =>  'required',

        ]);

        return view('pages.bookings', ['package' => $request->package, 'amount' => $request->price]);
    }


    public function getPaymentPage(Request $request)
    {
        $userId = null;
        if(Auth::check())
        {
            $userId = Auth::user()->id;
        }

        $this->booking->user_id             =   $userId;
        $this->booking->package_type        =   $request->package_type;
        $this->booking->booking_id          =   'Subscription'.rand(1111,9999);
        $this->booking->name                =   $request->name;
        $this->booking->phone_number        =   $request->phone_number;
        $this->booking->email_address       =   $request->email;
        $this->booking->location            =   $request->location;
        $this->booking->chef_type           =   $request->chef_type;
        $this->booking->meal_type           =   $request->meal_type;
        $this->booking->description         =   $request->description;
        $this->booking->number_of_person    =   $request->number_of_person;
        $this->booking->engagement_type     =   $request->engagement_type;

        $bookingID  =   $this->booking->booking_id;


        $message = "Your cook will be available within 24 - 72 Hours after confirmation of payment.";

        if($this->booking->save())
        {
           return view("pages.payment", ['request' => $request, 'bookingID' => $bookingID, 'amount' => $request->amount ])
           ->with('success', $message);
        }
    }
}
