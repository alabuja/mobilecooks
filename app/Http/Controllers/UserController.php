<?php

namespace App\Http\Controllers;

use App\User;
use App\Package;
use App\Chef;
use App\Order;
use App\Booking;
use App\PaymentBooking;
use Cloudder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Pagination\LengthAwarePaginator;

class UserController extends Controller
{

	public function __construct(User $user, Package $package, Chef $chefs, Order $orders, Booking $bookings, PaymentBooking $paymentsBooking)
	{
		$this->middleware('auth');

        $this->user             =   $user;
        $this->package          =   $package;
        $this->chefs            =   $chefs;
        $this->orders           =   $orders;
        $this->bookings         =   $bookings;
		$this->paymentsBooking 	=	$paymentsBooking;
	}

    public function profile()
    {
    	return view('pages.profile');
    }

    public function Updateprofile(Request $request)
    {
    	$this->validate($request, [
            'firstname'     =>  'required',
            'lastname'      =>  'required',
            'phone_number'  =>  'required',
    		'address'		=>	'nullable',
            'image'         =>  'required|mimes:jpeg,jpg,png',
    	]);

        $this->user->firstname         =    $request->firstname;
    	$this->user->lastname          =    $request->lastname;
        $this->user->phone_number      =    $request->phone_number ;
    	$this->user->address           =    $request->address;

        $user_image                               =   $request->file('image')->getRealPath();

        $userUrl                                  =   Cloudder::upload($user_image)->getResult();
        $userUrl                                  =   $userUrl['secure_url'];
        $this->user->file_url                     =   $userUrl;

    	$update   =   $this->user::where('id', Auth::user()->id)->update(['firstname' => $this->user->firstname, 'lastname' => $this->user->lastname, 'phone_number' => $this->user->phone_number, 'address' => $this->user->address, 'file_url' => $this->user->file_url]);

        $request->session()->flash('success', 'Your profile has been updated.');

    	return back();
    }

    public function changePassword()
    {
    	return view('pages.update');
    }

    public function updatepassword(Request $request)
    {
    	$this->validate($request, [
            'old' => 'required',
            'password' => 'required|confirmed',
        ]);

    	$user = $this->user->find(Auth::id());
        $hashedPassword = $user->password;

 
    	if (Hash::check($request->old, $hashedPassword)) {
            //Change the password
            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();
 
            $request->session()->flash('success', 'Your password has been changed.');
 
            return back();
        }
 
        $request->session()->flash('failure', 'Your password has not been changed.');
 
        return back();
    }

    public function package()
    {
    	return view('pages.package');
    }

    public function book()
    {
        $chefs              =   $this->chefs->allChef();
        return view('pages.subscribe', compact('chefs'));
    }

    public function dashboard()
    {
        return view('pages.dashboard');
    }

    public function bookings()
    {
        $bookings         =   $this->bookings->singleUserBooking(Auth::user()->id);
        $paginations    =   $this->bookings::paginate(20);

        return view('pages.users-bookings', compact('bookings', 'paginations'));
    }

    public function order()
    {   
        $orders         =   $this->orders->singleUserOrder(Auth::user()->id);
        $paginations    =   $this->orders::paginate(20);

        return view('pages.order', compact('orders', 'paginations'));
    }

    public function getSingleChef($id)
    {
        $chef           =   $this->chefs::where('id', $id)->first();

        return view('pages.single-chef', compact('chef'));
    }

    public function myChefs()
    {

        $chefs  =  $this->chefs->allChef();
        $userId   =  Auth::user()->id;

        $payment = $this->paymentsBooking->userPaymentsBooking($userId);

        return view('pages.my-chef', compact('chefs', 'payment'));
    }
}
