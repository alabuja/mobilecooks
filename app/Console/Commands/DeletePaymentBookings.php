<?php

namespace App\Console\Commands;

use App\PaymentBooking;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeletePaymentBookings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:bookings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete Bookings When Expiration Date Passes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $currentDate = Carbon::now();

        PaymentBooking::where('expirationDate', '<', $currentDate)
                        ->whereNull('deleted_at')
                        ->delete();
    }
}
