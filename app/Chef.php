<?php

namespace App;

use App\Notifications\ChefResetPassword;
use Illuminate\Notifications\Notifiable;
// use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Chef extends Authenticatable
{
    use SoftDeletes, Notifiable;
    
    protected $fillable = [
        'firstname', 'lastname', 'slug', 'address', 'email', 'phone_number', 'state_residence', 'state_origin', 'nationality', 'guarantor1_name', 'guarantor1_phone_number', 'guarantor1_address', 'guarantor1_occupation', 'guarantor2_name', 'guarantor2_phone_number', 'guarantor2_address', 'guarantor2_occupation', 'meal_type', 'specialty', 'client_name', 'client_phone_number', 'date_of_birth', 'number_of_years', 'image_url', 'bvn', 'password', 'verified', 'confirm_terms', 'review_avg', 'state_id',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ]; 

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function review()
    {
    	return $this->hasMany('App\Review');
    }

    public function order()
    {
        return $this->hasMany(Order::class);
    }

    public function cart()
    {
        return $this->hasMany(Cart::class);
    }

    public function countVerifiedChefs()
    {
        $count_verified     =   self::where('verified', 1)->count();

        return $count_verified;
    }

    public function countUnverifiedChefs()
    {
        $count_unverified     =   self::where('verified', 0)->count();

        return $count_unverified;
    }

    public function allChef()
    {
        $chefs =    self::select('*')
                        ->where('verified', 1)
                        ->orderBy('review_avg', 'desc')
                        ->get();

        return $chefs;
    }

    public function scopeSearch($query, $q)
    {
        if($q != "")
        {

           return $query->Where('verified', '=', 1)
            ->where(function($query) use ($q){
                $query->Where('lastname',       'LIKE', '%'. $q. '%' )
                ->orWhere('state_residence',    'LIKE', '%'. $q.'%')
                ->orWhere('specialty',          'LIKE', '%'. $q. '%');
            });
        }else{
            return $query->where(function($query) use ($q){
                $query->Where("verified", '=',      1);
            });
        }
    }

    public function filterByLocation()
    {
        $locations  =  self::selectRaw('state_residence, COUNT(state_residence) as number')
                            ->where('verified', 1)
                            ->groupBy('state_residence')
                            ->orderBy('number', 'desc')
                            ->get();

        // /dd($locations);

        return $locations;

    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ChefResetPassword($token));
    }
}
