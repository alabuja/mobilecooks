<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = "payments";

    protected $fillable = ['currency_code', 'payment_reference', 'amount', 'user_id'];

    
    public function allPayments()
    {
    	$payments  	=	self::whereNotNull('payment_reference')->get();

    	return $payments;
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function paymentsCount()
    {
        $count_payments     =   self::whereNotNull('payment_reference')->count();

        return $count_payments;
    }
}
 