<?php

namespace App\Mail;

use App\Event;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserEvent extends Mailable
{
    use Queueable, SerializesModels;
    protected $event;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Event $event)
    {
        $this->event  = $event;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.user_events')
                    ->subject('Your Quote details')
                    ->with([
                        'name'      =>  $this->event->name,
                        'budget' => $this->event->budget,
                        'event_type'  => $this->event->event_type,
                        'date_of_event' => $this->event->date_of_event,
                        'number_of_person' => $this->event->number_of_person,
                        'decoration_service' => $this->event->decoration_service,
                        'decoration_description' => $this->event->decoration_description,
                        'mobility_service' => $this->event->mobility_service,
                        'mobility_description' => $this->event->mobility_description,
                        'feeding_service' => $this->event->feeding_service,
                        'feeding_description' => $this->event->feeding_description,
                        'location_service' => $this->event->location_service,
                        'location_description' => $this->event->location_description,
                        'event_location_service' => $this->event->event_location_service,
                        'event_location_description' => $this->event->event_location_description,
                    ]) ;
    }
}
