<?php

namespace App\Mail;

use App\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserSubscription extends Mailable
{
    use Queueable, SerializesModels;
    protected $booking;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking      =   $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.subscription_user')
                    ->subject('New subscription Placed')
                    ->with([
                        'name'              =>  $this->booking->name,
                        'chef_type'         =>  $this->booking->chef_type,
                        'meal_type'         =>  $this->booking->meal_type,
                        'number_of_person'  =>  $this->booking->number_of_person,
                        'engagement_type'   =>  $this->booking->engagement_type
                    ]);
    }
}
