<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;
    
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function allBookings()
    {
    	$bookings = self::select('*')->get();

    	return $bookings;
    }

    public function singleUserBooking($id)
    {
        $bookings     =   self::where('user_id', $id)->orderBy('id', 'desc')->get();

        return $bookings;
    }

    public function bookingsCount()
    {
        $count_bookings     =   self::whereNotNull('phone_number')->count();

        return $count_bookings;
    }
}
