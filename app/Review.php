<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Review extends Model
{

    protected $fillable = [
        'chef_id', 'user_id', 'review_text', 'expertise', 'punctuality', 'courtesy',
    ];
	
    public function chef()
    {
    	return $this->belongsTo('App\Chef');
    }

    public static function getChefTotalReview($chef_id)
    {
    	$total_review 	= 	self::where('chef_id', $chef_id)->count();

    	return $total_review;
    }

    public static function getUserAvgReview($user_id)
    {
    	$expertise 		=	round(DB::table('reviews')->where('user_id', $user_id)->avg('expertise'));

        $punctuality 	=	round(DB::table('reviews')->where('user_id', $user_id)->avg('punctuality'));

        $courtesy		=	round(DB::table('reviews')->where('user_id', $user_id)->avg('courtesy'));

        $total_avg		=	round($expertise + $punctuality + $courtesy)/3;

		 return $total_avg;
    }

    public static function postChefAvgReview($chef_id)
    {
    	$expertise 			= 	round(DB::table('reviews')->where('chef_id', $chef_id)->avg('expertise'));

    	$punctuality 		= 	round(DB::table('reviews')->where('chef_id', $chef_id)->avg('punctuality'));

    	$courtesy 			= 	round(DB::table('reviews')->where('chef_id', $chef_id)->avg('courtesy'));

    	$total_chef_avg		=	round($expertise + $punctuality + $courtesy)/ 3;

    	return $total_chef_avg;
    }

    public static function checkUserReview($user_id, $chef_id)
    {
    	$user_review 	= 	self::where("user_id", $user_id)->where("chef_id", $chef_id)->first();

		 return $user_review;
    }

    public function chef_reviews($chef_id)
    {
        $chef   =   self::select('*')
                                ->where('chef_id', $chef_id)
                                ->orderBy('id', 'desc')
                                ->get();

        return $chef;
    }
}
