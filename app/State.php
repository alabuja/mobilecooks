<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = [
        'name',
    ];

    public function chef_residence()
    {
    	return $this->hasMany(Chef::class);
    }

    public function chef_origin()
    {
    	return $this->hasMany(Chef::class);
    }

    public function getStates()
    {
    	$states 	=	self::whereNotNull('name')->get();

    	return $states;
    }
}
