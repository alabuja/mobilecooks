<?php

namespace App;

use App\Notifications\AdminResetPassword;
use Illuminate\Notifications\Notifiable;
// use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\User;
use App\Chef;

class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'admins';
    
    protected $fillable = ['username', 'email', 'password', 'avatar_url'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPassword($token));
    }


    public function verifiedChefs()
    {
        $verified_chefs     =   Chef::select('*')->where('verified', 1)->orderBy('id', 'desc')->get();

        return $verified_chefs;
    }

    public function verifiedUsers()
    {
        $verified_users     =   User::select('*')->where('verified', 1)->orderBy('id', 'desc')->get();

        return $verified_users;
    }

    public function unVerifiedUsers()
    {
        $unverified_users     =   User::select('*')->where('verified', 0)->orderBy('id', 'desc')->get();

        return $unverified_users;   
    }

    public function unVerifiedChefs()
    {
        $unverified_chefs     =   Chef::select('*')->where('verified', 0)->orderBy('id', 'desc')->get();

        return $unverified_chefs;
    }

}
