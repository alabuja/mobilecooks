<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\AdminPaymentBooking;
use Mail;

class AdminPaymentBookingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $bookings;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($bookings)
    {
        $this->bookings   =   $bookings;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $recipients = ['danielaudueshiekpaobo@gmail.com', 'alabujadaniel@gmail.com', 'daniel.audu@driversng.com'];
        foreach ($recipients as $recipient) {
            Mail::to($recipient)->send(new AdminPaymentBooking($this->bookings));
        }
    }
}
