<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\OrderAdmin;
use Mail;

class AdminOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $orders;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($orders)
    {
        $this->orders      =    $orders;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $recipients = ['danielaudueshiekpaobo@gmail.com', 'alabujadaniel@gmail.com', 'daniel.audu@driversng.com'];
        foreach ($recipients as $recipient) {
            Mail::to($recipient)->send(new OrderAdmin($this->orders));
        }

        // Mail::to(ENV('ADMIN_MAIL_TO_ADDRESS2'))->send(new OrderAdmin($this->orders));
    }
}
