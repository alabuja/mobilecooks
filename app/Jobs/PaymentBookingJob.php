<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\PaymentBooking;
use Mail;

class PaymentBookingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $bookings;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($bookings)
    {
        $this->bookings   =   $bookings;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->bookings->user->email)->send(new PaymentBooking($this->bookings));
    }
}
