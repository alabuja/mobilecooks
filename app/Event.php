<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function allEvents()
    {
    	$events = self::select('*')->get();

    	return $events;
    }

    public function singleUserEvents($id)
    {
        $events     =   self::where('user_id', $id)->orderBy('id', 'desc')->get();

        return $events;
    }

    public function eventsCount()
    {
        $count_events     =   self::whereNotNull('phone_number')->count();

        return $count_events;
    }
}
