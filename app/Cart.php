<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Cart extends Model
{
	use SoftDeletes;

    protected $fillable = [
    	'user_id', 'chef_id', 'amount',
    ];

    public function singleUser($id)
    {
    	$user   =   self::where('user_id', $id)->orderBy('id', 'desc')->get();

    	return $user;
    }

    public function userSum($id)
    {
    	$sum 	=	self::where('user_id', $id)->sum('amount');
    	//Auth::id()
    	$sum = $sum / 100;
    	return $sum;
    }

    public static function sumCheckout()
    {
        $sumCheckout        =   self::where('user_id', Auth::user()->id)->sum('amount');

        $sumCheckout        =   $sumCheckout / 100;

        return $sumCheckout;
    }

    public function realAmount($id)
    {
    	$amount 	=	self::where('user_id', $id)->sum('amount');
    	//Auth::id()
    	return $amount;
    }

    public function chef()
    {
    	return $this->belongsTo(Chef::class);
    }

    public static function remove()
    {
    	$delete = self::where('user_id', Auth::user()->id)->delete();

    	return $delete;
    }


    public static function cartCount()
    {
    	$count  = self::where('user_id', Auth::user()->id)->count();

    	return $count;
    }
}
