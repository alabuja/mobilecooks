<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Order extends Model
{
    use SoftDeletes;

    protected $fillable = [
    	'user_id', 'payment_id', 'chef_id', 'deliver',
    ];

    public function chef()
    {
    	return $this->belongsTo(Chef::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
    // public function package()
    // {
    // 	return $this->belongsTo(Package::class);
    // }

    public function getAllOrders()
    {
    	$orders 	=	self::get();

    	return $orders;
    }

    public function singleUserOrder($id)
    {
        $orders     =   self::where('user_id', $id)->orderBy('id', 'desc')->get();

        return $orders;
    }

    public function ordersCount()
    {
        $count_orders     =   self::whereNotNull('payment_id')->count();

        return $count_orders;
    }

    public function chefOrders()
    {

        $chef_orders    =   self::select('orders.*')
                                ->join('users', 'orders.user_id', 'users.id')
                                ->where('orders.chef_id', Auth::guard('chef')->user()->id)
                                ->whereNotNull('orders.payment_id')
                                ->where('orders.deliver', 0)
                                ->where('users.verified', 1)
                                ->get();
        return $chef_orders;
    }
}
