<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'name', 'amount', 'number_of_cooks',
    ];


    public function viewPackages()
    {
    	$packages 	=	self::whereNotNull('name')->get();

    	return $packages; 
    }
}
