function deleteChef(id){

    var del = confirm("Are you sure you want to delete this? Press ok to continue");
    //alert(slug);
    if (del == true) {
        $.ajax({
            type: "GET",
            url: id,
            success: function (data) {
                var response = JSON.parse(data);
                alert('Order has been successfully Deleted!');
            },
            error: function (jqXHR) {
                var response = JSON.parse(jqXHR);
            }

        });
    }
}

function payWithPaystack(){

    $('#paynow').hide();
    $('#payment_loader').show();

    var handler = PaystackPop.setup({
        key:      $('#key').val(),
        email:    $('#email').val(),
        amount:   $('#amount').val(),
        ref:      $('#reference').val(), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
        currency: 'NGN',
      
        callback: function(response){
            let payment_id   =  response.trans;
            let reference   =  response.reference;
            if (response.status === "success") {
                let data = { 

                    chef_id:                    $("#chef_id").val(),
                    payment_reference:          reference,
                    amount:                     $('#amount').val(),
                    payment_id:                 payment_id, 
                    currency_code:              'NGN', 

                };
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
                });
                $.post("/paynow", data)
                    .done(function (result) {
                        alert('Chef was successfully paid for');
                        window.location.reload(true) ;
                    })
                    .fail(function (result) {
                        alert('failed');
                        window.location.reload(true);
                    });
                    // redirect to a success page
                } 
                else {
                    alert('Request Failed!!!');
                    window.location.reload(true);
                }
            //alert('success. transaction ref is ' + response.reference);
        },
        onClose: function(){
            alert('Request Failed!');
        }
    });
    handler.openIframe();
}


function payWithPaystackSubscription(){

    $('#paynow').hide();
    $('#payment_subscription_loader').show();

    var handler = PaystackPop.setup({
        key:      $('#key').val(),
        email:    $('#email').val(),
        amount:   $('#amount').val(),
        ref:      $('#reference').val(), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
        currency: 'NGN',
      
        callback: function(response){
            let payment_id   =  response.trans;
            let reference   =  response.reference;

            if (response.status === "success") {
                let data = { 
                    name:                       $('#name').val(),
                    payment_reference:          reference,
                    amount:                     $('#amount').val(),
                    payment_id:                 payment_id, 
                    currency_code:              'NGN',
                    email:                      $('#email').val(),
                    package_type:                      $('#package_type').val(),
                    phone_number:                      $('#phone_number').val(),
                    location:                      $('#location').val(),
                    chef_type:                      $('#chef_type').val(),
                    meal_type:                      $('#meal_type').val(),
                    description:                      $('#description').val(),
                    number_of_person:                      $('#number_of_person').val(),
                    engagement_type:                      $('#engagement_type').val(),
                    booking_id:                      $('#booking_id').val(),
                    user_id:                      $('#user_id').val(),
                };

                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
                });
                $.post("subscription/pay", data)
                    .done(function (result) {
                        alert('Payment Successfully made');
                        window.location.href = '{{ url('/') }}';
                    })
                    .fail(function (result) {
                        alert('failed');
                        window.location.reload(true);
                    });
                    // redirect to a success page
                } 
                else {
                    alert('Request Failed!!!');
                    window.location.reload(true);
                }
        },
        onClose: function(){
            alert('Request Failed!');
        }
    });
    handler.openIframe();
}

function payWithPaystackBooking()
{
    $('#paynow').hide();
    $('#payment_booking_loader').show();

    var handler = PaystackPop.setup({
        key:      $('#key').val(),
        email:    $('#email').val(),
        amount:   $('#amount').val(),
        ref:      $('#reference').val(), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
        currency: 'NGN',
      
        callback: function(response){
            let reference   =  response.reference;
            if (response.status === "success") {
                let data = { 
                    booking_type:               $('#booking_type').val(),
                    payment_reference:          reference,
                    amount:                     $('#amount').val(),
                    currency_code:              'NGN',
                    email:                      $('#email').val(),
                };

                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
                });
                $.post("/payment/book-new", data)
                    .done(function (result) {
                        alert('Payment Successfully made');
                        window.location = "/payment-bookings";
                    })
                    .fail(function (result) {
                        alert('failed');
                        window.location.reload(true);
                    });
                    // redirect to a success page
                } 
                else {
                    alert('Request Failed!!!');
                    window.location.reload(true);
                }
        },
        onClose: function(){
            alert('Request Failed!');
        }
    });
    handler.openIframe();
}

jQuery(function(e){
    "use strict";
    function i(){
        e(this).css("display","block");
        let i=e(this).find(".modal-dialog"),
        a=(e(window).height()-i.height())/2,
        t=parseInt(i.css("marginBottom"),10);
        t>a&&(a=t),i.css("margin-top",a)
    }
    e(".modal").on("show.bs.modal",i),
    e(".modal-popup .close-link").click(function(i){
        i.preventDefault(),
        e(".modal").modal("hide")
    }),
    e(window).on("resize",function(){
    e(".modal:visible").each(i)})
});

let checkbox            =   document.getElementById('confirm_terms');  
let register_button     =   document.getElementById('registerBtn');  

checkbox.onchange = function(){
        
    if(this.checked){
        register_button.disabled = false;
    } else {
        register_button.disabled = true;
    }
}