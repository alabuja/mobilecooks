function deleteChef(id){

    var del = confirm("Are you sure you want to delete this? Press ok to continue");
    //alert(slug);
    if (del == true) {
        $.ajax({
            type: "GET",
            url: '/admin/delete-chef/'+id,
            success: function (data) {
                var response = JSON.parse(data);
                console.log(response);
                alert('Chef has been successfully Deleted!');
                //$("#categories").load(" #categories");
            },
            error: function (jqXHR) {
                var response = JSON.parse(jqXHR);
                console.log(response);
            }

        });
    }
}

$(document).ready(function(){

    $('#PackageForm').on('submit', function(e){

        $('#add_package_loader').show();
        $('#btnPackage').hide();

        let formData    =   new FormData();
        formData.append('name',             $("#name").val());
        formData.append('amount',           $("#amount").val());
        formData.append('number_of_cooks',  $("#number_of_cooks").val());

        e.preventDefault();

        $.ajax({

            method:         'POST',
            url:            '/admin/package',
            headers:        {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            data:           formData,
            cache:          false,
            processData:    false,
            contentType:    false, 
            success:    function(data){
                console.log(data);
            },
            error:      function(jqXHR){
                console.log(jqXHR);
            },
            complete: function(){
                $('#add_package_loader').hide();
                $('#btnPackage').show();
                alert('Package Added');
                window.location.reload(true);
            }

        });
    });
});