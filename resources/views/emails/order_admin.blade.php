<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
</head>
<body>

	<p><strong>{{$orders['user']['firstname'].' '.$orders['user']['lastname']}}</strong> just booked <strong>{{$orders['chef']['firstname'].' '.$orders['chef']['lastname']}} for a job.</strong>
	</p>

	<p>Follow the link below by clicking on the button to see the Order Details</p>
	<p><a href="{{url('admin/order')}}" class="btn btn-primary">Orders</a></p>

	<footer>
		Copyright &copy; 2016 - {{date('Y')}} <a target="_blank" href="{{url('/')}}">MobileCooks</a> | All rights Reserved 
	</footer>

	<style type="text/css">
		body {
		  	background: #f0f0f0;
		  	font-family: 'Poppins', sans-serif;
		  	color:#4e4e4e;
		  	height: 100%;
		  	padding-top: 15px;
		  	padding-bottom: 15px;
		}
		p{
			font-size: 14px;
		}
		a{
			text-decoration: none;
		}
		.btn-primary {
		  	padding: 8px 20px;
		  	background: #c41c3d;
		  	color: #fff;
		  	border-radius: 4px;
		  	border:none;
		  	margin-top: 10px;
		}
		.btn-primary:hover, 
		.btn-primary:focus{
		  	background: #5bc475;
		  	outline: none;
		  	box-shadow: none;
		} 

		footer{
			margin-top: 20px; font-size: 14px;
		}

		footer a{
			text-transform: uppercase;
			color: #5bc475;
			text-decoration: none;
		}

		footer a:hover, footer a:focus{
			color: #c41c3d;
		}
	</style>
	<script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
</body>
</html>