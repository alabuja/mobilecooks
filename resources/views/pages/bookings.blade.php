@extends('layouts.app')

@section('content')

@section('title')
Bookings 
@endsection

<div class="container" id="profile">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel">
                @include("alerts")
                <div class="panel-body">
                    <form method="POST" action="{{ url('subscription/bookings') }}" aria-label="Booking Page">
                        @csrf

                        <div class="form-group row required">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            @if(Auth::check())
                            	<div class="col-md-6">
	                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ Auth::user()->firstname .' '. Auth::user()->lastname  }}" required autofocus>

	                                @if ($errors->has('name'))
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $errors->first('name') }}</strong>
	                                    </span>
	                                @endif
	                            </div>

	                        @else
	                        	<div class="col-md-6">
	                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="" required autofocus>

	                                @if ($errors->has('name'))
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $errors->first('name') }}</strong>
	                                    </span>
	                                @endif
	                            </div>
                            @endif
                        </div>

                        @if(Auth::check())
	                        <div class="form-group row required">
	                            <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

	                            <div class="col-md-6">
	                                <input id="phone_number" type="number" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" value="{{ Auth::user()->phone_number }}" required>

	                                @if ($errors->has('phone_number'))
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $errors->first('phone_number') }}</strong>
	                                    </span>
	                                @endif
	                            </div>
	                        </div>
	                    @else

	                    	<div class="form-group row required">
	                            <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

	                            <div class="col-md-6">
	                                <input id="phone_number" type="number" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" value="" required>

	                                @if ($errors->has('phone_number'))
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $errors->first('phone_number') }}</strong>
	                                    </span>
	                                @endif
	                            </div>
	                        </div>

	                    @endif

	                    @if(Auth::check())

	                        <div class="form-group row required">
	                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

	                            <div class="col-md-6">
	                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ Auth::user()->email }}">

	                                @if ($errors->has('email'))
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $errors->first('email') }}</strong>
	                                    </span>
	                                @endif
	                            </div>
	                        </div>

                        @else

                        	<div class="form-group row required">
	                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

	                            <div class="col-md-6">
	                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="">

	                                @if ($errors->has('email'))
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $errors->first('email') }}</strong>
	                                    </span>
	                                @endif
	                            </div>
	                        </div>

                        @endif

                        <div class="form-group row required">
                            <label for="location" class="col-md-4 col-form-label text-md-right">{{ __(' (Location)') }}</label>

                            <div class="col-md-6">
                                <input id="location" type="text" class="form-control{{ $errors->has('location') ? ' is-invalid' : '' }}" name="location"  required>

                                @if ($errors->has('location'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span> 
                                @endif
                            </div>
                        </div>

                        <input type="hidden" name="package_type" value="{{ $package }}">
                        <input type="hidden" name="amount" value="{{ $amount }}">

                        <div class="form-group row">
                            <label for="chef_type" class="col-md-4 col-form-label text-md-right">{{ __('Chef Type') }}</label>

                            <div class="col-md-6">

                                <select class="form-control" name="chef_type" required>
			                        <option value="Domestic / Private Chef">Domestic / Private Chef</option>
				                    <option value="Restaurant Chef">Restaurant Chef</option>
				                    <option value="Office Chef">Office Chef</option>
				                    <option value="Event Chef">Event Chef</option>
				                    <option value="Pastries/cakes Chef">Pastries/cakes Chef</option>
			                	</select>

                                @if ($errors->has('chef_type'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('chef_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="meal_type" class="col-md-4 col-form-label text-md-right">{{ __('Meal Type') }}</label>

                            <div class="col-md-6">

                                <select class="form-control" name="meal_type" required>
			                        <option value="Domestic Meals">Domestic Meals</option>
				                    <option value="Intercontinental Meals">Intercontinental Meals</option>
				                    <option value="Pastries/cakes">Pastries/cakes</option>
				                    <option value="others (specify)">others (specify)</option>
			                	</select>

                                @if ($errors->has('meal_type'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('meal_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                        	<label for="description" class="col-md-4 col-form-label text-md-right"> {{ __('Description')}}</label>

                        	<div class="col-md-6">
                        		<textarea id="description" name="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" rows="6" placeholder="Description of Type of Meal"></textarea>

                        	</div>
                        </div>

                        <div class="form-group row">
                        	<label for="number_of_person" class="col-md-4 col-form-label text-md-right"> {{ __('Number of Persons To be Fed')}}</label>
                        	
                        	<div class="col-md-6">
                        		<input id="number_of_person" type="number" class="form-control{{ $errors->has('number_of_person') ? ' is-invalid' : '' }}" name="number_of_person" value="" min="1">

	                            @if ($errors->has('number_of_person'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('number_of_person') }}</strong>
	                                </span>
	                            @endif
                        	</div>
                        </div>

                        <div class="form-group row">
                        	<label for="engagement_type" class="col-md-4 col-form-label text-md-right"> {{ __('Enagement Type')}}</label>
                        	
                        	<div class="col-md-6">
                        		<select class="form-control" name="engagement_type" required>
			                        <option value="Temporary One-off Hiring  Engagement">Temporary One-off Hiring  Engagement</option>
					                <option value="Full Term One-off Hiring engagement">Full Term One-off Hiring engagement</option>
					                <option value=" Full term (plus monthly salaries) Hiring Engagement"> Full term (plus monthly salaries) Hiring Engagement</option>
				                </select>

	                            @if ($errors->has('engagement_type'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('engagement_type') }}</strong>
	                                </span>
	                            @endif
	                        </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Bookings') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include("partials.footer") 
@endsection
