@extends('layouts.app')

@section('content')
@section('title')
Subscribe - {{ucfirst(Auth::user()->firstname).' '.ucfirst(Auth::user()->lastname)}}
@endsection


{{-- @include('partials.banner2') --}}
 	<div class="container" id="chefs">
	    <div class="row">          
		    @include("alerts")
		    @if(count($chefs) > 0)

				<form id="selectedCheckBoxForm" method="POST" action="{{url('subscribe')}}">
					@csrf

		          	@foreach($chefs as $i => $chef) 
		          		<div class="col-md-6 col-sm-6 col-xs-12">         
		          			<div class="chef_list_detail">
							    <div data-wow-delay="0.{{$i}}s" class="strip_list wow fadeIn animated" style="visibility: visible; animation-delay: 0s; animation-name: fadeIn;">
							            <div class="row">         
								            <div class="col-md-9 col-sm-12">
								              	<div class="desc">
									                <div class="thumb_strip"> 
										                <a href="#">
										                	<img src="{{$chef->image_url}}" alt="{{ $chef->lastname }}">
										                </a>  
									                </div>         
									              	<h3 style="font-size: 13px;">Chef {{ strlen($chef->lastname) > 12 ? substr(ucwords(strtolower($chef->lastname)), 0, 11) : ucwords(strtolower($chef->lastname))}} |
								              	<span class="state_residence" style="font-size: 11px;">
								              		@if(empty($chef->state_residence))
								              			Nigeria
								              		@else
								              			{{ucwords($chef->state_residence)}}
								              		@endif
								              	</span></h3>
									              	<hr>
									              	<h4>Specialty</h4>
									              	<p class="">{{$chef->specialty}}</p>

										            <div class="rating"> 
										                @for($x = 0; $x < 5; $x++)
		                    
										                @if($x < $chef->review_avg)
										                  <i class="fa fa-star"></i>
										                @else
										                  <i class="fa fa-star fa fa-star-o"></i>
										                @endif
										                
										                @endfor
										                (<small><a href="{{URL::to('chef/'.$chef->id.'-'.$chef->slug)}}">Read {{\App\Review::getChefTotalReview($chef->id)}} reviews</a></small>)
										            </div>
								            	</div>
								            </div>
								            <div class="col-md-3 col-sm-12">
										        <div class="go_to">
										            <div> 
										            	<a class="" href="#"><input type="checkbox" name="selected_chef[]" value="{{$chef->firstname .' '. $chef->lastname}} with phone number of {{$chef->phone_number}} and stays at {{$chef->address}} has been booked."></a>
										            </div>
										        </div>
									        </div>
								        </div>
								</div>
							</div>
		    			</div>
					@endforeach

					<input type="hidden" name="number_picked" id="number_picked" value="0" disabled>
					<input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}">
            		<input type="hidden" name="key" value="{{ config('paystack.secretKey') }}">
            		<input type="hidden" name="temporary_amount" id="temporary_amount">
            		<input type="hidden" name="email" value="alabujadaniel@yahoo.com">
            		<input type="hidden" name="amount" value="0" id="amount">

					<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
						<button type="submit" class="btn btn-primary" style="margin-bottom: 10px;">Book Order</button>
					</div>
			    </form>
			@else
				<h5>No Chef Available Now!!</h5>
		    @endif
		</div>
	</div>
@include("partials.footer") 
@endsection
