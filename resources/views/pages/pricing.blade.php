@extends('layouts.app')

@section('content')

@section('title')
Pricing
@endsection

<div class="container" id="pricing">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6 text-center">
		        <h4>Temporary One-Off Hiring Engagement Service</h4>
		        <span class="h2">
		        	<span>&#8358;</span>5,000
		        	<small style="font-size: .4em;">vat inclusive</small>
		    	</span>
                <ul>
                    <li>A verified chef</li>
                    <li>Matched in 24 - 72 hours</li>
                    <li>Fit For Short Term Jobs</li>
                    <li>Service Fees is &#8358; 5, 000</li>
                    <li>You pay chef directly</li>
                </ul>

                <form action="{{ url('subscription/package') }}" method="POST">
                    @csrf
                    <input type="hidden" name="package" value="Temporary One-Off Hiring">
                    <input type="hidden" name="name" value="$request['name']">
                    <input type="hidden" name="phone_number" value="$request['phone_number']">
                    <input type="hidden" name="email_address" value="$request['email_address']">
                    <input type="hidden" name="location" value="$request['location']">
                    <input type="hidden" name="chef_type" value="$request['chef_type']">
                    <input type="hidden" name="meal_type" value="$request['meal_type']">
                    <input type="hidden" name="description" value="$request['description']">
                    <input type="hidden" name="number_of_person" value="$request['number_of_person']">
                    <input type="hidden" name="engagement_type" value="$request['engagement_type']">
                    <input type="hidden" name="user_id" value="$request['user_id']">
                    <input type="hidden" name="price" value="5000">
                    <button class="btn btn-primary">
                        <span class="btn__text">
                            Subscribe Now
                        </span>
                    </button>
                </form>

		    </div>
		    <div class="col-md-6 text-center">
		        <h4>Full Term One-Off Hiring Engagement Service</h4>
		        <span class="h2">
		        	<span>&#8358;</span>25,000
		        	<small style="font-size: .4em;">vat inclusive</small>
		    	</span>
                <ul>
                    <li>A verified chef</li>
                    <li>Matched in 24 - 72 hours</li>
                    <li>Fit For Full Term Work</li>
                    <li>Service Fees is &#8358; 25, 000</li>
                    <li>You pay chef's salary directly</li>
                    <li>Access to Replacement Annually</li>
                </ul>

                <form action="{{ url('subscription/package') }}" method="POST">
                    @csrf
                    <input type="hidden" name="package" value="Full Term One-Off Hiring">
                    <input type="hidden" name="name" value="$request['name']">
                    <input type="hidden" name="phone_number" value="$request['phone_number']">
                    <input type="hidden" name="email_address" value="$request['email_address']">
                    <input type="hidden" name="location" value="$request['location']">
                    <input type="hidden" name="chef_type" value="$request['chef_type']">
                    <input type="hidden" name="meal_type" value="$request['meal_type']">
                    <input type="hidden" name="description" value="$request['description']">
                    <input type="hidden" name="number_of_person" value="$request['number_of_person']">
                    <input type="hidden" name="engagement_type" value="$request['engagement_type']">
                    <input type="hidden" name="user_id" value="$request['user_id']">
                    <input type="hidden" name="price" value="25000">
                    <button class="btn btn-primary">
                        <span class="btn__text">
                            Subscribe Now
                        </span>
                    </button>
                </form>
		    </div>
		</div>
	</div>

	<div class="row">
		<div class="text-center">
            <h3>Full Term Engagement Service</h3>
            <span class="h2">
		        <span>&#8358;</span>10,000
		        <small style="font-size: .4em;">vat inclusive</small>
		    </span>
        </div>
        <div class="col-md-12">
		    <div class="col-md-4 text-center">
		    	<h4>Monday To Friday</h4>
                <ul>
                    <li>&#8358; 40, 000 To &#8358; 80, 000 (Negotiable)</li>
                    <li>(10 hours Max)</li>
                </ul>

                <form action="{{ url('subscription/package') }}" method="POST">
                    @csrf
                    <input type="hidden" name="package" value="Monday - Friday">
                    <input type="hidden" name="name" value="$request['name']">
                    <input type="hidden" name="phone_number" value="$request['phone_number']">
                    <input type="hidden" name="email_address" value="$request['email_address']">
                    <input type="hidden" name="location" value="$request['location']">
                    <input type="hidden" name="chef_type" value="$request['chef_type']">
                    <input type="hidden" name="meal_type" value="$request['meal_type']">
                    <input type="hidden" name="description" value="$request['description']">
                    <input type="hidden" name="number_of_person" value="$request['number_of_person']">
                    <input type="hidden" name="engagement_type" value="$request['engagement_type']">
                    <input type="hidden" name="user_id" value="$request['user_id']">
                    <input type="hidden" name="price" value="10000">
                    <button class="btn btn-primary">
                        <span class="btn__text">
                            Subscribe Now
                        </span>
                    </button>
                </form>
		    </div>
		    <div class="col-md-4 text-center">
		    	<h4>Monday To Saturday</h4>
                <ul>
                    <li>&#8358; 60, 000 To &#8358; 120, 000 (Negotiable)</li>
                    <li>(10 hours Max)</li>
                </ul>

                <form action="{{ url('subscription/package') }}" method="POST">
                    @csrf
                    <input type="hidden" name="package" value="Monday - Saturday">
                    <input type="hidden" name="name" value="$request['name']">
                    <input type="hidden" name="phone_number" value="$request['phone_number']">
                    <input type="hidden" name="email_address" value="$request['email_address']">
                    <input type="hidden" name="location" value="$request['location']">
                    <input type="hidden" name="chef_type" value="$request['chef_type']">
                    <input type="hidden" name="meal_type" value="$request['meal_type']">
                    <input type="hidden" name="description" value="$request['description']">
                    <input type="hidden" name="number_of_person" value="$request['number_of_person']">
                    <input type="hidden" name="engagement_type" value="$request['engagement_type']">
                    <input type="hidden" name="user_id" value="$request['user_id']">
                    <input type="hidden" name="price" value="10000">
                    <button class="btn btn-primary">
                        <span class="btn__text">
                            Subscribe Now
                        </span>
                    </button>
                </form>
		    </div>
		    <div class="col-md-4 text-center">
		    	<h4>Monday To Sunday</h4>
                <ul>
                    <li>&#8358; 70, 000 To &#8358; 180, 000 (Negotiable)</li>
                    <li>(10 hours Max)</li>
                </ul>

                <form action="{{ url('subscription/package') }}" method="POST">
                    @csrf
                    <input type="hidden" name="package" value="Monday - Sunday">
                    <input type="hidden" name="name" value="$request['name']">
                    <input type="hidden" name="phone_number" value="$request['phone_number']">
                    <input type="hidden" name="email_address" value="$request['email_address']">
                    <input type="hidden" name="location" value="$request['location']">
                    <input type="hidden" name="chef_type" value="$request['chef_type']">
                    <input type="hidden" name="meal_type" value="$request['meal_type']">
                    <input type="hidden" name="description" value="$request['description']">
                    <input type="hidden" name="number_of_person" value="$request['number_of_person']">
                    <input type="hidden" name="engagement_type" value="$request['engagement_type']">
                    <input type="hidden" name="user_id" value="$request['user_id']">
                    <input type="hidden" name="price" value="10000">
                    <button class="btn btn-primary">
                        <span class="btn__text">
                            Subscribe Now
                        </span>
                    </button>
                </form>
		    </div>
        </div>
    </div>
</div>

@include("partials.footer") 
@endsection