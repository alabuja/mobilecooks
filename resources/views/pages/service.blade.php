@section('css')
    
    <link href="{{asset('css/pricing.css')}}" rel="stylesheet">
    @stop
<section id="services" class="service-item">
     <div class="container">
            <div class="center wow fadeInDown" id="underLine">
                <h3>How MobileCooks Works for Clients</h3>
                <hr align="center">
            </div>

            <div class="row">

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="media-body">
                            <!-- <h3 class="media-heading"><i class="fa fa-user-plus" aria-hidden="true"></i></h3> -->
                            <h3 class="media-heading">Specification & Convenience</h3>
                            <p>Specify your Request and Choose a convenient Plan that works for you</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="media-body">
                            <!-- <h3 class="media-heading"><i class="fa fa-check" aria-hidden="true"></i></h3> -->
                            <h3 class="media-heading">Chef Cooks In Your Comfort Zone</h3>
                            <p>You pay us our service fee upfront to mobilize the best chef to serve you within 24 - 72 hours, Your chef prepares your meals in and for your home, office, restaurant or event kitchen as you deem fit.</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="media-body">
                            <!-- <h3 class="media-heading"><i class="fa fa-money" aria-hidden="true"></i></h3> -->
                            <h3 class="media-heading">Cleanliness & Delivery</h3>
                            <p>Your Kitchen is left sparkling clean after every meal prepared and You pay the chef’s agreed a wages and or salaries through us, rate the chef and write reviews about chef.</p>
                        </div>
                    </div>
                </div>  
            </div><!--/.row-->
        </div><!--/.container-->

        <!-- For COok -->

        <div class="container">
            <div id="generic_price_table">   
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <!--PRICE HEADING START-->
                                <div class="price-heading clearfix" style="padding-top: 15px;">
                                    <h3>Full Term Engagement Service (Outsourcing)</h3>
                                </div>
                                <!--//PRICE HEADING END-->
                            </div>
                        </div>
                    </div>
                    <div class="container">
                
                        <!--BLOCK ROW START-->
                        <div class="row">
                            <div class="col-md-4">
                                <!--PRICE CONTENT START-->
                                <div class="generic_content clearfix">
                                    <!--HEAD PRICE DETAIL START-->
                                    <div class="generic_head_price clearfix">
                                        <!--HEAD CONTENT START-->
                                        <div class="generic_head_content clearfix">
                                            <!--HEAD START-->
                                            <div class="head_bg"></div>
                                            <div class="head">
                                                <span>Monday To Friday</span>
                                            </div>
                                            <!--//HEAD END-->
                                        </div>
                                        <!--//HEAD CONTENT END-->
                                        <!--PRICE START-->
                                        <div class="generic_price_tag clearfix">    
                                            <span class="price">
                                                <!-- <span class="sign">&#8358;</span>
                                                <span class="currency">10,000</span> -->
                                                <!-- <span class="month"><small>Service charge</small></span> -->
                                                <span class="month"><small>Call <b style="color: #c41c3d;">08135523685</b> for more information</small></span>
                                            </span>
                                        </div>
                                        <!--//PRICE END-->
                                    </div>                            
                                    <!--//HEAD PRICE DETAIL END-->
                            
                                    <!--FEATURE LIST START-->
                                    <div class="generic_feature_list">
                                        <ul>
                                            <!-- <li><span>&#8358; 45, 000 To &#8358; 80, 000</span> (Salary Negotiable)</li> -->
                                            <li><span>Salary is Negotiable</span></li>
                                            <li><span>9 hours for daily work</span> (Max)</li>
                                            <li><span>Pays Salary monthly via Mobilecook</span></li>
                                                <li><span>Service charge is required Upfront</span></li>
                                        </ul>
                                    </div>
                                    <!--//FEATURE LIST END-->
                            
                                    <!--BUTTON START-->
                                    <!-- <div class="generic_price_btn clearfix">
                                        <form action="{{ url('subscription/pricing') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="package" value="Monday To Friday">
                                            <input type="hidden" name="price" value="10000">
                                            <button class="btn btn-primary">
                                                <span class="btn__text">
                                                    Subscribe Now
                                                </span>
                                            </button>
                                        </form>
                                    </div> -->
                                    <!--//BUTTON END-->
                                </div>
                                <!--//PRICE CONTENT END-->
                            </div>
                    
                            <div class="col-md-4">
                                <!--PRICE CONTENT START-->
                                <div class="generic_content active clearfix">
                                    <!--HEAD PRICE DETAIL START-->
                                    <div class="generic_head_price clearfix">
                                        <!--HEAD CONTENT START-->
                                        <div class="generic_head_content clearfix">
                                            <!--HEAD START-->
                                            <div class="head_bg"></div>
                                            <div class="head">
                                                <span>Monday To Saturday</span>
                                            </div>
                                            <!--//HEAD END-->
                                        </div>
                                        <!--//HEAD CONTENT END-->
                                
                                        <!--PRICE START-->
                                        <div class="generic_price_tag clearfix">    
                                            <span class="price">
                                                <!-- <span class="sign">&#8358;</span>
                                                <span class="currency">15,000</span>
                                                <span class="month"><small>Service charge</small></span> -->
                                                <span class="month"><small>Call <b style="color: #c41c3d;">08135523685</b> for more information</small></span>
                                            </span>
                                        </div>
                                        <!--//PRICE END-->
                                    </div>                            
                                    <!--//HEAD PRICE DETAIL END-->
                            
                                    <!--FEATURE LIST START-->
                                    <div class="generic_feature_list">
                                        <ul>
                                            <!-- <li><span>&#8358; 65, 000 To &#8358; 120, 000</span> (Salary Negotiable)</li> -->
                                            <li><span>Salary is Negotiable</span></li>
                                            <li><span>9 hours for daily work</span> (Max)</li>
                                            <li><span>Pays Salary monthly via Mobilecook</span></li>
                                                <li><span>Service charge is required Upfront</span></li>
                                        </ul>
                                    </div>
                                    <!--//FEATURE LIST END-->
                            
                                    <!--BUTTON START-->
                                    <!-- <div class="generic_price_btn clearfix">
                                        <form action="{{ url('subscription/pricing') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="package" value="Monday To Saturday">
                                            <input type="hidden" name="price" value="15000">
                                            <button class="btn btn-primary">
                                                <span class="btn__text">
                                                    Subscribe Now
                                                </span>
                                            </button>
                                        </form>
                                    </div> -->
                                    <!--//BUTTON END-->
                                </div>
                                <!--//PRICE CONTENT END-->
                            </div>

                            <div class="col-md-4">
                                <!--PRICE CONTENT START-->
                                <div class="generic_content clearfix">
                                    <!--HEAD PRICE DETAIL START-->
                                    <div class="generic_head_price clearfix">
                                        <!--HEAD CONTENT START-->
                                        <div class="generic_head_content clearfix">
                                            <!--HEAD START-->
                                            <div class="head_bg"></div>
                                            <div class="head">
                                                <span>Monday To Sunday</span>
                                            </div>
                                            <!--//HEAD END-->
                                        </div>
                                        <!--//HEAD CONTENT END-->
                                
                                        <!--PRICE START-->
                                        <div class="generic_price_tag clearfix">    
                                            <span class="price">
                                                <!-- <span class="sign">&#8358;</span>
                                                <span class="currency">20,000</span>
                                                <span class="month"><small>Service charge</small></span> -->
                                                <span class="month"><small>Call <b style="color: #c41c3d;">08135523685</b> for more information</small></span>
                                            </span>
                                        </div>
                                        <!--//PRICE END-->
                                    </div>                            
                                    <!--//HEAD PRICE DETAIL END-->
                            
                                    <!--FEATURE LIST START-->
                                    <div class="generic_feature_list">
                                        <ul>
                                            <!-- <li><span>&#8358; 75, 000 To &#8358; 180, 000</span> (Salary Negotiable)</li> -->
                                            <li><span>Salary is Negotiable</span></li>
                                            <li><span>9 hours for daily work</span> (Max)</li>
                                            <li><span>Pays Salary monthly via Mobilecook</span></li>
                                                <li><span>Service charge is required Upfront</span></li>
                                        </ul>
                                    </div>
                                    <!--//FEATURE LIST END-->
                            
                                    <!--BUTTON START-->
                                    <!-- <div class="generic_price_btn clearfix">
                                        <form action="{{ url('subscription/pricing') }}" method="POST">
                                            @csrf
                                            <input type="hidden" name="package" value="Monday To Sunday">
                                            <input type="hidden" name="price" value="20000">
                                            <button class="btn btn-primary">
                                                <span class="btn__text">
                                                    Subscribe Now
                                                </span>
                                            </button>
                                        </form>
                                    </div> -->
                                    <!--//BUTTON END-->
                                </div>
                            <!--//PRICE CONTENT END-->
                            </div>
                        </div>  
                        <!--//BLOCK ROW END-->
                    </div>
                </section>
            </div>

            <div id="generic_price_table">   
                <section>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <!--PRICE HEADING START-->
                                <div class="price-heading clearfix" style="padding-top: 15px;">
                                    <h3>One-Off Hiring Engagement Service (Resourcing)</h3>
                                </div>
                                <!--//PRICE HEADING END-->
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="col-md-6">
                                    <!--PRICE CONTENT START-->
                                    <div class="generic_content clearfix">
                                        <!--HEAD PRICE DETAIL START-->
                                        <div class="generic_head_price clearfix">
                                            <!--HEAD CONTENT START-->
                                            <div class="generic_head_content clearfix">
                                                <!--HEAD START-->
                                                <div class="head_bg"></div>
                                                <div class="head">
                                                    <span>A day Service (8 hours)</span>
                                                </div>
                                                <!--//HEAD END-->
                                            </div>
                                            <!--//HEAD CONTENT END-->
                                            <!--PRICE START-->
                                            <div class="generic_price_tag clearfix">    
                                                <span class="price">
                                                    <!-- <span class="sign">&#8358;</span>
                                                    <span class="currency">5,000</span>
                                                    <span class="month"><small>Service charge</small></span> -->
                                                    <span class="month"><small>Call <b style="color: #c41c3d;">08135523685</b> for more information</small></span>
                                                </span>
                                            </div>
                                            <!--//PRICE END-->
                                        </div>                            
                                        <!--//HEAD PRICE DETAIL END-->
                                    
                                        <!--FEATURE LIST START-->
                                        <div class="generic_feature_list">
                                            <ul>
                                                <li><span>A verified chef</span></li>
                                                <li><span>Matched in 24 - 72 hours</span></li>
                                                <li><span>Fit For Short Term Jobs</span></li>
                                                <li><span>You pay chef A-day-wage directly</span></li>
                                                <li><span>Service charge is required Upfront</span></li>
                                            </ul>
                                        </div>
                                        <!--//FEATURE LIST END-->
                                    
                                        <!--BUTTON START-->
                                        <!-- <div class="generic_price_btn clearfix">
                                            <form action="{{ url('subscription/pricing') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="package" value="Temporary One-Off Hiring">
                                                <input type="hidden" name="price" value="5000">
                                                <button class="btn btn-primary">
                                                    <span class="btn__text">
                                                        Subscribe Now
                                                    </span>
                                                </button>
                                            </form>
                                        </div> -->
                                        <!--//BUTTON END-->
                                    </div>
                                    <!--//PRICE CONTENT END-->
                                </div>
                            
                                <div class="col-md-6">
                                    <!--PRICE CONTENT START-->
                                    <div class="generic_content clearfix">
                                        <!--HEAD PRICE DETAIL START-->
                                        <div class="generic_head_price clearfix">
                                            <!--HEAD CONTENT START-->
                                            <div class="generic_head_content clearfix">
                                                <!--HEAD START-->
                                                <div class="head_bg"></div>
                                                <div class="head">
                                                    <span>FullTime</span>
                                                </div>
                                                <!--//HEAD END-->
                                            </div>
                                            <!--//HEAD CONTENT END-->
                                        
                                            <!--PRICE START-->
                                            <div class="generic_price_tag clearfix">    
                                                <span class="price">
                                                    <!-- <span class="sign">&#8358;</span>
                                                    <span class="currency">35,000</span>
                                                    <span class="month"><small>Service charge</small></span> -->
                                                    <span class="month"><small>Call <b style="color: #c41c3d;">08135523685</b> for more information</small></span>
                                                </span>
                                            </div>
                                            <!--//PRICE END-->
                                        </div>                            
                                        <!--//HEAD PRICE DETAIL END-->
                                    
                                        <!--FEATURE LIST START-->
                                        <div class="generic_feature_list">
                                            <ul>
                                                <li><span>A verified chef</span></li>
                                                <li><span>Matched in 24 - 72 hours</span></li>
                                                <li><span>Fit For Full Term Work</span></li>
                                                <li><span>You pay chef directly</span></li>
                                                <li><span>Access to a replacement Annually</span></li>
                                                <li><span>Service charge is required Upfront</span></li>
                                            </ul>
                                        </div>
                                        <!--//FEATURE LIST END-->
                                    
                                        <!--BUTTON START-->
                                        <!-- <div class="generic_price_btn clearfix">
                                            <form action="{{ url('subscription/pricing') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="package" value="Full Term One-Off Hiring">
                                                <input type="hidden" name="price" value="35000">
                                                <button class="btn btn-primary">
                                                    <span class="btn__text">
                                                        Subscribe Now
                                                    </span>
                                                </button>
                                            </form>
                                        </div> -->
                                        <!--//BUTTON END-->
                                    </div>
                                <!--//PRICE CONTENT END-->
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div><!--/.container-->
    </section><!--/#services-->