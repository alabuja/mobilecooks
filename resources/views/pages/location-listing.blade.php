@extends('layouts.app')

@section('content')

@section('title')
	Chefs
@endsection
 	
	    <div class="container" id="chefs">
	      	<div class="row">

		        @if(count($chefs) > 0)
		          	@foreach($chefs as $i => $chef) 
		          		<div class="col-md-6 col-sm-6 col-xs-12">         
		          			<div class="chef_list_detail">
		          
						        <div data-wow-delay="0.{{$i}}s" class="strip_list wow fadeIn animated" style="visibility: visible; animation-delay: 0s; animation-name: fadeIn;">
						            <div class="row">         
							            <div class="col-md-8 col-sm-12">
							              	<div class="desc">
								                <div class="thumb_strip"> 
									                <a href="#">

									                	<img src="{{$chef->image_url or ''}}" alt="{{ $chef->lastname }}" >

									                </a>  
								                </div>         
								              	<h3>Chef {{ucwords($chef->lastname)}}</h3>
								              	<hr>
								              	<h4>Specialty</h4>
								              	<p class="">{{$chef->specialty}}</p>

									            <div class="rating"> 
									                @for($x = 0; $x < 5; $x++)
	                    
									                @if($x < $chef->review_avg)
									                  <i class="fa fa-star"></i>
									                @else
									                  <i class="fa fa-star fa fa-star-o"></i>
									                @endif
									                
									                @endfor
									                (<small><a href="{{URL::to('chef/'.$chef->id.'-'.$chef->slug)}}">Read {{\App\Review::getChefTotalReview($chef->id)}} reviews</a></small>)
									            </div>
							            	</div>
							            </div>
							            <div class="col-md-4 col-sm-12">
								            <div class="go_to">
								                <div>
								                	@if(Auth::check())
									                	<form >

    														<script src="https://js.paystack.co/v1/inline.js"></script>
									                		<input type="hidden" name="chef_id" id="chef_id" value="{{$chef->id}}">
									                		<input type="hidden" name="email" id="email" value="{{Auth::user()->email}}">
									                		<input type="hidden" name="amount" id="amount" value="150000">
									                		<input type="hidden" name="reference" id="reference" value="{{ Paystack::genTranxRef() }}">
            												<input type="hidden" name="key" id="key" value="{{ config('paystack.secretKey') }}">

            												<span id="payment_loader" style="display:none"><img src="{{url('img/ajax-loader.gif')}}" alt=""></span>

									                		<p><input type="button" id="paynow" class="btn_1" onclick="payWithPaystack()" value="Pay Now"/></p>
									                	</form> 
									                	<p>
									                		<a href="{{url('cart/'. $chef->id)}}"><i class="fa fa-plus-square-o"></i> Add to Cart</a>
									                	</p>
								                	@else
									                	<p><a class="btn_1" href="{{url('login')}}">Pay Now</a></p> 
									                	<p><a href="{{url('login')}}"><i class="fa fa-plus-square-o"></i> Add to Cart</a></p>
								                	@endif
								                	<a href="{{url('chef/'. $chef->id.'-'.$chef->slug)}}">View Review</a>
								                	
								                </div>
								            </div>
								        </div>
							        </div>
							    </div>
						    </div>
		    			</div>

					@endforeach

					{{$paginations->links()}}
				@else
					<h5 class="not_available">No Chef Available Now!!</h5>
		        @endif
		        	
		    </div>
	    </div>
@include("partials.footer") 
@endsection
