@extends('layouts.app')

@section('content')
@section('title')
My Bookings - {{ucwords(Auth::user()->firstname).' '.ucwords(Auth::user()->lastname)}}
@endsection

<div class="container" id="myOrder">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	<div class="panel">
                <div class="panel-body">
		            <table id="example2" class="table table-bordered table-striped">
			            <thead>
			                <tr>
			                    <th>S/N</th>
			                    <th>Chef Type</th>
			                    <th>Meal Type</th>
			                    <th>Engagement Type</th>
			                    <th>Package Type</th>
			                </tr>
			            </thead>
			            <tbody>
				            @foreach($bookings as $key => $booking)
				              	<tr>
					            	<td>{{ ++$key }}</td>
					            	<td>{{ $booking->chef_type }}</td>
					            	<td>{{ $booking->meal_type }}</td>
					            	<td>{{ $booking->engagement_type }}</td>
					            	<td>{{ $booking->package_type }}</td>
					            </tr>
				            @endforeach
			            </tbody>
			        </table>
			        {{$paginations->links()}}
		        </div>
		    </div>   	
        </div>
    </div>
</div>

@include("partials.footer") 
@endsection
