@extends('layouts.app')

@section('content')
@section('title')
My Cart
@endsection

<div class="container" id="myOrder">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

        	<div class="panel">
                <div class="panel-body">
                	@if(count($cart_items)> 0)
		            	<table id="example2" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                    <th>S/N</th>
				                    <th>Chef Name</th>
				                    <th>Amount</th>
				                    <th></th>

				                </tr>
				            </thead>
				            <tbody>
				            	@foreach($cart_items as $key => $cart_item)
				            		<tr>
				            			<td>{{++$key}}</td>
				            			<td>Chef {{$cart_item->chef->lastname}}</td>
				            			<td>&#8358 {{$cart_item->amount / 100}}</td>
				            			<td><a href="{{url('remove/'. $cart_item->id)}}"><i class="fa fa-minus-circle"></i></a></td>
				            		</tr>
				            	@endforeach
				            		<tr>
				            			<td></td>
				            			<td></td>
				            			<td class="pull-right">Your Total: &#8358  {{$sum}}</td>
				            			<td></td>
				            		</tr>
				            </tbody>
				        </table>
				        <a href="{{ url('chefs') }}" class="btn btn-primary">Continue</a>
				        <a href="{{ url('checkout') }}" class="btn btn-primary">Proceed to Checkout</a>
				    @else
			        	<h3>You have no items in your shopping cart</h3>
		            	<a href="{{ url('/chefs') }}" class="btn btn-primary">Continue</a>

		            @endif
		        </div>
		    </div>
            
        </div>
    </div>
</div>

@include("partials.footer") 
@endsection
