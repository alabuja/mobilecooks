@extends('layouts.app')

@section('content')
@section('title')
My Chefs - {{ucwords(Auth::user()->firstname).' '.ucwords(Auth::user()->lastname)}}
@endsection

<div class="container" id="myOrder">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	<div class="panel">
                @include("alerts")
                <div class="panel-body">
                	@if(count($payment) > 0) 
			            <table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
				            <thead>
				                <tr>
				                    <th>S/N</th>
				                    <th>Image</th>
				                    <th>First Name</th>
				                    <th>Lastname</th>
				                    <th>Address</th>
				                    <th>State Residence</th>	
				                    <th>View More</th>

				                </tr>
				            </thead>
				            <tbody>
					            @foreach($chefs as $key => $chef)
					              	<tr>
						            	<td>{{++$key}}</td>
						            	<td><img src="{{$chef->image_url or ''}}" alt="Chef {{ ucwords($chef->lastname) }}" ></td>
						            	<td>{{$chef->firstname}}</td>
						            	<td>{{$chef->lastname}}</td>
						            	<td>{{$chef->address}}</td>
						            	<td>{{$chef->state_residence}}</td>
						            	<td><a href="#" class="btn btn-success" data-toggle="modal" data-target="#commentModal{{$chef->id}}">View</a></td>
						            </tr>
					            @endforeach
				            </tbody>
				        </table>
				    @else
					    <div class="alert alert-success">You're not Entitled to view the contacts of all chefs. <a href="{{url('payment-bookings')}}" style="color: #c41c3d;">CLICK HERE</a> to know how to view the contacts of all chefs</div>
				    @endif

			        @foreach($chefs as $chef)
			        	<div class="modal fade" id="commentModal{{$chef->id}}" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel" aria-hidden="true">
                        	<div class="modal-dialog" role="document">
                            	<div class="modal-content">
                               		<div class="modal-header">
                                    	<button type="button" class="close" data-dismiss="modal">&times;</button>
                                    	<h4 class="modal-title">{{'Chef '.$chef->lastname}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p>
                                                	<a href="#">
                                                		<img src="{{$chef->image_url}}"
                                                                 alt="Image"
                                                                 style="max-width:100%;">
                                                    </a>
                                                </p>

                                                 <hr>
                                                 <div align="left">
                                                 	<b>Guarantor Name 2:</b> {{$chef->guarantor2_name ?? 'NIL'}}
                                                 </div>
                                                 <hr>
                                                 <div align="left">
                                                 	<b>Guarantor Phone Number 2:</b> {{$chef->guarantor2_phone_number ?? 'NIL' }}
                                                 </div>
                                                 <hr>
                                                 <div align="left">
                                                 	<b>Former Client Name:</b> {{$chef->client_name ?? 'NIL'}}
                                                 </div>
                                                 <hr>
                                                 <div align="left">
                                                 	<b>Former Client Phone Number:</b> {{$chef->client_phone_number ?? 'NIL'}}
                                                 </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="panel panel-body">
                                                    <div align="left">
                                                    	<b>Name:</b> {{$chef->lastname. ' '.$chef->firstname }}
                                                    </div>
                                                    <hr>
                                                    <div align="left">
                                                    	<b>Email Address:</b> {{$chef->email}}
                                                    </div>
                                                    <hr>
                                                    <div align="left">
                                                    	<b>Address: </b>{{$chef->address ?? 'NIL'}}
                                                    </div>
                                                    <hr>
                                                    <div align="left">
                                                    	<b>State of Residence: </b> 
                                                    	{{$chef->state_residence ?? 'NIL'}}
                                                    </div>
                                                    <hr>
                                                    <div align="left">
                                                    	<b>Phone Number:</b> {{$chef->phone_number ?? 'NIL'}}
                                                    </div>
                                                    <hr>
                                                    <div align="left">
                                                    	<b>Guarantor Name:</b> {{$chef->guarantor1_name ?? 'NIL'}}
                                                    </div>
                                                    <hr>
                                                    <div align="left">
                                                    	<b>Guarantor Phone Number:</b> {{$chef->guarantor1_phone_number ?? 'NIL'}}
                                                    </div>
                                                    <hr>
                                                    <div align="left">
                                                    	<b>Guarantor Phone Number:</b> {{$chef->guarantor1_phone_number ?? 'NIL'}}
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
			        @endforeach
		        </div>
		    </div>   	
        </div>
    </div>
</div>

@include("partials.footer") 

@endsection
