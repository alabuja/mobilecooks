@extends('layouts.app')

@section('content')

@section('title')
Dashboard - {{ucfirst(Auth::user()->firstname).' '.ucfirst(Auth::user()->lastname)}}
@endsection

<div class="container" id="dashboard">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel">
                <div class="panel-body">
                	<div class="col-md-6 text-center">
		        		<div id="dashboard-grey">
		        			<h5>My Profile</h5>
		        			<a class="btn btn-primary" href="{{url('profile')}}">My Profile</a>
		        		</div>
		        	</div>

		        	<div class="col-md-6 text-center">
		        		<div id="dashboard-grey">
		        			<h5>My Employed Chefs</h5>
			        		<a class="btn btn-primary" href="{{url('payment/chefs')}}">My Employed Chefs</a>
			        	</div>
		        	</div>

		        	<div class="col-md-6 text-center">
		        		<div id="dashboard-grey">
		        			<h5>View My Chefs</h5>
		        			<a class="btn btn-primary" href="{{url('payment/chefs')}}">View My Chefs</a>
		        		</div>
		        	</div>

		        	<div class="col-md-6 text-center">
		        		<div id="dashboard-grey">
		        			<h5>My Requests</h5>
		        			<a class="btn btn-primary" href="{{url('order')}}">My Requests</a>
		        		</div>
		        	</div>

		        	<div class="col-md-6 text-center">
		        		<div id="dashboard-grey">
		        			<h5>Hire Chef For Employment</h5>
		        			<a class="btn btn-primary" href="{{url('pricing')}}">Hire Chef For Employment</a>
		        		</div>
		        	</div>

		        	<div class="col-md-6 text-center">
		        		<div id="dashboard-grey">
		        			<h5>Plan Your Events</h5>
		        			<a class="btn btn-primary" href="{{url('event')}}">Plan Your Events</a>
		        		</div>
		        	</div>
		        </div>
		    </div>
        </div>
    </div>
</div>

@include("partials.footer") 
@endsection


