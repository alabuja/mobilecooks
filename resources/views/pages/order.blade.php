@extends('layouts.app')

@section('content')
@section('title')
Update Password - {{ucwords(Auth::user()->firstname).' '.ucwords(Auth::user()->lastname)}}
@endsection

<div class="container" id="myOrder">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	<div class="panel">
                <div class="panel-body">
                	@if(Auth::user()->verified == 0)
			        	<div class="alert alert-success">Kindly update your profile to view the profile of the chefs you've paid for. <a href="{{url('profile')}}" style="color: #c41c3d;">CLICK HERE</a></div>
			        @endif
		            <table id="example2" class="table table-bordered table-striped">
			            <thead>
			                <tr>
			                    <th>S/N</th>
			                    <th>First Name</th>
			                    <th>Lastname</th>
			                    <th>Email</th>
			                    <th>Phone Number</th>
			                    <th></th>
			                    <th></th>

			                </tr>
			            </thead>
			            <tbody>
			            @if(Auth::user()->verified == 0)
				        	<tr>
				        		
				        	</tr>
				        @else 
				            @foreach($orders as $key => $order)
				              	<tr>
					            	<td>{{++$key}}</td>
					            	<td>{{$order->chef->firstname}}</td>
					            	<td>{{$order->chef->lastname}}</td>
					            	<td>{{$order->chef->email}}</td>
					            	<td>{{$order->chef->phone_number}}</td>
					            	<td><a href="{{url('full-details/'. $order->chef->id)}}" class="btn btn-success">View Profile</a></td>
					            	<td><a href="" onclick="deleteChef('{{$order->id}}')" class="btn btn-danger"><i class="fa fa-trash-o"></i></a></td>
					            </tr>
				            @endforeach
			            @endif
			            </tbody>
			        </table>
			        {{$paginations->links()}}
		        </div>
		    </div>   	
        </div>
    </div>
</div>

@include("partials.footer") 
@endsection
