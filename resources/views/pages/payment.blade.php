@extends('layouts.app')

@section('content')

@section('Payment Page')
Pricing
@endsection

        <div class="container" style="margin-top: 160px;">
        	<section class="text-center">
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-10 col-md-8 col-md-offset-2" style="margin-bottom: 20px;">
		                    <h1 style="font-weight: 600; font-family: 'Open Sans', sans-serif;color:#4e4e4e;">Select Payment Method</h1>
		                    <hr>
		                </div>
		            </div>
		        </div>
		    </section>
            <div class="row">
                @include('alerts')
                <div class="col-sm-6 col-md-6 ">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Bank Payment</h4>
                        <span class="h1"><span class="pricing__dollar">&#8358;</span>{{ number_format($amount) }}</span>
                        <p>
                            <b>Zenith Bank</b><br>
                            <b>Account Name:</b> Audu Daniel <br>
                            <b>Account Number:</b> 2009886900
                        </p>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Card Payment</h4>
                        <span class="h1"><span class="pricing__dollar">&#8358;</span>{{ number_format($amount) }} +
                            <b style="font-size: 28px">&#8358;100</b></span>
                        <br><br>
                        <form>
                            @csrf
                            <script src="https://js.paystack.co/v1/inline.js"></script>
                            <input type="hidden" id="email" name="email" value="{{ $request['email'] }}">
                            <input type="hidden" id="name" name="name" value="{{ $request['name'] }}">
                            <input type="hidden" name="package_type" id="package_type" value="$request['package_type']">
                    		<input type="hidden" name="phone_number" id="phone_number" value="$request['phone_number']">
                    		<input type="hidden" name="location" id="location" value="$request['location']">
                    		<input type="hidden" name="chef_type" id="chef_type" value="$request['chef_type']">
                    		<input type="hidden" name="meal_type" id="meal_type" value="$request['meal_type']">
                    		<input type="hidden" name="description" id="description" value="$request['description']">
                    		<input type="hidden" name="number_of_person" id="number_of_person" value="$request['number_of_person']">
                    		<input type="hidden" name="engagement_type" id="engagement_type" value="$request['engagement_type']">
                    		<input type="hidden" name="booking_id" id="booking_id" value="$bookingID">
                    		<input type="hidden" name="user_id" id="user_id" value="$request['user_id']">

                            <input type="hidden" id="amount" name="amount" value="{{ $amount + 100 }}00">
                            <input type="hidden" name="quantity" value="">
                            <input type="hidden" id="reference" name="reference" value="{{ Paystack::genTranxRef() }}">
                            <input type="hidden" name="key" value="{{ config('paystack.secretKey') }}">
                            <span id="payment_subscription_loader" style="display:none"><img src="{{url('img/ajax-loader.gif')}}" alt=""></span>
                            <input type="button" id="paynow" class="btn_1" onclick="payWithPaystackSubscription()" value="Pay Via Card"/>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@include("partials.footer")
@endsection