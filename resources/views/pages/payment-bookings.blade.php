@extends('layouts.app')

@section('content')

@section('title')
Pricing
@endsection

	@section('css')
	
    <link href="{{asset('css/pricing.css')}}" rel="stylesheet">
    @stop

	<div class="container no-search-available" id="search">
		<div id="generic_price_table">   
			<section>
        		<div class="container">
            		<div class="row">
                		<div class="col-md-12">
                    		<!--PRICE HEADING START-->
                    		<div class="price-heading clearfix" style="padding-top: 15px;">
                    			<h3>Select Payment Plan</h3>
                    		</div>

                            <div class="col-md-11 col-md-offset-1">
                                <p style="font-weight: 600; font-size: 1.09em; text-align: justify; padding-bottom: 0.4rem;">These plans offers enterprises and households access to over 200 verified skillful professional chefs for the number of days they see fit to enjoy access to, chefs or cooks are ready to prepare whatever kind of meals you desire</p>
                            </div>
                    		<!--//PRICE HEADING END-->
                		</div>
            		</div>
        		</div>
        		<div class="container">
            
            		<!--BLOCK ROW START-->
            		<div class="row">
                		<div class="col-md-4">
                			<!--PRICE CONTENT START-->
                    		<a href="{{ url('book-now') }}">
                    			<div class="generic_content clearfix">
	                        		<!--HEAD PRICE DETAIL START-->
	                        		<div class="generic_head_price clearfix">
	                            		<!--HEAD CONTENT START-->
	                            		<div class="generic_head_content clearfix">
	                            			<!--HEAD START-->
	                                		<div class="head_bg"></div>
	                                		<div class="head">
	                                    		<span>7 Days Plan</span>
	                                		</div>
	                                		<!--//HEAD END-->
	                            		</div>
	                        		</div>
	                        		<div class="generic_feature_list">
	                        			<ul>
	                            			<li><span>&#8358; 5, 000.00 <b>ONLY</b></span> </li>
	                            		</ul>
	                        		</div>
	                        		<div class="generic_price_btn clearfix">
	                        			<form action="{{ url('book-now') }}" method="POST">
						                    @csrf
						                    <input type="hidden" name="booking_type" value="7 Days">
	                        				<button class="btn btn-primary">
						                        <span class="btn__text">
						                            Subscribe Now
						                        </span>
						                    </button>
						                </form>
	                        		</div>                         
	                        		<!--//HEAD PRICE DETAIL END-->
	                    		</div>
                    		</a>
                    		<!--//PRICE CONTENT END-->
                		</div>
                
                		<div class="col-md-4">
                			<!--PRICE CONTENT START-->
                    		<a href="{{ url('book-now') }}">
                    			<div class="generic_content active clearfix">
	                        		<!--HEAD PRICE DETAIL START-->
	                        		<div class="generic_head_price clearfix">
	                            		<!--HEAD CONTENT START-->
	                            		<div class="generic_head_content clearfix">
	                            			<!--HEAD START-->
	                                		<div class="head_bg"></div>
	                                		<div class="head">
	                                    		<span>14 Days Plan</span>
	                                		</div>
	                                		<!--//HEAD END-->
	                            		</div>
	                        		</div>
	                        		<div class="generic_feature_list">
	                        			<ul>
	                            			<li><span>&#8358; 10, 000.00 <b>ONLY</b></span> </li>
	                            		</ul>
	                        		</div>
	                        		<div class="generic_price_btn clearfix">
	                        			<form action="{{ url('book-now') }}" method="POST">
						                    @csrf
						                    <input type="hidden" name="booking_type" value="14 Days">
	                        				<button class="btn btn-primary">
						                        <span class="btn__text">
						                            Subscribe Now
						                        </span>
						                    </button>
						                </form>
	                        		</div>                        
	                        		<!--//HEAD PRICE DETAIL END-->
	                    		</div>
                    		</a>
                    		<!--//PRICE CONTENT END-->
                		</div>

                		<div class="col-md-4">
                			<div class="generic_content clearfix">
	                        	<!--HEAD PRICE DETAIL START-->
	                        	<div class="generic_head_price clearfix">
	                            	<!--HEAD CONTENT START-->
	                            	<div class="generic_head_content clearfix">
	                            		<!--HEAD START-->
	                                	<div class="head_bg"></div>
	                                	<div class="head">
	                                    	<span>30 Days Plan</span>
	                                	</div>
	                                	<!--//HEAD END-->
	                            	</div>
	                        	</div>
	                        	<div class="generic_feature_list">
	                        		<ul>
	                            		<li><span>&#8358; 20, 000.00 <b>ONLY</b></span> </li>
	                            	</ul>
	                        	</div>
	                        	<div class="generic_price_btn clearfix">
                        			<form action="{{ url('book-now') }}" method="POST">
					                    @csrf
					                    <input type="hidden" name="booking_type" value="30 Days">
                        				<button class="btn btn-primary">
					                        <span class="btn__text">
					                            Subscribe Now
					                        </span>
					                    </button>
					                </form>
                        		</div>
	                    	</div>
                		</div>
            		</div>
            		<!--//BLOCK ROW END-->
        		</div>
    		</section>
		</div>
	</div>

@include("partials.footer") 
@endsection
