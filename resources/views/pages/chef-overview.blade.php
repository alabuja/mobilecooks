@section('js')

<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
@stop

    <section class="pricing-page" id="services" >
        <div class="container">
            <div class="pricing-area">
                <div class="form-group row pull-right" style="margin-top: 10px;">
                    <div class="col-md-6 offset-md-4">
                        <a href="{{ url('payment-bookings') }}" class="btn btn-primary">
                                {{ __('View More') }}
                            </a>
                        </div>
                    </div>
                <div class="row">
                    
                	<div class="col-md-12">
                        <div id="carousel-example" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                @foreach($allChefs->chunk(4) as $key => $chefs)
                                    <div class="item {{ $key == 0 ? ' active' : '' }}">
                                        <div class="row">
                                            @foreach($chefs as $chef)
                                                <div class="col-sm-3">
                                                    <div class="col-item">
                                                        <div class="photo">
                                                            <img src="{{$chef->image_url}}" class="img-responsive" alt="{{ $chef->lastname }}" style="height: 260px;" />
                                                        </div>
                                                        <div class="info">
                                                            <div class="row">
                                                                <div class="price col-md-6">
                                                                    <h5>Chef {{ $chef->lastname }}</h5>
                                                                </div>
                                                                <div class="rating col-md-6">
                                                                    @for($x = 0; $x < 5; $x++)
                                    
                                                                    @if($x < $chef->review_avg)
                                                                      <i class="fa fa-star"></i>
                                                                    @else
                                                                      <i class="fa fa-star fa fa-star-o"></i>
                                                                    @endif
                                                                    
                                                                    @endfor
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        <!--.row-->
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                </div><!--/pricing-area-->
                
            </div>
        </div><!--/container-->
    </section><!--/pricing-page-->

    <style type="text/css">
            .col-item
            {
                border: 1px solid #E1E1E1;
                border-radius: 5px;
                background: #5bc475;
            }
            .col-item .photo img
            {
                margin: 0 auto;
                width: 100%;
            }

            .col-item .info
            {
                padding: 10px;
                border-radius: 0 0 5px 5px;
                margin-top: 1px;
            }

            .col-item .price
            {
                /*width: 50%;*/
                float: left;
                margin-top: 5px;
            }

            .col-item .price h5
            {
                line-height: 20px;
                margin: 0;
                color: #fff;
            }

            .price-text-color
            {
                color: #219FD1;
            }

            .col-item .info .rating
            {
                color: #777;
            }

            .col-item .rating
            {
                /*width: 50%;*/
                float: left;
                font-size: 17px;
                text-align: right;
                line-height: 52px;
                margin-bottom: 10px;
                height: 52px;
            }

            .col-item .separator
            {
                border-top: 1px solid #E1E1E1;
            }

            .clear-left
            {
                clear: left;
            }

            .col-item .separator p
            {
                line-height: 20px;
                margin-bottom: 0;
                margin-top: 10px;
                text-align: center;
            }

            .col-item .separator p i
            {
                margin-right: 5px;
            }
            .col-item .btn-add
            {
                width: 50%;
                float: left;
            }

            .col-item .btn-add
            {
                border-right: 1px solid #E1E1E1;
            }

            .col-item .btn-details
            {
                width: 50%;
                float: left;
                padding-left: 10px;
            }
            .controls
            {
                margin-top: 20px;
            }
            [data-slide="prev"]
            {
                margin-right: 10px;
            }
        </style>
