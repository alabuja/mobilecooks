@extends('layouts.app')

@section('content')

@section('Payment Page')
Pricing
@endsection

        <div class="container" style="margin-top: 160px;">
        	<section class="text-center">
		        <div class="container">
		            <div class="row">
		                <div class="col-sm-10 col-md-8 col-md-offset-2" style="margin-bottom: 20px;">
		                    <h1 style="font-weight: 600; font-family: 'Open Sans', sans-serif;color:#4e4e4e;">Select Payment Method</h1>
		                    <hr>
		                </div>
		            </div>
		        </div>
		    </section>
            <div class="row">
                @include('alerts')
                <div class="col-sm-5 col-md-5 ">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Bank Payment</h4>
                        <span class="h1"><span class="pricing__dollar">&#8358;</span>{{ number_format($amount) }}</span>
                        <p>
                            <b>Zenith Bank</b><br>
                            <b>Account Name:</b> Audu Daniel <br>
                            <b>Account Number:</b> 2009886900
                        </p>
                        <p><b>Kindly call  081-3552-3685 to confirm payment and get your access activated. </b></p>
                    </div>
                </div>
                <div class="col-sm-2 col-md-2">
                	<h1 style="color: #4e4e4e; font-weight: 600; font-family:'Open Sans', sans-serif; ">OR</h1>
                </div>
                <div class="col-sm-5 col-md-5">
                    <div class="pricing pricing-1 boxed boxed--border boxed--lg text-center">
                        <h4>Card Payment</h4>
                        <span class="h1"><span class="pricing__dollar">&#8358;</span>{{ number_format($amount) }} +
                            <b style="font-size: 28px">&#8358;100</b></span>
                        <br><br>
                        @if(Auth::check())
                        	<form action="{{ url('subscribe') }}" method="POST">
	                            @csrf
	                            <!-- <script src="https://js.paystack.co/v1/inline.js"></script> -->
	                            <input type="hidden" id="booking_type" name="booking_type" value="{{ $booking_type }}">
	                            <input type="hidden" id="email" name="email" value="{{ $email }}">
	                            <input type="hidden" id="amount" name="amount" value="{{ $amount + 100 }}00">
	                            <input type="hidden" id="hire_type" name="hire_type" value="payment_booking">
	                            <input type="hidden" name="metadata"
                                       value="{{ json_encode(['hire_type' => 'payment_booking']) }} ">
	                            <input type="hidden" id="reference" name="reference" value="{{ Paystack::genTranxRef() }}">
	                            <input type="hidden" id="key" name="key" value="{{ config('paystack.secretKey') }}">

	                            <input type="submit" id="paynow" class="btn_1" value="Pay Via Card"/>
	                        </form>
                        @else
                        	<p><a class="btn_1" href="{{url('login')}}">Pay Via Card</a></p> 
                        @endif
                    </div>
                </div>
            </div>
        </div>
@include("partials.footer")
@endsection