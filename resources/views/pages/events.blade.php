@extends('layouts.app')

@section('content')

@section('title')
Plan Your Events 
@endsection

	@section('css')
	<style type="text/css">
		#show_decoration_description, #show_mobility_description, #show_feeding_description, #show_location_description, #no_show_location_description, #show_event_location_description{
    		display: none;
    	}
	</style>
	@stop

<div class="container" id="profile">
    <div class="row">
    	<div class="col-md-10 col-md-offset-1" style="margin-bottom: 12px;">
            <p style="font-weight: 600; font-size: 1.09em; text-align: justify;">MOBILECOOKS is your first companion for planning the desired events of your dreams, this is because it  has partnered with a high impact team with a wealth of experience in event planning, decorations, master compere, quality food preparation whether indigenous or intercontinental dishes and transportation. We assure you quality delivery of the outcome of your events surrounding location, designs, mobility and feeding, as we plan to work with your budget be it a wedding ceremony, birthday ceremony, speaking engagements, fundraising, dinner nights, celebration parties, etc.</p>

        </div>
        <div class="col-md-10 col-md-offset-1">
            <div class="panel">
                @include("alerts")
                <div class="panel-body">
            		<h4><b>Kindly let us know how you want your event to be</b></h4><br>
                    <form method="POST" action="{{ url('events') }}" aria-label="Plan Your Events">
                        @csrf

                        <div class="form-group row required">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            @if(Auth::check())
                            	<div class="col-md-6">
	                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ Auth::user()->firstname .' '. Auth::user()->lastname  }}" required autofocus>

	                                @if ($errors->has('name'))
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $errors->first('name') }}</strong>
	                                    </span>
	                                @endif
	                            </div>

	                        @else
	                        	<div class="col-md-6">
	                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="" required autofocus>

	                                @if ($errors->has('name'))
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $errors->first('name') }}</strong>
	                                    </span>
	                                @endif
	                            </div>
                            @endif
                        </div>

                        @if(Auth::check())
	                        <div class="form-group row required">
	                            <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

	                            <div class="col-md-6">
	                                <input id="phone_number" type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" value="{{ Auth::user()->phone_number }}" required>

	                                @if ($errors->has('phone_number'))
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $errors->first('phone_number') }}</strong>
	                                    </span>
	                                @endif
	                            </div>
	                        </div>
	                    @else

	                    	<div class="form-group row required">
	                            <label for="phone_number" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>

	                            <div class="col-md-6">
	                                <input id="phone_number" type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" value="" required>

	                                @if ($errors->has('phone_number'))
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $errors->first('phone_number') }}</strong>
	                                    </span>
	                                @endif
	                            </div>
	                        </div>

	                    @endif

	                    @if(Auth::check())

	                        <div class="form-group row required">
	                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

	                            <div class="col-md-6">
	                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ Auth::user()->email }}">

	                                @if ($errors->has('email'))
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $errors->first('email') }}</strong>
	                                    </span>
	                                @endif
	                            </div>
	                        </div>

                        @else

                        	<div class="form-group row required">
	                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

	                            <div class="col-md-6">
	                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="">

	                                @if ($errors->has('email'))
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $errors->first('email') }}</strong>
	                                    </span>
	                                @endif
	                            </div>
	                        </div>

                        @endif

                        <div class="form-group row">
                            <label for="event_type" class="col-md-4 col-form-label text-md-right">{{ __('Event type') }}</label>

                            <div class="col-md-6">
                                <input id="event_type" type="text" class="form-control{{ $errors->has('event_type') ? ' is-invalid' : '' }}" placeholder="Wedding, birthday, etc" name="event_type"  >

                                @if ($errors->has('event_type'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('event_type') }}</strong>
                                    </span> 
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="budget" class="col-md-4 col-form-label text-md-right">{{ __('Budget') }}</label>

                            <div class="col-md-6">
								<input id="budget" type="text" class="form-control{{ $errors->has('budget') ? ' is-invalid' : '' }}" name="budget"  >

                                @if ($errors->has('budget'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('budget') }}</strong>
                                    </span> 
                                @endif
                            </div>
                        </div>

                        <div class="form-group row required">
                            <label for="date_of_event" class="col-md-4 col-form-label text-md-right">{{ __('Date of event') }}</label>

                            <div class="col-md-6">
								<input id="date_of_event" type="date" class="form-control{{ $errors->has('date_of_event') ? ' is-invalid' : '' }}" name="date_of_event" required>

                                @if ($errors->has('date_of_event'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('date_of_event') }}</strong>
                                    </span> 
                                @endif
                            </div>
                        </div>

                        <div class="form-group row required">
                        	<label for="number_of_person" class="col-md-4 col-form-label text-md-right"> {{ __('Estimated persons at event')}}</label>
                        	
                        	<div class="col-md-6">
                        		<input id="number_of_person" type="number" class="form-control{{ $errors->has('number_of_person') ? ' is-invalid' : '' }}" name="number_of_person" value="" min="1" required>

	                            @if ($errors->has('number_of_person'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('number_of_person') }}</strong>
	                                </span>
	                            @endif
                        	</div>
                        </div>

                        <div class="form-group row">
                        	<label for="decoration_service" class="col-md-4 col-form-label text-md-right"> {{ __('Would you be needing decoration services?')}}</label>
                        	
                        	<div class="col-md-6">
                        		<select class="form-control" name="decoration_service" onchange="showDecoration('show_decoration_description', this)" required>
			                        <option value="No">No</option>
					                <option value="Yes">Yes</option>
				                </select>

	                            @if ($errors->has('decoration_service'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('decoration_service') }}</strong>
	                                </span>
	                            @endif
	                        </div>
                        </div>

                        <div class="form-group row" id="show_decoration_description">
                        	<label for="decoration_description" class="col-md-4 col-form-label text-md-right"> {{ __('Describe the type of decoration Service') }}</label>
                        	<div class="col-md-6">
                        		<textarea id="decoration_description" name="decoration_description" class="form-control" rows="4"> </textarea>
	                        </div>
                        </div>

                        <!-- Mobility Service -->
                        <div class="form-group row">
                        	<label for="mobility_service" class="col-md-4 col-form-label text-md-right"> {{ __('Would you be needing mobility/transport service?')}}</label>
                        	
                        	<div class="col-md-6">
                        		<select class="form-control" name="mobility_service" onchange="showMobility('show_mobility_description', this)" required>
			                        <option value="No">No</option>
					                <option value="Yes">Yes</option>
				                </select>

	                            @if ($errors->has('mobility_service'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('mobility_service') }}</strong>
	                                </span>
	                            @endif
	                        </div>
                        </div>

                        <div class="form-group row" id="show_mobility_description">
                        	<label for="mobility_description" class="col-md-4 col-form-label text-md-right"> {{ __('Describe the type of Mobility Service') }}</label>
                        	<div class="col-md-6">
                        		<textarea id="mobility_description" name="mobility_description" class="form-control" rows="4"> </textarea>
	                        </div>
                        </div>

                        <!-- Feeding Service -->
                        <div class="form-group row">
                        	<label for="feeding_service" class="col-md-4 col-form-label text-md-right"> {{ __('Would you be needing feeding services?')}}</label>
                        	
                        	<div class="col-md-6">
                        		<select class="form-control" name="feeding_service" onchange="showFeeding('show_feeding_description', this)" required>
			                        <option value="No">No</option>
					                <option value="Yes">Yes</option>
				                </select>

	                            @if ($errors->has('feeding_service'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('feeding_service') }}</strong>
	                                </span>
	                            @endif
	                        </div>
                        </div>

                        <div class="form-group row" id="show_feeding_description">
                        	<label for="feeding_description" class="col-md-4 col-form-label text-md-right"> {{ __('Describe the type of Feeding Service') }}</label>
                        	<div class="col-md-6">
                        		<textarea id="feeding_description" name="feeding_description" class="form-control" rows="4"> </textarea>
	                        </div>
                        </div>

                        <!-- Location Service -->
                        <div class="form-group row">
                        	<label for="location_service" class="col-md-4 col-form-label text-md-right"> {{ __('Do you have a location for the event already?')}}</label>
                        	
                        	<div class="col-md-6">
                        		<select class="form-control" name="location_service" onchange="showLocation(this)" required>
			                        <option value="No">No</option>
					                <option value="Yes">Yes</option>
				                </select>

	                            @if ($errors->has('location_service'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('location_service') }}</strong>
	                                </span>
	                            @endif
	                        </div>
                        </div>

                        <div class="form-group row" id="show_location_description">
                        	<label for="location_description" class="col-md-4 col-form-label text-md-right"> {{ __('where?') }}</label>
                        	<div class="col-md-6">
                        		<textarea id="location_description" name="location_description" class="form-control" rows="4"> </textarea>
	                        </div>
                        </div>

                        <div class="form-group row" id="no_show_location_description">
                        	<label for="event_location_service" class="col-md-4 col-form-label text-md-right"> {{ __('Would you be needing an event-location services ?')}}</label>
                        	
                        	<div class="col-md-6">
                        		<select class="form-control" name="event_location_service" onchange="showEventLocation('show_event_location_description', this)" required>
			                        <option value="No">No</option>
					                <option value="Yes">Yes</option>
				                </select>

	                            @if ($errors->has('location_service'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('location_service') }}</strong>
	                                </span>
	                            @endif
	                        </div>
                        </div>

                        <div class="form-group row" id="show_event_location_description">
                        	<label for="event_location_description" class="col-md-4 col-form-label text-md-right"> {{ __('Around which location should we find a space, what kind of space should we find and what is your budget for the space?') }}</label>
                        	<div class="col-md-6">
                        		<textarea id="event_location_description" name="event_location_description" class="form-control" rows="4"> </textarea>
	                        </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Get Quote') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include("partials.footer") 

		@section('js')
    		<script type="text/javascript">
    			function showDecoration(divId, element)
    			{
    				document.getElementById(divId).style.display = element.value === 'Yes' ? 'block' : 'none';
    			}

    			function showMobility(divId2, element2) {
    				document.getElementById(divId2).style.display = element2.value === 'Yes' ? 'block' : 'none';
    			}

    			function showFeeding(divId3, element3) {
    				document.getElementById(divId3).style.display = element3.value === 'Yes' ? 'block' : 'none';
    			}
    			function showLocation(element4) {

    				if(element4.value === 'Yes'){
    					document.getElementById('show_location_description').style.display = 'block';
    					document.getElementById('no_show_location_description').style.display = 'none';
    				}

    				if(element4.value === 'No'){
    					document.getElementById('no_show_location_description').style.display = 'block';
    					document.getElementById('show_location_description').style.display = 'none';
    				}
    				// document.getElementById(divId4).style.display = element4.value === 'Yes' ? 'block' : 'none';
    			}

    			function showEventLocation(divId5, element5) {
    				document.getElementById(divId5).style.display = element5.value === 'Yes' ? 'block' : 'none';
    			}
    		</script>
   		@stop
@endsection
