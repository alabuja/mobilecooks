@extends("layouts.app")

@section("content")

@section('title')
Order Details
@endsection


 <div class="chef_list_detail" id="checkout">
    <div class="container">
      	<div class="row">
        	<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
          		<div class="box_style_2" id="order_process">
          			<form action="{{url('subscribe')}}" method="POST" id="order_details" role= "form">
          				@csrf

		          		<div class="form-group">
		            		<input type="hidden" class="form-control" id="firstname" name="firstname" value="{{$user->firstname}}" placeholder="Firstname">

		            		@if ($errors->has('firstname'))
		                		<span class="invalid-feedback" role="alert">
		                    		<strong>{{ $errors->first('firstname') }}</strong>
		                		</span>
		            		@endif
		          		</div>
	          			<div class="form-group">
	            			<input type="hidden" class="form-control" id="lastname" value="{{$user->lastname}}" name="lastname" placeholder="Lastname">
				            @if ($errors->has('lastname'))
				                <span class="invalid-feedback" role="alert">
				                    <strong>{{ $errors->first('lastname') }}</strong>
				                </span>
				            @endif
	          			</div>
	          			<div class="form-group">
	            			<input type="hidden" id="email" name="email" value="{{$user->email}}" class="form-control" placeholder="Your Email">
	            			@if ($errors->has('email'))
				                <span class="invalid-feedback" role="alert">
				                    <strong>{{ $errors->first('email') }}</strong>
				                </span>
				            @endif
	          			</div>
	          			<hr>

        		</div>
        	<!-- End box_style_1 -->   
		    	<div id="cart_box">
	          		<h3>Your order <i class="icon_cart_alt pull-right"></i></h3>
	          
	          		<table class="table table_summary">
	            		<tbody>
	              			@foreach($cart_items as $n=>$cart_item)
	              				<tr>
	                				<td>
	                					<a href="{{URL::to('remove/'.$cart_item->id)}}" class="remove_item"><i class="fa fa-minus-circle"></i>
	                					</a> Chef {{$cart_item->chef->lastname}} 
	                				</td>
	                				<td>
	                					<strong class="pull-right">&#8358 {{$cart_item->amount / 100}}</strong>
	                				</td>
	              				</tr>
	              				<input type="hidden" name="chef_id" value="{{$cart_item->id}}">
	              			@endforeach
	            		</tbody>
	          		</table>
          			<!-- Edn options 2 -->
          			<hr>
	          		@if($sum > 0)
	          		<table class="table table_summary">
	            		<tbody>
	              			<tr>
	                			<td class="total"> TOTAL 
	                				<span class="pull-right">&#8358 {{$price = $sum}}</span>
	                			</td>
	              			</tr>
	            		</tbody>
	          		</table>
	          		<hr>
	          		<div class="form-group row">
	          			<div class="col-md-4 ">
	          				<button type="submit" class="btn_full">Confirm Your Request</button>
	          			</div>
	          		</div>
        		</div>
        			<input type="hidden" name="reference" value="{{ Paystack::genTranxRef() }}">
            		<input type="hidden" name="key" value="{{ config('paystack.secretKey') }}">
        			<input type="hidden" name="amount" value="{{$amount}}">
          		</form>
          	@else
            <a class="btn_full" href="#">Empty Cart</a> </div>
          	@endif
        <!-- End cart_box -->
        </div>
                
      	</div>
    </div>
</div>
 
@include("partials.footer") 
@endsection
