@extends('layouts.app')

@section('content')
@section('title')
Single Chef Details
@endsection

<div class="container" id="myOrder">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        	<div class="panel">

                <div class="panel-body">
                	<table id="example2" class="table table-bordered table-striped">
			            <thead>
			            	
			            </thead>
			            <tbody>
			            	<div class="col-md-5">
			            		<tr><img src="{{$chef->image_url}}" style="border-radius: 10px; width: 125px; height: 125px;"></tr>
			            	</div>
			            	<p><br></p>
				            <tr>
					            <td><strong>Firstname:</strong> {{$chef->firstname}}</td>
					            <td><strong>Lastname:</strong> {{$chef->lastname}}</td>
					            <td><strong>Email:</strong> {{$chef->email}}</td>
					            <td><strong>Phone Number:</strong> {{$chef->phone_number}}</td>
					        </tr>

					        <tr>
					            <td><strong>Specialty:</strong> {{$chef->specialty}}</td>
					            <td><strong>State of Residence:</strong> {{$chef->state_residence}}</td>
					            <td><strong>State of Origin:</strong> {{$chef->state_origin}}</td>
					            <td><strong>Nationality:</strong> {{$chef->nationality}}</td>
					        </tr>

					        <tr>
					            <td><strong>Date of birth:</strong> {{$chef->date_of_birth}}</td>
					            <td><strong>Guarantor's Name:</strong> {{$chef->guarantor1_name}}</td>
					            <td><strong>Gurantor's Phone Number:</strong> {{$chef->guarantor1_phone_number}}</td>
					        </tr>

					        <tr>
					            <td><strong>Past Employer Name:</strong> {{$chef->client_name}}</td>
					            <td><strong>Past Employer Phone Number:</strong> {{$chef->client_phone_number}}</td>
					        </tr>
			            </tbody>
			        </table>
                </div>
            </div>
        </div>
    </div>
</div>

@include("partials.footer") 
@endsection
