@extends('layouts.app')

@section('content')
@section('title')
Thank You
@endsection

<div class="white_for_login">
    <div class="container margin_60">
      <div class="row">
        <div class="col-md-3"> </div>
        <div class="col-md-6">
          <div class="box_style_2">
              <h3>Thank you!</h3>
            </div>
            <h4>Summary</h4>
            <table class="table table-striped nomargin">
              <tbody>
                @foreach(\App\Cart::where('user_id',Auth::user()->id)->orderBy('id')->get() as $key => $cart_item)
                <tr>
                    <td>Chef {{$cart_item->chef->lastname}}</td>
                    <td><strong class="pull-right">&#8358 {{$cart_item->amount / 100}}</strong></td>
                </tr>
                
                     
                @endforeach

                <tr>
                  <td class="total_confirm"> TOTAL </td>
                  <td class="total_confirm"><span class="pull-right">&#8358 {{\App\Cart::sumCheckout()}}</span></td>
                </tr>
 
              </tbody>
            </table>
            <a href="{{url('order')}}" class="btn btn-submit">My Requests</a> 
            <div style="display:none;">{{\App\Cart::remove()}}</div>
          </div>
        </div>
        <div class="col-md-3"> </div>
      </div>
    </div>
  </div>

@include("partials.footer") 
@endsection