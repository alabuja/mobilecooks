@extends("layouts.app")
@section("content")
@section('title')
Home
@endsection
@include("partials.banner") 

<!-- Content ================================================== --> 
@include("pages.chef-overview")
@include("pages.service") 
@include("partials.footer") 
 
<!-- End section --> 
<!-- End Content =============================================== --> 

@endsection
