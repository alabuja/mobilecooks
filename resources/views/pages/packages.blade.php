    <section class="pricing-page">
        <div class="container">
            <div class="center">  
                <h2>Packages</h2>
                <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
            </div>  
            <div class="pricing-area text-center">
                <div class="row">

                	<div class="col-md-12">
                        <div class="col-md-8 col-md-offset-2">
                            @include("alerts")
                        </div>
                       {{-- @if(count($packages) > 0)
                            @foreach($packages as $package)
                                <div class="col-sm-4 plan price-one wow fadeInDown">
                                    <ul>
                                        <li class="heading-four">
                                                <h1>{{ $package->name or '' }} 
                                                </h1>
                                            <span>&#8358 {{ $package->amount or '' }}</span>
                                        </li>
                                        <li>{{ $package->number_of_cooks or '' }} @if($package->number_of_cooks > 1) 
                                                        Cooks
                                                    @else
                                                        Cook
                                                    @endif</li>
                                        <li>1GB Dadicated Ram</li>
                                        <li>10 Addon Domain</li>
                                        <li>10 Email Account</li>
                                        <li>24/7 Support</li> 
                                        <li class="plan-action">
                                            <a href="{{url('subscribe/'. $package->id.'-'.str_replace(' ', '-', strtolower($package->name)))}}" class="btn btn-primary">Subscribe to Package</a>
                                        </li>
                                    </ul>
                                </div>
                            @endforeach
                        @else
                            <h1>No Package Available Yet</h1>
                        @endif --}}
                    </div>
                </div><!--/pricing-area-->
            </div>
        </div><!--/container-->
    </section><!--/pricing-page-->