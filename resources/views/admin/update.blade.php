@extends('admin.layouts.app')
     
@section('content') 
				<!-- profile head start-->
            <!-- page head start-->
            <div class="page-head">
                <h3>
                    Change Password
                </h3>
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper">
                <!--state overview start-->
                <div class="col-md-11">
                	<div class="col-md-8 col-md-offset-2">
                		@include("alerts")
                	</div>
                	<div class="row state-overview">
                		<div class="col-md-8 col-md-offset-2">
                			<form method="POST" aria-label="Update Password">
		                        @csrf

		                        <div class="form-group row required">
		                            <label for="old" class="col-md-4 col-form-label text-md-right">{{ __('Current Password') }}</label>

		                            <div class="col-md-6">
		                                <input id="old" type="password" class="form-control{{ $errors->has('old') ? ' is-invalid' : '' }}" name="old" required>

		                                @if ($errors->has('old'))
		                                    <span class="invalid-feedback" role="alert">
		                                        <strong>{{ $errors->first('old') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>

		                        <div class="form-group row required">
		                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}</label>

		                            <div class="col-md-6">
		                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

		                                @if ($errors->has('password'))
		                                    <span class="invalid-feedback" role="alert">
		                                        <strong>{{ $errors->first('password') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>

		                        <div class="form-group row required">
		                            <label for="password_confirmation" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

		                            <div class="col-md-6">
		                                <input id="password_confirmation" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required>

		                                @if ($errors->has('password_confirmation'))
		                                    <span class="invalid-feedback" role="alert">
		                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
		                                    </span>
		                                @endif
		                            </div>
		                        </div>

		                        <div class="form-group row mb-0">
		                            <div class="col-md-6 offset-md-4">
		                                <button type="submit" class="btn btn-primary" style="color:#fff;background-color:#428bca;border-color:#357ebd;">
		                                    {{ __('Update Password') }}
		                                </button>
		                            </div>
		                        </div>
		                    </form>
                   		</div>
                	</div>
                </div>
                <!--state overview end-->
            </div>

            @include('admin.footer')
@endsection