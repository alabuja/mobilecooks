@extends('admin.layouts.app')
     
@section('content') 
				<!-- profile head start-->
            <!-- page head start-->
            <div class="page-head">
                <h3>
                    Verified Cooks
                </h3>
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper">
                <!--state overview start-->
                <div class="col-md-11">
                	<div class="col-md-8 col-md-offset-2">
                		@include("alerts")
                	</div>
                	<div class="row state-overview">
                		@if(count($verified_chefs) > 0)
	                    	<table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
	                        	<thead>
		                            <tr>
		                                <th>S/N</th>
		                                <th>Name</th> 
		                                <th>Address</th>  
		                                <th>Email</th>                                                 
		                                <th>Phone Number</th>                           
		                                <th>Image</th>                                                
		                                <th></th>                                               
		                                <th></th>                                               
		                                <th></th>                                               
		                                <th></th>                                               
		                            </tr>
	                            </thead>
	                            <tbody>

	                            	@foreach($verified_chefs as $key => $chef)
	                            		<tr>
	                            			<td>{{++$key}}</td>
	                            			<td>{{$chef->firstname.' '.$chef->lastname}}</td>
	                            			<td>{{$chef->address}}</td>
	                            			<td>{{$chef->email}}</td>
	                            			<td>{{$chef->phone_number or ''}}</td>
	                            			<td><img src="{{$chef->image_url}}" height="125px" width="125px"></td>
	                            			<td><a href="" class="btn btn-danger pull-right" onclick="deleteChef('{{$chef->id}}')">Delete</a></td>
	                            			<td><form action="{{url('admin/unverify-'.$chef->id.'-'.$chef->slug)}}" method="POST">
	                            				@csrf
	                            					<input class="hidden" name="verify" value="0">
	                            					<button class="btn btn-success" type="submit">Unverify</button>
	                            				</form>
	                            			</td>
	                            			<td><a href="{{url('admin/update/chef-'.$chef->id)}}" class="btn btn-info pull-right">Update Chef</a></td>

	                            			<td><a href="{{url('admin/chef-'.$chef->id)}}" class="btn btn-primary pull-right" style="color:#fff;background-color:#428bca;border-color:#357ebd;">View Profile</a></td>
	                            		</tr>
	                            	@endforeach

	                            </tbody>
	                        </table>
                        @else
                        	<h1>No Verified Chef Available</h1>
                        @endif
                	</div>
                </div>
                <!--state overview end-->
            </div>

            @include('admin.footer')
@endsection