@extends('admin.layouts.app')
     
@section('content') 
				<!-- profile head start-->
            <!-- page head start-->
            <div class="page-head">
                <h3>
                    Events
                </h3>
                <div class="state-information">
                </div>
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper">
                <!--state overview start-->
                <div class="col-md-12">
                	<div class="row state-overview">
                		@if(count($events) > 0)
	                    	<table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
	                            <thead>
	                            <tr>
	                                <th>S/N</th>
	                                <th>Name of User</th> 
	                                <th>Email</th>  
	                                <th>Phone Number</th>                                       
	                                <th>Event Type</th>                                       
	                                <th>Date of Event</th>                                      
	                                <th>Action</th>                                      
	                            </tr>
	                            </thead>
	                            <tbody>
	                            	@foreach($events as $key => $event)
	                            		<tr>
	                            			<td>{{++$key}}</td>
	                            			<td>{{$event->name}}</td>
	                            			<td>{{$event->email}}</td>
	                            			<td>{{$event->phone_number}}</td>
	                            			<td>{{$event->event_type}}</td>
	                            			<td>{{$event->date_of_event}}</td>
	                            			<td><a href="" data-toggle="modal" data-target="#event{{$event->id}}">View</a></td>
	                            		</tr>
	                            	@endforeach
	                            </tbody>
	                        </table>
	                    @else
                        	<h1>No Booking Made Yet</h1>
                        @endif

                	</div>
                </div>
                <!--state overview end-->
            </div>

            @foreach($events as $s)
                <div id="event{{$s->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="panel panel-default panel-body">
                                            <div align="center">Name: {{$s->name}}</div>
                                                <hr>
                                            <div align="center">Email: {{$s->email}}</div>
                                                <hr>
                                            <div align="center">Phone Number: {{$s->phone_number}}</div>
                                                <hr>
                                            <div align="center">Event Type: {{$s->event_type}}</div>
                                            <hr>
                                            <div align="center">Date of Event: {{$s->date_of_event}}</div>
                                            <hr>
                                            <div align="center">Budget: {{$s->budget}}</div>
                                            <hr>
                                            <div align="center">Number of Persons: {{$s->number_of_person}}</div>
                                            <hr>
                                            <div align="center">Decoration Service {{$s->decoration_service}}</div>
                                            <hr>
                                            <div align="center">Decoration Description: {{$s->decoration_description}}</div>
                                            <hr>
                                            <div align="center">Mobility Service: {{$s->mobility_service}}</div>
                                            <hr>
                                            <div align="center">Mobility Description: {{$s->mobility_description}}</div>
                                            <hr>
                                            <div align="center">Feeding Service: {{$s->feeding_service}}</div>
                                            <hr>
                                            <div align="center">Feeding Description: {{$s->feeding_description}}</div>
                                            <hr>
                                            <div align="center">Location Service: {{$s->location_service}}</div>
                                            <hr>
                                            <div align="center">Location Description: {{$s->location_description}}</div>
                                            <hr>
                                            <div align="center">Event Location Service: {{$s->event_location_service}}</div>
                                            <hr>
                                            <div align="center">Event Location Description: {{$s->event_location_description}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @include('admin.footer')
@endsection