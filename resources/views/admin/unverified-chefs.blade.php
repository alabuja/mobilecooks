@extends('admin.layouts.app')
     
@section('content') 
				<!-- profile head start-->
            <!-- page head start-->
            <div class="page-head">
                <h3>
                    Unverified Cooks
                </h3>
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper">
                <!--state overview start-->
                <div class="col-md-11">
                	<div class="col-md-8 col-md-offset-2">
                		@include("alerts")
                	</div>
                	<div class="row state-overview">
                		@if(count($unverified_chefs) > 0)
	                    	<table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
	                            <thead>
	                            <tr>
	                                <th>S/N</th>
	                                <th>Name</th> 
	                                <th>Address</th>  
	                                <th>Email</th>                                                 
	                                <th>Phone Number</th>                               
	                                <th>Image</th>                                               
	                                <th>Verify</th>                                                 
	                                <th></th>                                                 
	                                <th></th>                                                 
	                                <th></th>                                                 
	                            </tr>
	                            </thead>
	                            <tbody>
	                            	@foreach($unverified_chefs as $key => $chef)
	                            		<tr>
	                            			<td>{{++$key}}</td>
	                            			<td>{{$chef->firstname.' '.$chef->lastname}}</td>
	                            			<td>{{$chef->address}}</td>
	                            			<td>{{$chef->email}}</td>
	                            			<td>{{$chef->phone_number}}</td>
	                            			<td><img src="{{$chef->image_url}}" alt="{{$chef->firstname}}" style="width: 125px; height: 125px; border-radius: 10px;"></td>
	                            			<td><form action="{{url('admin/verify/'.$chef->id.'/'.$chef->slug)}}" method="POST">
	                            				@csrf
	                            					<input type="hidden" name="verify" value="1">
	                            					<button class="btn btn-success" type="submit">Verify</button>
	                            				</form>
	                            			</td>
	                            			<td><a href="" class="btn btn-danger pull-right" onclick="deleteChef('{{$chef->id}}')">Delete</a></td>

	                            			<td><a href="{{url('admin/update/chef-'.$chef->id)}}" class="btn btn-info pull-right">Update Chef</a></td>

	                            			<td><a href="{{url('admin/chef-'.$chef->id)}}" class="btn btn-primary pull-right" style="color:#fff;background-color:#428bca;border-color:#357ebd;">View Profile</a></td>
	                            		</tr>
	                            	@endforeach
	                            </tbody>
	                        </table>
	                    @else
                        	<h1>No Unverify Chef Available</h1>
                        @endif
                	</div>
                </div>
                <!--state overview end-->
            </div>

            @include('admin.footer')
@endsection