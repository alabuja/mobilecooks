@extends('admin.layouts.app')
     
@section('content') 
				<!-- profile head start-->
            <!-- page head start-->
            <div class="page-head">
                <h3>
                    Payments Bookings (Subscription Basics)
                </h3>
                <div class="state-information">
                </div>
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper">
                <!--state overview start-->
                <div class="col-md-11">
                	<div class="row state-overview">
                		@if(count($payments) > 0)
	                    	<table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
	                            <thead>
	                            <tr>
	                                <th>S/N</th>
	                                <th>Name of User</th> 
	                                <th>Amount</th>  
	                                <th>Booking Type</th>  
	                                <th>Payment Reference</th>                                       
	                            </tr>
	                            </thead>
	                            <tbody>
	                            	@foreach($payments as $key => $payment)
	                            		<tr>
	                            			<td>{{++$key}}</td>
	                            			<td>{{$payment->user->firstname .' '. $payment->user->lastname}}</td>
	                            			<td>&#8358 {{$payment->amount / 100}}</td>
	                            			<td>{{$payment->booking_type}}</td>
	                            			<td>{{$payment->payment_reference}}</td>
	                            		</tr>
	                            	@endforeach
	                            </tbody>
	                        </table>
	                    @else
                        	<h1>No Payment Made Yet</h1>
                        @endif

                	</div>
                </div>
                <!--state overview end-->
            </div>

            @include('admin.footer')
@endsection