@extends('admin.layouts.app')
     
@section('content') 

				<!-- profile head start-->
            <div class="profile-hero">
                <div class="profile-intro">
                    <img src="{{ Auth::guard('admin')->user()->avatar_url }}" alt="{{ Auth::guard('admin')->user()->username }}"/>
                    <h1>{{ ucwords(Auth::guard('admin')->user()->username) }}</h1>
                </div>
            </div>
            <!-- profile head end-->

            <!--body wrapper start-->
            <div class="wrapper no-pad">

            <div class="profile-desk">
            <aside class="p-aside">
                <section class="panel profile-info">
                    <p><strong>Username:: </strong> {{ Auth::guard('admin')->user()->username }} </p>
                    <p><strong>Email:: </strong> {{ Auth::guard('admin')->user()->email }} </p>
                </section>

                <div class="profile-timeline">

                </div>

            </aside>
            </div>

            </div>
            <!--body wrapper end-->

                @include('admin.footer')

@endsection