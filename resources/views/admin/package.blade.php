@extends('admin.layouts.app')
     
@section('content') 
				<!-- profile head start-->
            <!-- page head start-->
            <div class="page-head">
                <h3>
                    Packages We Offer
                </h3>
                <div class="state-information">
                    <a data-toggle="modal" href="#addPackage" class="btn btn-success m-t-10"><i class="fa fa-plus"></i> Add Package </a>
                </div>
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper">
                <!--state overview start-->
                <div class="col-md-11">
                	<div class="row state-overview">
                		@if(count($packages) > 0)
	                    	<table id="example2" class="table table-bordered table-striped">
	                            <thead>
	                            <tr>
	                                <th>S/N</th>
	                                <th>Name of Package</th> 
	                                <th>Amount</th>  
	                                <th>Number of Cooks</th>                                       
	                            </tr>
	                            </thead>
	                            <tbody>
	                            	@foreach($packages as $key => $package)
	                            		<tr>
	                            			<td>{{++$key}}</td>
	                            			<td>{{$package->name}}</td>
	                            			<td>&#8358 {{$package->amount}}</td>
	                            			<td>{{$package->number_of_cooks}}</td>
	                            		</tr>
	                            	@endforeach
	                            </tbody>
	                        </table>
	                        {{$paginations->links()}}
	                    @else
                        	<h1>No Package Available Yet</h1>
                        @endif

                	</div>
                </div>
                <!--state overview end-->
            </div>

            @include('admin.footer')
            @include('admin.modal.package')
@endsection