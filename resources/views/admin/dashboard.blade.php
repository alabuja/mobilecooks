@extends('admin.layouts.app')
     
@section('content') 			

 			<!-- page head start-->
            <div class="page-head">
                <h3>
                    Dashboard
                </h3>
                <span class="sub-title">Welcome to MobileCooks</span>
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper">
                <!--state overview start-->
                <div class="row state-overview">
                    <div class="col-lg-3 col-sm-6">
                        <section class="panel purple">
                            <div class="symbol">
                                <i class="fa fa-send"></i>
                            </div>
                            <a href="{{ url('admin/verified-users') }}">
                                <div class="value white">
                                    <h1 class="timer" data-from="0" data-to="{{$count_verified_users or ''}}"
                                        data-speed="1000">
                                        <!--320-->
                                    </h1>
                                    <p>Verified Users</p>
                                </div>
                            </a>
                        </section>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <section class="panel ">
                            <div class="symbol purple-color">
                                <i class="fa fa-tags"></i>
                            </div>
                            <a href="{{ url('admin/verified-chefs') }}">
                                <div class="value gray">
                                    <h1 class="purple-color timer" data-from="0" data-to="{{$count_verified_chefs or ''}}"
                                        data-speed="1000">
                                        <!--123-->
                                    </h1>
                                    <p>Verified Chefs</p>
                                </div>
                            </a>
                        </section>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <section class="panel green">
                            <div class="symbol ">
                                <i class="fa fa-cloud-upload"></i>
                            </div>
                            <a href="{{ url('admin/unverified-users') }}">
                                <div class="value white">
                                    <h1 class="timer" data-from="0" data-to="{{$count_unverified_users or ''}}"
                                        data-speed="1000">
                                        <!--432-->
                                    </h1>
                                    <p>Unverified Users</p>
                                </div>
                            </a>
                        </section>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <section class="panel">
                            <div class="symbol green-color">
                                <i class="fa fa-bullseye"></i>
                            </div>
                            <a href="{{ url('admin/unverified-chefs') }}">
                                <div class="value gray">
                                    <h1 class="green-color timer" data-from="0" data-to="{{$count_unverified_chefs or ''}}"
                                        data-speed="1000">
                                        <!--2345-->
                                    </h1>
                                    <p>Unverified Chefs</p>
                                </div>
                            </a>
                        </section>
                    </div>
                </div>
                <!--state overview end-->

                <!--state overview start-->
                <div class="row state-overview">
                    <div class="col-lg-3 col-sm-6">
                        <section class="panel purple">
                            <div class="symbol">
                                <i class="fa fa-send"></i>
                            </div>
                            <a href="{{ url('admin/order') }}">
                                <div class="value white">
                                    <h1 class="timer" data-from="0" data-to="{{$orders}}"
                                        data-speed="1000">
                                        <!--320-->
                                    </h1>
                                    <p>Orders</p>
                                </div>
                            </a>
                        </section>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <section class="panel ">
                            <div class="symbol purple-color">
                                <i class="fa fa-money"></i>
                            </div>
                            <a href="{{ url('admin/payment') }}">
                                <div class="value gray">
                                    <h1 class="purple-color timer" data-from="0" data-to="{{$payments}}"
                                        data-speed="1000">
                                        <!--123-->
                                    </h1>
                                    <p>Payments</p>
                                </div>
                            </a>
                        </section>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <section class="panel ">
                            <div class="symbol purple-color">
                                <i class="fa fa-money"></i>
                            </div>
                            <a href="{{ url('admin/subscriptions') }}">
                                <div class="value gray">
                                    <h1 class="purple-color timer" data-from="0" data-to="{{$bookings}}"
                                        data-speed="1000">
                                        <!--123-->
                                    </h1>
                                    <p>Subscriptions</p>
                                </div>
                            </a>
                        </section>
                    </div>
                    <div class="col-lg-3 col-sm-6">
                        <section class="panel ">
                            <div class="symbol purple-color">
                                <i class="fa fa-money"></i>
                            </div>
                            <a href="{{ url('admin/events') }}">
                                <div class="value gray">
                                    <h1 class="purple-color timer" data-from="0" data-to="{{$events}}"
                                        data-speed="1000">
                                        <!--123-->
                                    </h1>
                                    <p>Events</p>
                                </div>
                            </a>
                        </section>
                    </div>

                    <div class="col-lg-3 col-sm-6">
                        <section class="panel ">
                            <div class="symbol purple-color">
                                <i class="fa fa-money"></i>
                            </div>
                            <a href="{{ url('admin/payments/booking') }}">
                                <div class="value gray">
                                    <h1 class="purple-color timer" data-from="0" data-to="{{$paymentsBooking}}"
                                        data-speed="1000">
                                        <!--123-->
                                    </h1>
                                    <p>Payments Booking</p>
                                </div>
                            </a>
                        </section>
                    </div>
                </div>
            </div>

                @include('admin.footer')

@endsection