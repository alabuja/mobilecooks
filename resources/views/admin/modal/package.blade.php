<!-- Modal -->
                        <div class="modal fade" id="addPackage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">New Package</h4>
                                    </div>
                                    <div class="modal-body">
                                    	<form class="" id="PackageForm">
                        					@csrf

	                                        <div class="row">
	                                            <div class="form-group {{ $errors->has('name') ? 'has-error': ''}} required">
	                                                <label class="control-label col-md-4" for="name">Department Name</label>

	                                                <div class="col-md-8">
	                                                    <input size="16" type="text" value="{{old('name')}}" id="name" class="form-control" name="name" required>
	                                                    @if($errors->has('name'))
														    <span class="help-block">{{$errors->first('name') }}</span>
														@endif
	                                                </div>
	                                            </div>

	                                            <div class="form-group {{ $errors->has('amount') ? 'has-error': ''}} required">
	                                                <label class="control-label col-md-4" for="amount">Price </label>

	                                                <div class="col-md-8">
	                                                    <input size="16" type="number" value="{{old('amount')}}" id="amount" class="form-control" name="amount" required>
	                                                    @if($errors->has('amount'))
														    <span class="help-block">{{$errors->first('amount') }}</span>
														@endif
	                                                </div>
	                                            </div>

	                                            <div class="form-group {{ $errors->has('number_of_cooks') ? 'has-error': ''}} required">
	                                                <label class="control-label col-md-4" for="number_of_cooks">Number of Cooks</label>

	                                                <div class="col-md-8">
	                                                    <input size="16" type="number" value="{{old('number_of_cooks')}}" id="number_of_cooks" class="form-control" name="number_of_cooks" required>
	                                                    @if($errors->has('number_of_cooks'))
														    <span class="help-block">{{$errors->first('number_of_cooks') }}</span>
														@endif
	                                                </div>
	                                            </div>
	                                           
	                                      
	                                        </div>
	                                    
	                                    <span id="add_package_loader" style="display:none"><img src="{{ url('img/ajax-loader.gif') }}" alt=""></span>

	                                    <div class="modal-footer">
	                                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
	                                        <button type="submit" class="btn btn-success" id="btnPackage">New Package</button>
	                                    </div>
	                                </div>
                                	</form>
                                </div>
                            </div>
                        </div>
                        <!-- modal -->