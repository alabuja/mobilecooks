@extends('admin.layouts.app')
     
@section('content') 
				<!-- profile head start-->
            <!-- page head start-->
            <div class="page-head">
                <h3>
                    Orders
                </h3>
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper">
                <!--state overview start-->
                <div class="col-md-11">
                	<div class="row state-overview">
                		@if(count($orders) > 0)
	                    	<table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
	                            <thead>
	                            <tr>
	                                <th>S/N</th>
	                                <th>Name of User</th> 
	                                <th>Phone Number of User</th>
	                                <th>Name of Chef</th> 
	                                <th>Phone Number of Chef</th>                         
	                            </tr>
	                            </thead>
	                            <tbody>

	                            	@foreach($orders as $key => $order)
	                            		<tr>
	                            			<td>{{++$key}}</td>
	                            			<td>{{$order->user->firstname." ".$order->user->lastname}}</td>
	                            			<td>{{$order->user->phone_number or ''}}</td>
	                            			<td>{{$order->chef->firstname." ".$order->chef->lastname}}</td>
	                            			<td>{{$order->chef->phone_number }}</td>
	                            		</tr>
	                            	@endforeach
	                            </tbody>
	                        </table>
                        @else
                        	<h1>No Unverify User Available</h1>
                        @endif
                	</div>
                </div>
                <!--state overview end-->
            </div>

            @include('admin.footer')
@endsection