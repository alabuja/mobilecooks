<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="author" content="Alabuja Daniel" />
    <meta name="keyword" content="" />
    <meta name="description" content="" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="shortcut icon" href="javascript:;" type="image/png">

    <title>Mobile Cooks | Admin </title>

    <!-- Styles -->

    <!--right slidebar-->
    <link href="{{URL::asset('admin/css/slidebars.css')}}" rel="stylesheet">

    <!--jquery-ui-->
    <link href="{{URL::asset('admin/js/jquery-ui/jquery-ui-1.10.1.custom.min.css')}}" rel="stylesheet" />

    <!--iCheck-->
    <link href="{{URL::asset('admin/js/icheck/skins/all.css')}}" rel="stylesheet">

    <link href="{{URL::asset('admin/css/owl.carousel.css')}}" rel="stylesheet">
    <link href="{{URL::asset('admin/css/datatables.min.css')}}" rel="stylesheet">

    <!--common style-->
    <link href="{{URL::asset('admin/css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('admin/css/style-responsive.css')}}" rel="stylesheet">

</head>
<body class="sticky-header"> 
    <section>
        <!-- sidebar left start-->
        <div class="sidebar-left">
            <!--responsive view logo start-->
            <div class="logo dark-logo-bg visible-xs-* visible-sm-*">
                <a href="{{url('admin/home')}}">
                    <img src="#" alt="">
                    <!--<i class="fa fa-maxcdn"></i>-->
                    <span class="brand-name">MobileCooks</span>
                </a>
            </div>
            <!--responsive view logo end-->

            <div class="sidebar-left-info">
                <!-- visible small devices start-->
                <div class=" search-field">  </div>
                <!-- visible small devices end-->

                <!--sidebar nav start-->
                <ul class="nav nav-pills nav-stacked side-navigation">
                    <li>
                        <h3 class="navigation-title">Navigation</h3>
                    </li>
                    <li class="{{ Request::is('admin/home') ? 'active' : '' }}"><a href="{{ url('admin/home') }}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                    <li class="{{ Request::is('admin/verified-chefs') ? 'active' : '' }}"><a href="{{ url('admin/verified-chefs') }}"><i class="fa fa-home"></i> <span>Verified Chefs</span></a></li>

                    <li class="{{ Request::is('admin/verified-users') ? 'active' : '' }}"><a href="{{ url('admin/verified-users') }}"><i class="fa fa-home"></i> <span>Verified Users</span></a></li>

                    <li class="{{ Request::is('admin/order') ? 'active' : '' }}"><a href="{{ url('admin/order') }}"><i class="fa fa-home"></i> <span>Orders</span></a></li>

                    <li class="{{ Request::is('admin/payment') ? 'active' : '' }}"><a href="{{ url('admin/payment') }}"><i class="fa fa-money"></i> <span>Payments</span></a></li>
                    <li class="{{ Request::is('admin/payments/booking') ? 'active' : '' }}"><a href="{{ url('admin/payments/booking') }}"><i class="fa fa-money"></i> <span>Payments Bookings</span></a></li>
                    <li class="{{ Request::is('admin/subscriptions') ? 'active' : '' }}"><a href="{{ url('admin/subscriptions') }}"><i class="fa fa-money"></i> <span>Subscriptions</span></a></li>
                    <li class="{{ Request::is('admin/events') ? 'active' : '' }}"><a href="{{ url('admin/events') }}"><i class="fa fa-calender"></i> <span>Events</span></a></li>
                    <li class="{{ Request::is('admin/unverified-chefs') ? 'active' : '' }}"><a href="{{ url('admin/unverified-chefs') }}"><i class="fa fa-home"></i> <span>Unverified Chefs</span></a></li>
                    <li class="{{ Request::is('admin/unverified-users') ? 'active' : '' }}"><a href="{{ url('admin/unverified-users') }}"><i class="fa fa-home"></i> <span>Unverified Users</span></a></li>
                    <li class="{{ Request::is('admin/new/chefs') ? 'active' : '' }}"><a href="{{ url('admin/new/chefs') }}"><i class="fa fa-home"></i> <span>Add Chef</span></a></li>

                </ul>
                <!--sidebar nav end-->

            </div>
        </div>
        <!-- sidebar left end-->

        <!-- body content start--> 
        <div class="body-content" >

            <!-- header section start-->
            <div class="header-section">

                <!--logo and logo icon start-->
                <div class="logo dark-logo-bg hidden-xs hidden-sm">
                    <a href="{{url('admin/home')}}">
                        <!-- <img src="{{URL::asset('admin/img/logo-icon.png')}}" alt=""> -->
                        <!--<i class="fa fa-maxcdn"></i>-->
                        <span class="brand-name">MobileCooks</span>
                    </a>
                </div>

                <div class="icon-logo dark-logo-bg hidden-xs hidden-sm">
                    <a href="{{url('admin/home')}}">
                        MC
                    </a>
                </div>
                <!--logo and logo icon end-->

                <!--toggle button start-->
                <a class="toggle-btn"><i class="fa fa-outdent"></i></a>
                <!--toggle button end-->

                <!--mega menu end-->
                <div class="notification-wrap">

                <!--right notification start-->
                <div class="right-notification">
                    <ul class="notification-menu">
                        <!-- <li>
                            <form class="search-content" action="index.html" method="post">
                                <input type="text" class="form-control" name="keyword" placeholder="Search...">
                            </form>
                        </li> -->

                        <li>
                            <a href="javascript:;" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <img src="{{ Auth::guard('admin')->user()->avatar_url }}" alt="{{ Auth::guard('admin')->user()->username }}">{{ Auth::guard('admin')->user()->username }}
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu purple pull-right">
                                <li><a href="{{ url('admin/profile') }}">  Profile</a></li>
                                <li><a href="{{ url('admin/update') }}">  Change Password</a></li>
                                <li>
                                    <a href="{{ url('admin/logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="fa fa-sign-out pull-right"></i> Log Out
                                    </a>
                                    <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                    
                                </li>
                            </ul>
                        </li>
                        {{-- <li>
                            <div class="sb-toggle-right">
                                <i class="fa fa-indent"></i>
                            </div>
                        </li> --}}

                    </ul>
                </div>
                <!--right notification end-->
                </div>

            </div>
            <!-- header section end-->
    


        @yield('content')
           

        </div>
        <!--body wrapper end-->


        <!-- body content end-->
    </section>

    <!-- Scripts -->
<!-- Placed js at the end of the document so the pages load faster -->
<script src="{{URL::asset('admin/js/jquery-1.10.2.min.js')}}"></script>

<!--jquery-ui-->
<script src="{{URL::asset('admin/js/jquery-ui/jquery-ui-1.10.1.custom.min.js')}}" type="text/javascript"></script>

<script src="{{URL::asset('admin/js/jquery-migrate.js')}}"></script>
<script src="{{URL::asset('admin/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('admin/js/modernizr.min.js')}}"></script>

<!--Nice Scroll-->
<script src="{{URL::asset('admin/js/jquery.nicescroll.js')}}" type="text/javascript"></script>

<!--right slidebar-->
<script src="{{URL::asset('admin/js/slidebars.min.js')}}"></script>

<!--Icheck-->
<script src="{{URL::asset('admin/js/icheck/skins/icheck.min.js')}}"></script>
<script src="{{URL::asset('admin/js/datatables.min.js')}}"></script>
<script src="{{URL::asset('admin/js/datatables.script.js')}}"></script>

<!--jquery countTo-->
<script src="{{URL::asset('admin/js/jquery.countTo.js')}}"  type="text/javascript"></script>

<!--owl carousel-->
<script src="{{URL::asset('admin/js/owl.carousel.js')}}"></script>


<!--common scripts for all pages-->

<script src="{{URL::asset('admin/js/scripts.js')}}"></script>
<script src="{{URL::asset('admin/js/custom.js')}}"></script>
<!-- 
<script src="{{URL::asset('js/officials.js')}}"></script> -->
<script type="text/javascript">
    $(document).ready(function() {
        //countTo
        $('.timer').countTo();
    });

</script>

    @yield('js')

</body>
</html>
