@extends('admin.layouts.app')
     
@section('content') 
				<!-- profile head start-->
            <!-- page head start-->
            <div class="page-head">
                <h3>
                    Subscriptions
                </h3>
                <div class="state-information">
                </div>
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper">
                <!--state overview start-->
                <div class="col-md-12">
                	<div class="row state-overview">
                		@if(count($bookings) > 0)
	                    	<table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
	                            <thead>
	                            <tr>
	                                <th>S/N</th>
	                                <th>Name of User</th> 
	                                <th>Email</th>  
	                                <th>Phone Number</th>                                       
	                                <th>Location</th>                                       
	                                <th>Chef Type</th>                                      
	                                <th>Action</th>                                      
	                            </tr>
	                            </thead>
	                            <tbody>
	                            	@foreach($bookings as $key => $booking)
	                            		<tr>
	                            			<td>{{++$key}}</td>
	                            			<td>{{$booking->name}}</td>
	                            			<td>{{$booking->email_address}}</td>
	                            			<td>{{$booking->phone_number}}</td>
	                            			<td>{{$booking->location}}</td>
	                            			<td>{{$booking->chef_type}}</td>
	                            			<td><a href="" data-toggle="modal" data-target="#booking{{$booking->id}}">View</a></td>
	                            		</tr>
	                            	@endforeach
	                            </tbody>
	                        </table>
	                    @else
                        	<h1>No Booking Made Yet</h1>
                        @endif

                	</div>
                </div>
                <!--state overview end-->
            </div>

            @foreach($bookings as $s)
                <div id="booking{{$s->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="panel panel-default panel-body">
                                            <div align="center">Name: {{$s->name}}</div>
                                                <hr>
                                            <div align="center">Email: {{$s->email_address}}</div>
                                                <hr>
                                            <div align="center">Phone Number: {{$s->phone_number}}</div>
                                                <hr>
                                            <div align="center">Location: {{$s->location}}</div>
                                            <hr>
                                            <div align="center">Chef Type: {{$s->chef_type}}</div>
                                            <hr>
                                            <div align="center">Meal Type: {{$s->meal_type}}</div>
                                            <hr>
                                            <div align="center">Description: {{$s->description}}</div>
                                            <hr>
                                            <div align="center">Number Of Persons: {{$s->number_of_person}}</div>
                                            <hr>
                                            <div align="center">Engagement Type: {{$s->engagement_type}}</div>
                                            <hr>
                                            <div align="center">Package Type: {{$s->package_type}}</div>
                                            <hr>
                                            <div align="center">Booking Id: {{$s->booking_id}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @include('admin.footer')
@endsection