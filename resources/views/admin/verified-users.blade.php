@extends('admin.layouts.app')
     
@section('content') 
				<!-- profile head start-->
            <!-- page head start-->
            <div class="page-head">
                <h3>
                    Verified Users
                </h3>
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper">
                <div class="col-md-11">
                	<div class="row state-overview">
                		@if(count($verified_users) > 0)
	                    	<table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
	                            <thead>
		                            <tr>
		                                <th>S/N</th>
		                                <th>Name</th> 
		                                <th>Email</th> 
		                                <th>Phone Number</th>  
		                                <th>Valid ID</th>                                 
		                                <th>Address</th>                                                 
		                                <th>Verified</th>                                              
		                            </tr>
	                            </thead>
	                            <tbody>
	                            	@foreach($verified_users as $key => $user)
	                            		<tr>
	                            			<td>{{++$key}}</td>
	                            			<td>{{$user->firstname.' '.$user->lastname}}</td>
	                            			<td>{{$user->email}}</td>
	                            			<td>{{$user->phone_number or ''}}</td>
	                            			<td><a href="{{$user->file_url}}" download="download" target="_blank">Download</a></td>
	                            			<td>{{$user->address or ''}}</td>
	                            			<td><a href="#"><input type="checkbox" class="" name="verified" checked disabled></a></td>
	                            		</tr>
	                            	@endforeach
	                            </tbody>
	                        </table>
                        @else
                        	<h1>No Verified User Available</h1>
                        @endif
                	</div>
                </div>
                <!--state overview end-->
            </div>

            @include('admin.footer')
@endsection