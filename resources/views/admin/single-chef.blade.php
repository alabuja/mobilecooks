@extends('admin.layouts.app')
     
@section('content') 
				<!-- profile head start-->
            <!-- page head start-->
            <div class="page-head">
                <h3>
                    Single Chef
                </h3>
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper">
                <!--state overview start-->
                <div class="col-md-11">
                	<div class="row state-overview">
                		<div class="col-md-6 ">
                			<p><img src="{{$chef->image_url}}" alt="{{$chef->firstname}}" style="width: 125px; height: 125px; border-radius: 10px;"></p>
                			<p><strong>Name:: </strong>{{ucwords($chef->firstname.' '. $chef->lastname)}}</p>
                			<p><strong>Phone Number:: </strong>{{$chef->phone_number or ''}}</p>
                			<p><strong>Email Address:: </strong>{{$chef->email}}</p>
                			<p><strong>Home Address:: </strong>{{$chef->address or ''}}</p>
                			<p><strong>BVN:: </strong>{{$chef->bvn or ''}}</p>
                			<p><strong>Nationality:: </strong>{{$chef->nationality or ''}}</p>
                			<p><strong>State of Residence:: </strong>{{$chef->state_residence or ''}}</p>
                			<p><strong>State of Origin:: </strong>{{$chef->state_origin or ''}}</p>
                			<p><strong>Date of Birth:: </strong>{{$chef->date_of_birth or ''}}</p>
                			<p><strong>Dish Status Type:: </strong>{{$chef->meal_type or ''}}</p>
                			<p><strong>Specialty:: </strong>{{$chef->specialty or ''}}</p>
                		</div>
                		<div class="col-md-6">
                			<p><strong>Guarantor Name 1:: </strong>{{$chef->guarantor1_name or ''}}</p>
                			<p><strong>Guarantor Phone Number 1:: </strong>{{$chef->guarantor1_phone_number or ''}}</p>
                			<p><strong>Guarantor Address 1:: </strong>{{$chef->guarantor1_address or ''}}</p>
                			<p><strong>Guarantor Occupation 1:: </strong>{{$chef->guarantor1_occupation or ''}}</p>
                			<p><strong>Guarantor Name 2:: </strong>{{$chef->guarantor2_name or ''}}</p>
                			<p><strong>Guarantor Phone Number 2:: </strong>{{$chef->guarantor2_phone_number or ''}}</p>
                			<p><strong>Guarantor Address 2:: </strong>{{$chef->guarantor2_address or ''}}</p>
                			<p><strong>Guarantor Occupation 2:: </strong>{{$chef->guarantor2_occupation or ''}}</p>
                			<p><strong>Past Employer Name:: </strong>{{$chef->client_name or ''}}</p>
                			<p><strong>Past Employer Phone Number:: </strong>{{$chef->client_phone_number or ''}}</p>
                			<p><strong>Experience (Years) :: </strong>{{$chef->number_of_years or ''}}</p>
                			<p><strong>Average Review :: </strong>{{$chef->review_avg or ''}}</p>
                		</div>
                	</div>
                </div>
                <!--state overview end-->
            </div>

            @include('admin.footer')
@endsection