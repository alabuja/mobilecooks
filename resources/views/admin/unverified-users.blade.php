@extends('admin.layouts.app')
     
@section('content') 
				<!-- profile head start-->
            <!-- page head start-->
            <div class="page-head">
                <h3>
                    Unverified Users
                </h3>
            </div>
            <!-- page head end-->

            <!--body wrapper start-->
            <div class="wrapper">
                <!--state overview start-->
                <div class="col-md-11">
                	<div class="row state-overview">
                		<div class="col-md-8 col-md-offset-2">
                			@include("alerts")
                		</div>
                		@if(count($unverified_users) > 0)
	                    	<table id="zero_configuration_table" class="display table table-striped table-bordered" style="width:100%">
	                            <thead>
	                            <tr>
	                                <th>S/N</th>
	                                <th>Name</th> 
	                                <th>Email</th> 
	                                <th>Phone Number</th>  
	                                <th>Valid ID</th>                                                    
	                                <th>Address</th>                               
	                                <th>Verify</th>                              
	                            </tr>
	                            </thead>
	                            <tbody>

	                            	@foreach($unverified_users as $key => $user)
	                            		<tr>
	                            			<td>{{++$key}}</td>
	                            			<td>{{$user->firstname.' '.$user->lastname}}</td>
	                            			<td>{{$user->email}}</td>
	                            			<td>{{$user->phone_number or ''}}</td>
	                            			<td><a href="{{$user->file_url}}" download="download" target="_blank">Download</a></td>
	                            			<td>{{$user->address or ''}}</td>
	                            			<td><form action="{{url('admin/verify/'.$user->id)}}" method="POST">
	                            				@csrf
	                            					<input class="hidden" name="verify" value="1">
	                            					<button class="btn btn-success" type="submit">Verify</button>
	                            				</form>
	                            			</td>
	                            		</tr>
	                            	@endforeach
	                            </tbody>
	                        </table>
                        @else
                        	<h1>No Unverify User Available</h1>
                        @endif
                	</div>
                </div>
                <!--state overview end-->
            </div>

            @include('admin.footer')
@endsection