@extends('layouts.app')

@section('content')
@section('title')
  Login
@endsection

<div class="container" id="login">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-5">
                <div class="panel">
                    <div class="panel-heading">
                        <p style="font-size: 15px; font-weight: 600;">Login as a client to Discover an Awesome Experience With Professional Chefs </p>
                    </div>
                    <div class="panel-body">
                        <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-8">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-8">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login as a Client') }}
                                    </button>

                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        <i class="fa fa-lock"></i> {{ __('Forgot Password?') }}
                                    </a>

                                    <a class="btn btn-link" href="{{ url('register') }}">
                                        <i class="fa fa-sign-in" aria-hidden="true"></i> {{ __('Sign up as a Client') }}
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-md-1 loginOr">
                <h5 class="loginh5">OR</h5>
            </div>

            <div class="col-md-5">
                <div class="panel">
                    <div class="panel-heading">
                        <p style="font-size: 15px; font-weight: 600;">Showcase yourself as a skillful professional chef/cook, receive calls and mails for cooking opportunities.</p>
                    </div>
                    <div class="panel-body">
                        <form method="POST" action="{{ url('chef/chef_authenticate') }}" aria-label="{{ __('Login') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-8">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-8">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login as a Chef') }}
                                    </button>

                                    <a class="btn btn-link" href="{{ url('chef/password/reset') }}">
                                        <i class="fa fa-lock"></i> {{ __('Forgot Password?') }}
                                    </a>

                                    <a class="btn btn-link" href="{{ url('chef/register') }}">
                                        <i class="fa fa-sign-in" aria-hidden="true"></i> {{ __('Sign up as a Chef') }}
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include("partials.footer") 
@endsection
