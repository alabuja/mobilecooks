@extends('layouts.app')

@section('content')
@section('title')
    Investor's Relation
@endsection

	<!-- <section class="pricing-page"> -->
        <div class="container" id="investors">

            <div class="row">
            	<div class="col-md-6 col-md-offset-3">
            		<h2 style="margin-top: 20px; margin-bottom: 40px; font-size: 30px; text-transform: capitalize; text-align: center; font-weight: 700;">Investor’s Relation</h2>
            	</div>
            	<div class="col-md-12">
            		
            		<div class="col-md-6">
            			<p class="lead">
	            			We currently operate in Nigeria but we are in the very early stages of growing the business. If you are interested in what we are doing, kindly invest in our processes and business. <b>Please call us on 081-3552-3685 to further this discussion.</b>
	            		</p>
            		</div>

            		<div class="col-md-6">
            			<img src="{{asset('img/investors.jpg')}}" style="width: 100%;">
            		</div>
            	</div>
            </div>
        </div><!--/container-->
    <!-- </section> --><!--/pricing-page-->
@include("partials.footer") 
@endsection
