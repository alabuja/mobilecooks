@extends('layouts.app')

@section('content')

@section('title')
FAQ
@endsection

	<!-- <section class="pricing-page"> -->
        <div class="container" id="faq">

            <div class="row">
            	<div class="col-md-10 col-md-offset-1">
            		
            		<p><strong><h3>How Reliable & Trustworthy Are Your Cooks / Chefs?</h3></strong></p>
            		<p class="lead">They are recommended to us by our head chefs and cooks who are also vetted. Home owners also recommend their best cooks who they can trust to be on our platform.</p>

            		<p><strong><h3>Does MobileCook Work?</h3></strong></p>
            		<p class="lead">You can use our platform by paying a token of &#8358 1500 ONLY to view a vetted skillful chef / cook contact or use &#8358 5,000 to view all vetted chefs/ cooks on our platform for 30days. There is the visitation service that our verified, vetted & skillful handpicked chefs / cooks visits you at your home or office to prepare the best meal for you at affordable prices; for 8 visits per month is &#8358 40,000.00, 15 visits per month is &#8358 80,000.00, 25 visits per month is &#8358 100,000.00 while 30 visits per month  <a href="{{ url('pricing') }}">See Pricing for More</a></p>

            		{{-- <p><strong><h3>Can The Cook Travel With Me For a Cooking Job?</h3></strong></p>
            		<p class="lead">Yes of course! It is very possible, when the situation arises the available interested cook would be connected to discuss with customer on whatever arrangement is to be made.</p>

            		<p><strong><h3>How Do I Pay Cook?</h3></strong></p>
            		<p class="lead">After successful discussion with the cook on what is to be prepared. You get billed by the cook and you can negotiate with the cook if you like.</p>

            		<p><strong><h3>How can I Join The Mobile Cooks Network?</h3></strong></p>
            		<p class="lead">Click on “Become a Chef”, complete the application, ensure you give authentic information on your guarantor who knows you are a cook and information of client/company you have cooked for.</p>

            		<p><strong><h3>How Do I Carry My Cooking Services?</h3></strong></p>
            		<p class="lead">As a cook, you must have a white apron and its head gear when serving a client. A cook can get our Aprons and head gear from our shop at affordable prices.</p>

                    <p><strong><h3>Why We Collect Valid ID From Users?</h3></strong></p>
                    <p class="lead">We need to know the faces of users who are engaging the professional cooks on the platform and ensure that business is properly done in comfort & safety between user and the cooks, in other to ensure we protect the freelance cooks on this platform.</p> --}}

            	</div>
            </div>
        </div><!--/container-->
    <!-- </section> --><!--/pricing-page-->
@include("partials.footer") 
@endsection
