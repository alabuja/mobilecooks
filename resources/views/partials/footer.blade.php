 <div class="footer-bottom">
        <section id="bottom">
            <div class="container wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="600ms">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="widget">
                            <h3>About Us</h3>
                            <p>Mobilecooks.com.ng is an easy to use online platform that provides users with vetted skilled professional chefs to homes and businesses within short notice, also have notable, experienced experts on ground ready to provide event planning coupled with decorations, mobility, transportation management and meals preparation at events. </p>
                        </div>    
                    </div><!--/.col-md-3-->

                    <div class="col-md-4 col-sm-4">
                        <div class="widget">
                            <h3>Site Navigation</h3>
                            <ul>
                                <li><a href="{{url('login')}}"><i class="fa fa-user-plus" aria-hidden="true"></i> Get Started</a></li>
                                <li><a href="{{url('login')}}"><i class="fa fa-cutlery" aria-hidden="true"></i> Become a Chef</a></li>
                                <li><a href="{{url('faq')}}"><i class="fa fa-question" aria-hidden="true"></i> FAQS</a></li>
                                <li><a href="{{url('terms')}}"><i class="fa fa-info" aria-hidden="true"></i> Terms & Conditions</a></li>
                            </ul>
                        </div>    
                    </div><!--/.col-md-3-->

                    <div class="col-md-4 col-sm-4">
                        <div class="widget socials">
                            <h3>Connect With Us</h3>
                            <p><a href="{{ url('investors') }}">Investors</a></p>
                            <p><a href="{{ url('support') }}">Support</a></p>
                            <p>081-3552-3685</p>
                            <!-- <p><a href="mailto:info@mobilecooks.com.ng">info@mobilecooks.com.ng</a></p> -->
                            <ul>
                                <li><a href="https://www.facebook.com/mobilecooks"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://twitter.com/mobilecooksng"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="https://www.pinterest.com/mobilecooks"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                            
                        </div>    
                    </div><!--/.col-md-3-->
                </div>
            </div>
        </section><!--/#bottom-->
        <hr>
        <footer id="footer" class="midnight-blue">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 copyright">
                        Copyright &copy; 2015 - {{date('Y')}} <a target="_blank" href="{{url('/')}}" title="">MobileCooks</a>. All Rights Reserved.
                    </div>
                    <div class="col-sm-6">
                        <p class="pull-right">A Product of <a target="_blank" href="http://leadbitt.com.ng/">LEADBITT COMPANY</a> RC:1270218</p>
                    </div>
                </div>
            </div>
        </footer><!--/#footer-->
    </div>
