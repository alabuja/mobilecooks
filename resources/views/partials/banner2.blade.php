    <section id="main-slider" class="no-margin">
        <div class="carousel slide">
            
            <div class="carousel-inner">

                <div class="page_header2 active">
                    <div class="container">
                        <div class="row page_header">
                            <!-- <div class="col-sm-6"> -->
                                <div class="carousel-content">
                                    <h1 class="animation animated-item-1">@yield('sub-content')</h1>
                                    <p class="">@yield('description')</p>
                                    
                                    <!-- <a class="btn-slide animation animated-item-3" href="#">Read More</a> -->

                                    
                                </div>
                            <!-- </div> -->

                        </div>
                    </div>
                </div><!--/.item-->
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
        <!-- <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
            <i class="fa fa-chevron-left"></i>
        </a>
        <a class="next hidden-xs" href="#main-slider" data-slide="next">
            <i class="fa fa-chevron-right"></i>
        </a> -->
    </section><!--/#main-slider-->