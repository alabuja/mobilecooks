    <section id="main-slider" class="no-margin">
        <div class="carousel slide" data-ride="carousel">
            
            <div class="carousel-inner">

                <div class="item active" style="background-image:linear-gradient(0deg,rgba(255,0,150,0.3),rgba(255,0,150,0.3)), url(img/banner.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <!-- <div class="col-sm-6"> -->
                                <div class="carousel-content col-sm-12 col-md-8">
                                    <h1 class="animation animated-item-1">Hire Vetted Professional Chefs and <br> Event Planning Solutions for your Homes and <br> Enterprises in Real-time!</h1>
                                    
                                    <!-- <a class="btn-slide animation animated-item-3" href="#">Read More</a> -->

                                    <div class="search_container">
                                        <form action="{{url('payment-bookings')}}" method="GET" class="" id="search" role="form"> 
                                            <input type="search" placeholder="Search By Location, Name and Specialty" name="q" id="search">
                                            <button class="icon" type="submit"><i class="fa fa-search"></i></button>
                                        </form>
                                    </div>
                                    <p style="padding-top: 10px;"><i class="fa fa-phone"></i>  <!-- <i class="fa fa-whatsapp"></i> --> 081-3552-3685 <!-- or <i class="fa fa-envelope"></i> info@mobilecooks.com.ng --> </p>
                                </div>
                                <div class=" col-sm-12 col-md-4">
                                    <div class="media services-wrap"  id="mainSliderChef">
                                        <div class="">
                                            <h3 class="media-heading">Find Chefs ASAP on Chefs Village! </h3>
                                            <div id="dailyAccess">
                                                <span><i class="fa fa-check"></i> Access to over 200 chefs/cooks Weekly.</span><br />
                                                <span><i class="fa fa-check"></i> All Chefs/Cooks are <b>VETTED</b>.</span><br />
                                                <span><i class="fa fa-check"></i> All  Chefs/Cooks are <b>VERIFIED</b>.</span><br />
                                                <span><i class="fa fa-check"></i> All  Chefs/Cooks are <b>RELIABLE</b>.</span><br />
                                                <span><i class="fa fa-check"></i> All  Chefs/Cooks are <b>TRUSTED</b>.</span>
                                            </div>
                                            <br />
                                            <div id="dailyAccess">
                                                <span><strong>NGN 5,000 / 7 Days</strong></span><br/>
                                                <span><strong>NGN 10,000 / 14 Days</strong></span><br/>
                                                <span><strong>NGN 20,000 / 30 Days</strong></span>
                                            </div>
                                            <br /><br /><br />

                                                <span id="gainAccess"><a href="{{ url('payment-bookings') }}" class="btn btn-primary">GAIN ACCESS  <i class="fa fa-angle-right"></i> </a></span>
                                        </div>
                                    </div>
                                </div>
                            <!-- </div> -->

                        </div>
                    </div>
                </div><!--/.item-->
                <div class="item" style="background-image: linear-gradient(0deg,rgba(255,0,150,0.3),rgba(255,0,150,0.3)), url(img/main2.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <!-- <div class="col-sm-6"> -->
                                <div class="carousel-content col-sm-12 col-md-8">
                                    <h1 class="animation animated-item-1">Hire Vetted Professional Chefs and <br> Event Planning Solutions for your Homes and <br> Enterprises in Real-time!</h1>
                                    
                                    <!-- <a class="btn-slide animation animated-item-3" href="#">Read More</a> -->

                                    <div class="search_container">
                                        <form action="{{url('payment-bookings')}}" method="GET" class="" id="search" role="form"> 
                                            <input type="search" placeholder="Search By Location, Name and Specialty" name="q" id="search">
                                            <button class="icon" type="submit"><i class="fa fa-search"></i></button>
                                        </form>
                                    </div>
                                    <p style="padding-top: 10px;"><i class="fa fa-phone"></i>  <!-- <i class="fa fa-whatsapp"></i> --> 081-3552-3685 <!-- or <i class="fa fa-envelope"></i> info@mobilecooks.com.ng --> </p>
                                </div>
                                <div class=" col-sm-12 col-md-4">
                                    <div class="media services-wrap"  id="mainSliderChef">
                                        <div class="">
                                            <h3 class="media-heading">Find Chefs ASAP on Chefs Village! </h3>
                                            <div id="dailyAccess">
                                                <span><i class="fa fa-check"></i> Access to over 200 chefs/cooks Weekly.</span><br />
                                                <span><i class="fa fa-check"></i> All Chefs/Cooks are <b>VETTED</b>.</span><br />
                                                <span><i class="fa fa-check"></i> All  Chefs/Cooks are <b>VERIFIED</b>.</span><br />
                                                <span><i class="fa fa-check"></i> All  Chefs/Cooks are <b>RELIABLE</b>.</span><br />
                                                <span><i class="fa fa-check"></i> All  Chefs/Cooks are <b>TRUSTED</b>.</span>
                                            </div>
                                            <br />
                                            <div id="dailyAccess">
                                                <span><strong>NGN 5,000 / 7 Days</strong></span><br/>
                                                <span><strong>NGN 10,000 / 14 Days</strong></span><br/>
                                                <span><strong>NGN 20,000 / 30 Days</strong></span>
                                            </div>
                                            <br /><br /><br />

                                                <span id="gainAccess"><a href="{{ url('payment-bookings') }}" class="btn btn-primary">GAIN ACCESS  <i class="fa fa-angle-right"></i> </a></span>
                                        </div>
                                    </div>
                                </div>
                            <!-- </div> -->

                        </div>
                    </div>
                </div><!--/.item-->
                <div class="item" style="background-image: linear-gradient(0deg,rgba(255,0,150,0.3),rgba(255,0,150,0.3)), url(img/chef.jpg)">
                    <div class="container">
                        <div class="row slide-margin">
                            <!-- <div class="col-sm-6"> -->
                                <div class="carousel-content col-sm-12 col-md-8">
                                    <h1 class="animation animated-item-1">Gain Access To Vetted Professional Chefs and <br> Event Planning Solutions for your Homes and <br> Enterprises in Real-time!</h1>
                                    
                                    <!-- <a class="btn-slide animation animated-item-3" href="#">Read More</a> -->

                                    <div class="search_container">
                                        <form action="{{url('payment-bookings')}}" method="GET" class="" id="search" role="form"> 
                                            <input type="search" placeholder="Search By Location, Name and Specialty" name="q" id="search">
                                            <button class="icon" type="submit"><i class="fa fa-search"></i></button>
                                        </form>
                                    </div>
                                    <p style="padding-top: 10px;"><i class="fa fa-phone"></i>  <!-- <i class="fa fa-whatsapp"></i> --> 081-3552-3685 <!-- or <i class="fa fa-envelope"></i> info@mobilecooks.com.ng --> </p>
                                </div>
                                <div class=" col-sm-12 col-md-4">
                                    <div class="media services-wrap"  id="mainSliderChef">
                                        <div class="">
                                            <h3 class="media-heading">Find Chefs ASAP on Chefs Village! </h3>
                                            <div id="dailyAccess">
                                                <span><i class="fa fa-check"></i> Access to over 200 chefs/cooks Weekly.</span><br />
                                                <span><i class="fa fa-check"></i> All Chefs/Cooks are <b>VETTED</b>.</span><br />
                                                <span><i class="fa fa-check"></i> All  Chefs/Cooks are <b>VERIFIED</b>.</span><br />
                                                <span><i class="fa fa-check"></i> All  Chefs/Cooks are <b>RELIABLE</b>.</span><br />
                                                <span><i class="fa fa-check"></i> All  Chefs/Cooks are <b>TRUSTED</b>.</span>
                                            </div>
                                            <br />
                                            <div id="dailyAccess">
                                                <span><strong>NGN 5,000 / 7 Days</strong></span><br/>
                                                <span><strong>NGN 10,000 / 14 Days</strong></span><br/>
                                                <span><strong>NGN 20,000 / 30 Days</strong></span>
                                            </div>
                                            <br /><br /><br />

                                                <span id="gainAccess"><a href="{{ url('payment-bookings') }}" class="btn btn-primary">GAIN ACCESS  <i class="fa fa-angle-right"></i> </a></span>
                                        </div>
                                    </div>
                                </div>
                            <!-- </div> -->

                        </div>
                    </div>
                </div><!--/.item-->
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
        <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
            <i class="fa fa-chevron-left"></i>
        </a>
        <a class="next hidden-xs" href="#main-slider" data-slide="next">
            <i class="fa fa-chevron-right"></i>
        </a>
    </section><!--/#main-slider-->

    <style type="text/css">
        #mainSliderChef{
            min-height: 350px;
        }
        #mainSliderChef h3 {
            font-size: 1.7rem;
            font-weight: 600;
            color: #fff;

        }
        .fa-check{
            font-size: 2.0rem;
            color: #fff;
        }
        ul li {
            letter-spacing: 0.0625rem;
            padding-top: 0.09rem;
        }
        #dailyAccess{
            color: #fff;
            font-size: 1.8rem;
            font-weight: 400;
            font-family: 'Open Sans', sans-serif;
        }
        #gainAccess{
            font-size: 2.0rem;
            font-family: 'Open Sans', sans-serif;
            color: #fff;
        }
        #mainSliderChef{
            background: #2ECC71;
            border-radius: 30px;
        }

        #dailyAccess span{
            color: #fff;
        }

        #gainAccess a{
            color: #fff; 
        }
        
    </style>