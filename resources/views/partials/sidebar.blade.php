<div class="col-md-3 col-sm-5 col-xs-12 side-bar">   
        
    <div id="cart_box">
      	<h3>Your Requests <i class="fa fa-shopping-cart pull-right"></i></h3>
      	<table class="table table_summary">
	      <tbody>
	      </tbody>
      	</table>  
      	@foreach($cart_items as $n=>$cart_item)
            <table class="table table_summary">
              	<tbody>
              		<tr>
                		<td><a href="{{URL::to('delete_item/'.$cart_item->id)}}" class="remove_item"><i class="fa fa-minus-circle"></i></a> {{$cart_item->item_name}} </td>
                		<td><strong class="pull-right">&#8358 {{$cart_item->amount}}</strong></td>
              		</tr>
             	</tbody>
            </table> 
      	@endforeach    
        
      	<hr>
        @if($sum > 0)
      		<table class="table table_summary">
        		<tbody>
        			<tr>
          				<td class="total"> TOTAL 
          					<span class="pull-right"> 
          						{{$price = $sum}}
          					</span>
            			</td>
        			</tr>
        		</tbody>
      		</table>
      	<hr>
          <a class="btn_full" href="{{url('get')}}">Book Now</a>  
          @else
            <a class="btn_full" href="#0">Empty Cart</a>  
          @endif
    </div>  

</div>