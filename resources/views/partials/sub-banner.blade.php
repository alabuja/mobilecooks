<div class="sub-banner">
    <div class="overlay">
      <div class="container">
        <div id="sub_content" class="animated zoomIn">
    <div class="col-md-2 col-sm-3">
      <div id="thumb"><img alt="{{$chef->firstname}}" src="{{$chef->image_url}}"></div>
    </div>  
    <div class="col-md-10 col-sm-9">  
      <h1>{{$chef->firstname}} {{$chef->lastname}}</h1>
      <div class="sub_cont_lt"><i class="fa fa-map-marker"></i> {{$chef->address}}</div>
      <div class="rating"> 
        @for($x = 0; $x < 5; $x++)
                  
              @if($x < $chef->review_avg)
                <i class="fa fa-star"></i>
              @else
                <i class="fa fa-star fa fa-star-o"></i>
              @endif
             
              @endfor
              (<small><a href="#0">Read {{$total_reviews}} Reviews</a></small>)

        
      </div>
      </div>
    </div>
      </div>
    </div>
  </div>