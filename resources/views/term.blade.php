@extends('layouts.app')

@section('content')
@section('title')
    Terms & Conditions
@endsection

	<!-- <section class="pricing-page"> -->
        <div class="container" id="terms">

            <div class="row">
            	<div class="col-md-11 col-md-offset-1">
            		
            		<p class="lead">All registered cooks under this platform avails themselves any cooking job.</p>
            		<p class="lead">All Cooks must be transparent, having a clean conscience in dealing with clients.</p>
            		<p class="lead">Cooks with hidden intentions and fraudulent acts, would be removed from network and handled as a criminal case.</p>
            		<p class="lead">All cooks details are opened to clients who may show interest to employ them.</p>
            		<p class="lead">The platform supports recruitment of cooks for companies and households.</p>
            		<p class="lead">At no point should information be copied or transfered from this platform without proper consent from owners.</p>
            		<p class="lead">Any form of harassment either physical, verbal or sexual to any the cooks by any client making use of our services would not be tolerated.</p>
            		<p class="lead">Any breach in terms and conditions would attract fine or litigation.</p>
                    <p class="lead">
                        All cooks on this platform are independent freelance contractors who carry their businesses independently. Mobile cooks.com.ng shall not be held for any fall away relationship, damages or negligence from the cook. We are simply offering a platform that would help companies and families connect to freelance professional cooks who have being verified and vise versa.
                    </p>
                    <p class="lead">
                        
                    </p>
            	</div>
            </div>
        </div><!--/container-->
    <!-- </section> --><!--/pricing-page-->
@include("partials.footer") 
@endsection
