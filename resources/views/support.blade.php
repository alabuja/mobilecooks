@extends('layouts.app')

@section('content')
@section('title')
    Support
@endsection

	<!-- <section class="pricing-page"> -->
        <div class="container" id="investors">

            <div class="row">
            	<div class="col-md-6 col-md-offset-3">
            		<h2 style="margin-top: 20px; margin-bottom: 40px; font-size: 30px; text-transform: capitalize; text-align: center; font-weight: 700;">Support</h2>
            	</div>
            	<div class="col-md-12">
            		
            		<div class="col-md-6">
            			<p class="lead">
	            			Keeping our circles of professional chefs and cooks, is a bit difficult but with your support in connecting us with the right set of professional chefs we can keep building a healthy database of the best chefs and cooks character-wise and skill-set wise. 
	            		</p>
            		</div>
            		<div class="col-md-6">
            			<img src="{{asset('img/support.jpg')}}" style="width: 100%;">
            		</div>
            		
            	</div>
            </div>
        </div><!--/container-->
    <!-- </section> --><!--/pricing-page-->
@include("partials.footer") 
@endsection
