@extends('layouts.app')

@section('content')
@section('title')
	Become a Mobilecook
@endsection

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel">
            	@include("alerts")
                <div class="panel-body">
                    <form method="POST" aria-label="Become a Mobilecook">
                        @csrf

                        <div class="form-group row col-md-12">
                        	<div class="col-md-6 required">
	                            <label for="firstname" class="col-form-label text-md-right">{{ __('Firstname') }}</label>
	                            <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}" required autofocus>

	                            @if ($errors->has('firstname'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('firstname') }}</strong>
	                                </span>
	                            @endif
	                        </div>

	                        <div class="col-md-6 required">
	                            <label for="lastname" class="col-form-label text-md-right">{{ __('Lastname') }}</label>
	                            <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" required>

	                            @if ($errors->has('lastname'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('lastname') }}</strong>
	                                </span>
	                            @endif
	                        </div>
                        </div>

                        <div class="form-group row col-md-12">
                        	<div class="col-md-6">
	                            <label for="email" class="">{{ __('E-Mail Address') }}</label>
	                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}">

	                            @if ($errors->has('email'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('email') }}</strong>
	                                </span>
	                            @endif
	                        </div>

	                        <div class="col-md-6 required">
	                            <label for="phone_number">{{ __('Phone Number') }}</label>
								<input id="phone_number" type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" value="{{ old('phone_number') }}" required>

	                            @if ($errors->has('phone_number'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('phone_number') }}</strong>
	                                </span>
	                            @endif
	                        </div>
                        </div>

                        <div class="form-group row col-md-12">
                        	<div class="col-md-6 required">
	                            <label for="address">{{ __('Address') }}</label>
								<input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" required>

	                            @if ($errors->has('address'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('address') }}</strong>
	                                </span>
	                            @endif
	                        </div>

	                        <div class="col-md-6 required">
	                            <label for="number_of_years">{{ __('Number Of Years') }}</label>
								<input id="number_of_years" type="number" class="form-control{{ $errors->has('number_of_years') ? ' is-invalid' : '' }}" name="number_of_years" value="{{ old('number_of_years') }}" required>

	                            @if ($errors->has('number_of_years'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('number_of_years') }}</strong>
	                                </span>
	                            @endif
	                        </div>
                        </div>

                        <div class="form-group row col-md-12">
                        	<div class="col-md-6 required">
	                            <label for="image_url">{{ __('Image (Picture)') }}</label>
								<input id="image_url" type="file" class="form-control{{ $errors->has('image_url') ? ' is-invalid' : '' }}" name="image_url" value="{{ old('image_url') }}" required>

	                            @if ($errors->has('image_url'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('image_url') }}</strong>
	                                </span>
	                            @endif
	                        </div>

	                        <div class="col-md-6 required">
	                            <label for="bvn">{{ __('BVN (Bank Verification Number)') }}</label>
								<input id="bvn" type="number" class="form-control{{ $errors->has('bvn') ? ' is-invalid' : '' }}" name="bvn" value="{{ old('bvn') }}" required>

	                            @if ($errors->has('bvn'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong>{{ $errors->first('bvn') }}</strong>
	                                </span>
	                            @endif
	                        </div>
                        </div>
                        
                        <div class="form-group row col-md-12 required">
                            
                            <label for="specialty" class="col-md-4 col-form-label text-md-right">{{ __('Specialty') }}</label>

                            <textarea id="specialty" name="specialty" class="form-control{{ $errors->has('specialty') ? ' is-invalid' : '' }}" rows="6" placeholder="e.g White Rice, Egusi Soup, etc" required></textarea>

                            @if ($errors->has('specialty'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('specialty') }}</strong>
                                </span>
                            @endif
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Become a Mobilecook') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include("partials.footer") 
@endsection
