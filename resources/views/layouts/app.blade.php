<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Mobilecooks.com.ng is an easy to use online platform that provides users with an opportunity to directly do business with a freelance professional cook in convenience, comfort & safety.">
    <meta name="author" content="Daniel Alabuja">
    <meta name="keyword" content="chefs, cooks, hire cooks, hire chefs, food, cook, Hire mobilecooks, professional cooks, professional chefs">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <title>Mobile Cooks | @yield('title')</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('img/logo.png')}}">
  <!-- core CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('admin/css/datatables.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/responsive.css')}}" rel="stylesheet">

    @yield('css')
    <style type="text/css">
        .footer-bottom{
            bottom: 0;
            width: 100%;
        }
    </style>
</head><!--/head-->

<body class="homepage">

    <header id="header">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="padding: 1em;">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('img/newmobilecook_2_115x70.png')}}" alt="logo"></a>
                </div>
         
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        @guest
                            <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{url('/')}}"><i class="fa fa-home"></i> Home</a></li>
                            <li class="{{ Request::is('payment-bookings') ? 'active' : '' }}"><a href="{{url('payment-bookings')}}"><i class="fa fa-eye"></i> Find Chefs ASAP</a></li>

                            <li class="{{ Request::is('pricing') ? 'active' : '' }}"><a href="{{url('pricing')}}"><i class="fa fa-money"></i> Hire Chef For Employment</a></li>
                            <span><a href="{{route('login')}}" class="btn btn-default"> Get Started</a> </span> 
                        @else
                            <li class="{{ Request::is('dashboard') ? 'active' : '' }}"><a href="{{url('dashboard')}}"><i class="fa fa-home"></i> Dashboard</a></li>
                           <!--  <li class="{{ Request::is('subscription') ? 'active' : '' }}"><a href="{{url('subscription')}}"><i class="fa fa-eye"></i> Subscription</a></li> -->
                            <!-- <li class="{{ Request::is('cart') ? 'active' : '' }}"><a href="{{url('cart')}}"><i class="fa fa-shopping-cart"></i> Cart ({{\App\Cart::cartCount()}})</a></li> -->
                            <li class="{{ Request::is('order') ? 'active' : '' }}"><a href="{{url('order')}}">My Requests</a></li>
                            <li class="{{ Request::is('bookings') ? 'active' : '' }}"><a href="{{url('bookings')}}">My Bookings</a></li>
                            
                        @endguest

                        <li class="{{ Request::is('event') ? 'active' : '' }}"><a href="{{url('event')}}"><i class="fa fa-calendar"></i> Plan Your Events</a></li>
                        @guest
                        @else
                        <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->firstname }} <i class="fa fa-btn fa-caret-down"></i></a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ url('profile') }}"><i class="fa fa-btn fa-user"></i> Profile</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('update')}}"><i class="fa fa-btn fa-edit"></i> Change password</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                            <i class="fa fa-power-off"></i> Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    
    </header><!--/header-->

    @yield('content')


    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/jquery.prettyPhoto.js')}}"></script>
    <script src="{{asset('js/jquery.isotope.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{URL::asset('admin/js/datatables.min.js')}}"></script>
    <script src="{{URL::asset('admin/js/datatables.script.js')}}"></script>
    <script src="{{asset('js/wow.min.js')}}"></script>
    <script src="{{asset('js/custom.js')}}"></script>
    @yield('js')
</body>
</html>