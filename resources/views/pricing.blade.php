@extends('layouts.app')

@section('content')

@section('title')
Pricing
@endsection

	@section('css')
	
    <link href="{{asset('css/pricing.css')}}" rel="stylesheet">
    @stop

	<div class="container no-search-available" id="search">
		<div id="generic_price_table">   
			<section>
        		<div class="container">
            		<div class="row">
                		<div class="col-md-12">
                    		<!--PRICE HEADING START-->
                    		<div class="price-heading clearfix" style="padding-top: 15px;">
                    			<h3>Full Term Engagement Service (Outsourcing)</h3>
                    		</div>

                            <div class="col-md-11 col-md-offset-1">
                                <p style="font-weight: 600; font-size: 1.09em; text-align: justify; padding-bottom: 0.4rem;">These plans offers  users  sourcing and matching of professional chefs to them within 24 to 72 hours, while mobilecooks.com.ng handles chefs salaries monthly. Replacement shall be granted as long Monthly dues are paid to Mobilecooks.com.ng.</p>
                            </div>
                    		<!--//PRICE HEADING END-->
                		</div>
            		</div>
        		</div>
        		<div class="container">
            
            		<!--BLOCK ROW START-->
            		<div class="row">
                		<div class="col-md-4">
                			<!--PRICE CONTENT START-->
                    		<div class="generic_content clearfix">
                        		<!--HEAD PRICE DETAIL START-->
                        		<div class="generic_head_price clearfix">
                            		<!--HEAD CONTENT START-->
                            		<div class="generic_head_content clearfix">
                            			<!--HEAD START-->
                                		<div class="head_bg"></div>
                                		<div class="head">
                                    		<span>Monday To Friday</span>
                                		</div>
                                		<!--//HEAD END-->
                            		</div>
                            		<!--//HEAD CONTENT END-->
                            		<!--PRICE START-->
                            		<div class="generic_price_tag clearfix">	
                                		<span class="price">
                                    		<!-- <span class="sign">&#8358;</span>
                                    		<span class="currency">10,000</span>
                                    		<span class="month"><small>Service charge</small></span> -->
                                            <span class="month"><small>Call <b style="color: #c41c3d;">08135523685</b> for more information</small></span>
                                		</span>
                            		</div>
                            		<!--//PRICE END-->
                        		</div>                            
                        		<!--//HEAD PRICE DETAIL END-->
                        
                        		<!--FEATURE LIST START-->
                        		<div class="generic_feature_list">
                        			<ul>
                            			<!-- <li><span>&#8358; 45, 000 To &#8358; 80, 000</span> (Salary Negotiable)</li> -->
                                        <li><span>Salary is Negotiable</span></li>
                                		<li><span>9 hours for daily work</span> (Max)</li>
                                		<li><span>Pays Salary monthly via Mobilecook</span></li>
		                                	<li><span>Service charge is required Upfront</span></li>
                            		</ul>
                        		</div>
                        		<!--//FEATURE LIST END-->
                        
                        		<!--BUTTON START-->
                        		<!-- <div class="generic_price_btn clearfix">
                        			<form action="{{ url('subscription/pricing') }}" method="POST">
					                    @csrf
					                    <input type="hidden" name="package" value="Monday To Friday">
					                    <input type="hidden" name="price" value="10000">
                        				<button class="btn btn-primary">
					                        <span class="btn__text">
					                            Subscribe Now
					                        </span>
					                    </button>
					                </form>
                        		</div> -->
                        		<!--//BUTTON END-->
                    		</div>
                    		<!--//PRICE CONTENT END-->
                		</div>
                
                		<div class="col-md-4">
                			<!--PRICE CONTENT START-->
                    		<div class="generic_content active clearfix">
                        		<!--HEAD PRICE DETAIL START-->
                        		<div class="generic_head_price clearfix">
                            		<!--HEAD CONTENT START-->
                            		<div class="generic_head_content clearfix">
                            			<!--HEAD START-->
                                		<div class="head_bg"></div>
                                		<div class="head">
                                    		<span>Monday To Saturday</span>
                                		</div>
                                		<!--//HEAD END-->
                            		</div>
                            		<!--//HEAD CONTENT END-->
                            
                            		<!--PRICE START-->
                            		<div class="generic_price_tag clearfix">	
                                		<span class="price">
                                    		<!-- <span class="sign">&#8358;</span>
                                    		<span class="currency">15,000</span>
                                    		<span class="month"><small>Service charge</small></span> -->
                                            <span class="month"><small>Call <b style="color: #c41c3d;">08135523685</b> for more information</small></span>
                                		</span>
                            		</div>
                            		<!--//PRICE END-->
                        		</div>                            
                        		<!--//HEAD PRICE DETAIL END-->
                        
                        		<!--FEATURE LIST START-->
                        		<div class="generic_feature_list">
                        			<ul>
                            			<!-- <li><span>&#8358; 65, 000 To &#8358; 120, 000</span> (Salary Negotiable)</li> -->
                                        <li><span>Salary is Negotiable</span></li>
                                		<li><span>9 hours for daily work</span> (Max)</li>
                                		<li><span>Pays Salary monthly via Mobilecook</span></li>
		                                	<li><span>Service charge is required Upfront</span></li>
                            		</ul>
                        		</div>
                        		<!--//FEATURE LIST END-->
                        
                        		<!--BUTTON START-->
                        		<!-- <div class="generic_price_btn clearfix">
                        			<form action="{{ url('subscription/pricing') }}" method="POST">
					                    @csrf
					                    <input type="hidden" name="package" value="Monday To Saturday">
					                    <input type="hidden" name="price" value="15000">
                        				<button class="btn btn-primary">
					                        <span class="btn__text">
					                            Subscribe Now
					                        </span>
					                    </button>
					                </form>
                        		</div> -->
                        		<!--//BUTTON END-->
                    		</div>
                    		<!--//PRICE CONTENT END-->
                		</div>

                		<div class="col-md-4">
                			<!--PRICE CONTENT START-->
                    		<div class="generic_content clearfix">
                        		<!--HEAD PRICE DETAIL START-->
                        		<div class="generic_head_price clearfix">
                            		<!--HEAD CONTENT START-->
                            		<div class="generic_head_content clearfix">
                            			<!--HEAD START-->
                                		<div class="head_bg"></div>
                                		<div class="head">
                                    		<span>Monday To Sunday</span>
                                		</div>
                                		<!--//HEAD END-->
                            		</div>
                            		<!--//HEAD CONTENT END-->
                            
                            		<!--PRICE START-->
                            		<div class="generic_price_tag clearfix">	
                                		<span class="price">
                                    		<!-- <span class="sign">&#8358;</span>
                                    		<span class="currency">20,000</span>
                                    		<span class="month"><small>Service charge</small></span> -->
                                            <span class="month"><small>Call <b style="color: #c41c3d;">08135523685</b> for more information</small></span>
                               	 		</span>
                            		</div>
                            		<!--//PRICE END-->
                        		</div>                            
                        		<!--//HEAD PRICE DETAIL END-->
                        
                        		<!--FEATURE LIST START-->
                        		<div class="generic_feature_list">
                        			<ul>
                            			<!-- <li><span>&#8358; 75, 000 To &#8358; 180, 000</span> (Salary Negotiable)</li> -->
                                        <li><span>Salary is Negotiable</span></li>
                                		<li><span>9 hours for daily work</span> (Max)</li>
                                		<li><span>Pays Salary monthly via Mobilecook</span></li>
		                                	<li><span>Service charge is required Upfront</span></li>
                            		</ul>
                        		</div>
                        		<!--//FEATURE LIST END-->
                        
                        		<!--BUTTON START-->
                        		<!-- <div class="generic_price_btn clearfix">
                        			<form action="{{ url('subscription/pricing') }}" method="POST">
					                    @csrf
					                    <input type="hidden" name="package" value="Monday To Sunday">
					                    <input type="hidden" name="price" value="20000">
                        				<button class="btn btn-primary">
					                        <span class="btn__text">
					                            Subscribe Now
					                        </span>
					                    </button>
					                </form>
                        		</div> -->
                        		<!--//BUTTON END-->
                    		</div>
                    	<!--//PRICE CONTENT END-->
                		</div>
            		</div>	
            		<!--//BLOCK ROW END-->
        		</div>
    		</section>
		</div>

		<div id="generic_price_table">   
			<section>
        		<div class="container">
            		<div class="row">
                		<div class="col-md-12">
                    		<!--PRICE HEADING START-->
                    		<div class="price-heading clearfix" style="padding-top: 15px;">
                    			<h3>One-Off Hiring Engagement Service (Resourcing)</h3>
                    		</div>
                    		<!--//PRICE HEADING END-->

                            <div class="col-md-11 col-md-offset-1">
                                <p style="font-weight: 600; font-size: 1.09em; text-align: justify; padding-bottom: 0.4rem;">These plans allow Mobilecooks.com.ng handle sourcing and matching chefs to employers, users pays a one-off service charge per chef and while the employer handles salaries directly to chefs.</p>
                            </div>
                		</div>
            		</div>
        		</div>
        		<div class="container">
            		<div class="row">
            			<div class="col-md-8 col-md-offset-2">
		            		<div class="col-md-6">
		                		<!--PRICE CONTENT START-->
		                    	<div class="generic_content clearfix">
		                        	<!--HEAD PRICE DETAIL START-->
		                        	<div class="generic_head_price clearfix">
		                            	<!--HEAD CONTENT START-->
		                            	<div class="generic_head_content clearfix">
		                            		<!--HEAD START-->
		                                	<div class="head_bg"></div>
		                                	<div class="head">
		                                    	<span>A day Service (8 hours)</span>
		                                	</div>
		                                	<!--//HEAD END-->
		                            	</div>
		                            	<!--//HEAD CONTENT END-->
		                            	<!--PRICE START-->
		                            	<div class="generic_price_tag clearfix">	
		                                	<span class="price">
		                                    	<!-- <span class="sign">&#8358;</span>
		                                    	<span class="currency">5,000</span>
		                                    	<span class="month"><small>Service charge</small></span> -->
                                                <span class="month"><small>Call <b style="color: #c41c3d;">08135523685</b> for more information</small></span>
		                                	</span>
		                            	</div>
		                            	<!--//PRICE END-->
		                        	</div>                            
		                        	<!--//HEAD PRICE DETAIL END-->
		                        
		                        	<!--FEATURE LIST START-->
		                        	<div class="generic_feature_list">
		                        		<ul>
		                                	<li><span>A verified chef</span></li>
		                                	<li><span>Matched in 24 - 72 hours</span></li>
		                                	<li><span>Fit For Short Term Jobs</span></li>
		                                	<li><span>You pay chef A-day-wage directly</span></li>
		                                	<li><span>Service charge is required Upfront</span></li>
		                            	</ul>
		                        	</div>
		                        	<!--//FEATURE LIST END-->
		                        
		                        	<!--BUTTON START-->
		                        	<!-- <div class="generic_price_btn clearfix">
		                        		<form action="{{ url('subscription/pricing') }}" method="POST">
						                    @csrf
						                    <input type="hidden" name="package" value="Temporary One-Off Hiring">
						                    <input type="hidden" name="price" value="5000">
	                        				<button class="btn btn-primary">
						                        <span class="btn__text">
						                            Subscribe Now
						                        </span>
						                    </button>
						                </form>
		                        	</div> -->
		                        	<!--//BUTTON END-->
		                    	</div>
		                    	<!--//PRICE CONTENT END-->
		                	</div>
		                
		                	<div class="col-md-6">
		                		<!--PRICE CONTENT START-->
		                    	<div class="generic_content clearfix">
		                        	<!--HEAD PRICE DETAIL START-->
		                        	<div class="generic_head_price clearfix">
		                            	<!--HEAD CONTENT START-->
		                            	<div class="generic_head_content clearfix">
		                            		<!--HEAD START-->
		                                	<div class="head_bg"></div>
		                                	<div class="head">
		                                    	<span>FullTime</span>
		                                	</div>
		                                	<!--//HEAD END-->
		                            	</div>
		                            	<!--//HEAD CONTENT END-->
		                            
		                            	<!--PRICE START-->
		                            	<div class="generic_price_tag clearfix">	
		                                	<span class="price">
		                                    	<!-- <span class="sign">&#8358;</span>
		                                    	<span class="currency">35,000</span>
		                                    	<span class="month"><small>Service charge</small></span> -->
                                                <span class="month"><small>Call <b style="color: #c41c3d;">08135523685</b> for more information</small></span>
		                               	 	</span>
		                            	</div>
		                            	<!--//PRICE END-->
		                        	</div>                            
		                        	<!--//HEAD PRICE DETAIL END-->
		                        
		                        	<!--FEATURE LIST START-->
		                        	<div class="generic_feature_list">
		                        		<ul>
		                            		<li><span>A verified chef</span></li>
		                                	<li><span>Matched in 24 - 72 hours</span></li>
		                                	<li><span>Fit For Full Term Work</span></li>
		                                	<li><span>You pay chef directly</span></li>
		                                	<li><span>Access to a replacement Annually</span></li>
		                                	<li><span>Service charge is required Upfront</span></li>
		                            	</ul>
		                        	</div>
		                        	<!--//FEATURE LIST END-->
		                        
		                        	<!--BUTTON START-->
		                        	<!-- <div class="generic_price_btn clearfix">
		                        		<form action="{{ url('subscription/pricing') }}" method="POST">
						                    @csrf
						                    <input type="hidden" name="package" value="Full Term One-Off Hiring">
						                    <input type="hidden" name="price" value="35000">
	                        				<button class="btn btn-primary">
						                        <span class="btn__text">
						                            Subscribe Now
						                        </span>
						                    </button>
						                </form>
		                        	</div> -->
		                        	<!--//BUTTON END-->
		                    	</div>
		                    <!--//PRICE CONTENT END-->
		                	</div>
            			</div>
            		</div>
        		</div>
    		</section>
		</div>
	</div>

@include("partials.footer") 
@endsection
