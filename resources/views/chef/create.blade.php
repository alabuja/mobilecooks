@extends('chef.layouts.app')

@section('content')

@section('title')
	User's Profile
@endsection

<div class="container" id="profile">
    <div class="row justify-content-center">
        <div class="col-md-12" id="add_chef"> 
            <div class="panel">
            	@include('alerts')
            	<div class="panel-heading"><p style="font-size: 15px; font-weight: 600;">Update your profile to Start Receiving Calls and Mails From Clients For Cooking Opportunities</p></div>
                <div class="panel-body">

                    <form method="POST" action="{{url('chef/new')}}" enctype="multipart/form-data">
                        @csrf 

                        <div class="form-group row col-md-12">
                        	<div class="col-md-4 required">
	                            <label for="firstname" class="col-form-label text-md-right">{{ __('Firstname') }}</label>
	                            <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ ucwords(Auth::guard('chef')->user()->firstname) }}" required autofocus>

	                            @if ($errors->has('firstname'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('firstname') }}</strong>
	                                </span>
	                            @endif
	                        </div>

	                        <div class="col-md-4 required">
	                            <label for="lastname" class="col-form-label text-md-right">{{ __('Lastname') }}</label>
	                            <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ ucwords(Auth::guard('chef')->user()->lastname) }}" required>

	                            @if ($errors->has('lastname'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('lastname') }}</strong>
	                                </span>
	                            @endif
	                        </div>

	                        <div class="col-md-4">
	                            <label for="email" class="">{{ __('E-Mail Address') }}</label>
	                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ Auth::guard('chef')->user()->email }}" disabled>

	                            @if ($errors->has('email'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('email') }}</strong>
	                                </span>
	                            @endif
	                        </div>
                        </div>

                        <div class="form-group row col-md-12">
                        	<div class="col-md-4 required">
	                            <label for="phone_number">{{ __('Phone Number') }}</label>
								<input id="phone_number" type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" value="{{ ucwords(Auth::guard('chef')->user()->phone_number) }}" required>

	                            @if ($errors->has('phone_number'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('phone_number') }}</strong>
	                                </span>
	                            @endif
	                        </div>
	                        <div class="col-md-4 required">
	                            <label for="address">{{ __('Address') }}</label>
								<input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ ucwords(Auth::guard('chef')->user()->address) }}" required>

	                            @if ($errors->has('address'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('address') }}</strong>
	                                </span>
	                            @endif
	                        </div>
	                        <div class="col-md-4 required">
	                            <label for="number_of_years">{{ __('Number Of Years') }}</label>
								<input id="number_of_years" type="number" class="form-control{{ $errors->has('number_of_years') ? ' is-invalid' : '' }}" name="number_of_years" value="{{ ucwords(Auth::guard('chef')->user()->number_of_years) }}" required>

	                            @if ($errors->has('number_of_years'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('number_of_years') }}</strong>
	                                </span>
	                            @endif
	                        </div>
                        </div>

                        <div class="form-group row col-md-12">

	                        <div class="col-md-4 required">
	                            <label for="date_of_birth">{{ __('Date of Birth') }}</label>
								<input id="date_of_birth" type="date" class="form-control{{ $errors->has('date_of_birth') ? ' is-invalid' : '' }}" name="date_of_birth" value="{{ ucwords(Auth::guard('chef')->user()->date_of_birth) }}" required>

	                            @if ($errors->has('date_of_birth'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('date_of_birth') }}</strong>
	                                </span>
	                            @endif
	                        </div>

                        	<div class="col-md-4 required">
	                            <label for="state_residence">{{ __('State of Residence') }}</label>
								<input id="state_residence" type="text" class="form-control{{ $errors->has('state_residence') ? ' is-invalid' : '' }}" name="state_residence" value="{{ ucwords(Auth::guard('chef')->user()->state_residence) }}" required>

	                            @if ($errors->has('state_residence'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('state_residence') }}</strong>
	                                </span>
	                            @endif
	                        </div>

	                        <div class="col-md-4 required">
	                            <label for="state_origin">{{ __('State of Origin') }}</label>
								<input id="state_origin" type="text" class="form-control{{ $errors->has('state_origin') ? ' is-invalid' : '' }}" name="state_origin" value="{{ ucwords(Auth::guard('chef')->user()->state_origin) }}" required>

	                            @if ($errors->has('state_origin'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('state_origin') }}</strong>
	                                </span>
	                            @endif
	                        </div>

                        </div>

                        <div class="form-group row col-md-12">

                        	<div class="col-md-6 required">
	                            <label for="nationality">{{ __('Nationality') }}</label>
								<input id="nationality" type="text" class="form-control{{ $errors->has('nationality') ? ' is-invalid' : '' }}" name="nationality" value="{{ ucwords(Auth::guard('chef')->user()->nationality) }}" required>

	                            @if ($errors->has('nationality'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('nationality') }}</strong>
	                                </span>
	                            @endif
	                        </div>

	                        <!-- <div class="col-md-4 required">
	                            <label for="bvn">{{ __('BVN (Bank Verification Number)') }}</label>
								<input id="bvn" class="form-control{{ $errors->has('bvn') ? ' is-invalid' : '' }}" name="bvn" value="{{ ucwords(Auth::guard('chef')->user()->bvn) }}" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" type = "number" maxlength = "11" required>

	                            @if ($errors->has('bvn'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('bvn') }}</strong>
	                                </span>
	                            @endif
	                        </div> -->
                        	 
                        	<div class="col-md-6 required">
	                            <label for="guarantor1_name">{{ __('Guarantor\'s Name') }}</label>
								<input id="guarantor1_name" type="text" class="form-control{{ $errors->has('guarantor1_name') ? ' is-invalid' : '' }}" name="guarantor1_name" value="{{ ucwords(Auth::guard('chef')->user()->guarantor1_name) }}" required>

	                            @if ($errors->has('guarantor1_name'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('guarantor1_name') }}</strong>
	                                </span>
	                            @endif
	                        </div>
                        </div>

                        <div class="form-group row col-md-12">
                        	
	                        <div class="col-md-4 required">
	                            <label for="guarantor1_phone_number">{{ __('Guarantor\'s Phone Number') }}</label>
								<input id="guarantor1_phone_number" type="number" class="form-control{{ $errors->has('guarantor1_phone_number') ? ' is-invalid' : '' }}" name="guarantor1_phone_number" value="{{ ucwords(Auth::guard('chef')->user()->guarantor1_phone_number) }}" required>

	                            @if ($errors->has('guarantor1_phone_number'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('guarantor1_phone_number') }}</strong>
	                                </span>
	                            @endif
	                        </div>

	                        <div class="col-md-4 required">
	                            <label for="guarantor1_address">{{ __('Guarantor\'s Address') }}</label>
								<input id="guarantor1_address" type="text" class="form-control{{ $errors->has('guarantor1_address') ? ' is-invalid' : '' }}" name="guarantor1_address" value="{{ ucwords(Auth::guard('chef')->user()->guarantor1_address) }}" required>

	                            @if ($errors->has('guarantor1_address'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('guarantor1_address') }}</strong>
	                                </span>
	                            @endif
	                        </div>

	                        <div class="col-md-4 required">
	                            <label for="guarantor1_occupation">{{ __('Guarantor\'s Occupation') }}</label>
								<input id="guarantor1_occupation" type="text" class="form-control{{ $errors->has('guarantor1_occupation') ? ' is-invalid' : '' }}" name="guarantor1_occupation" value="{{ ucwords(Auth::guard('chef')->user()->guarantor1_occupation) }}" required>

	                            @if ($errors->has('guarantor1_occupation'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('guarantor1_occupation') }}</strong>
	                                </span>
	                            @endif
	                        </div>
	                    </div>

                        <div class="form-group row col-md-12">

                        	<div class="col-md-4">
	                            <label for="guarantor2_name">{{ __('Guarantor\'s Name 2') }}</label>
								<input id="guarantor2_name" type="text" class="form-control{{ $errors->has('guarantor2_name') ? ' is-invalid' : '' }}" name="guarantor2_name" value="{{ ucwords(Auth::guard('chef')->user()->guarantor2_name) }}">

	                            @if ($errors->has('guarantor2_name'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('guarantor2_name') }}</strong>
	                                </span>
	                            @endif
	                        </div>

                        	<div class="col-md-4">
	                            <label for="guarantor2_phone_number">{{ __('Guarantor\'s Phone Number 2') }}</label>
								<input id="guarantor2_phone_number" type="number" class="form-control{{ $errors->has('guarantor2_phone_number') ? ' is-invalid' : '' }}" name="guarantor2_phone_number" value="{{ ucwords(Auth::guard('chef')->user()->guarantor2_phone_number) }}">

	                            @if ($errors->has('guarantor2_phone_number'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('guarantor2_phone_number') }}</strong>
	                                </span>
	                            @endif
	                        </div>

	                        <div class="col-md-4">
	                            <label for="guarantor2_address">{{ __('Guarantor\'s 2 Address') }}</label>
								<input id="guarantor2_address" type="text" class="form-control{{ $errors->has('guarantor2_address') ? ' is-invalid' : '' }}" name="guarantor2_address" value="{{ ucwords(Auth::guard('chef')->user()->guarantor2_address) }}">

	                            @if ($errors->has('guarantor2_address'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('guarantor2_address') }}</strong>
	                                </span>
	                            @endif
	                        </div>

	                        
                        </div>
                        <div class="form-group row col-md-12">
                        	<div class="col-md-4">
	                            <label for="guarantor2_occupation">{{ __('Guarantor\'s Occupation 2') }}</label>
								<input id="guarantor2_occupation" type="text" class="form-control{{ $errors->has('guarantor2_occupation') ? ' is-invalid' : '' }}" name="guarantor2_occupation" value="{{ ucwords(Auth::guard('chef')->user()->guarantor2_occupation) }}">

	                            @if ($errors->has('guarantor2_occupation'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('guarantor2_occupation') }}</strong>
	                                </span>
	                            @endif
	                        </div>

	                        <div class="col-md-4 required">
	                            <label for="client_name">{{ __('Past Employer\'s Name') }}</label>
								<input id="client_name" type="text" class="form-control{{ $errors->has('client_name') ? ' is-invalid' : '' }}" name="client_name" value="{{ ucwords(Auth::guard('chef')->user()->client_name) }}" required>

	                            @if ($errors->has('client_name'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('client_name') }}</strong>
	                                </span>
	                            @endif
	                        </div>
                        	<div class="col-md-4 required">
	                            <label for="client_phone_number">{{ __('Past Employer\'s Phone Number') }}</label>
								<input id="client_phone_number" type="number" class="form-control{{ $errors->has('client_phone_number') ? ' is-invalid' : '' }}" name="client_phone_number" value="{{ ucwords(Auth::guard('chef')->user()->client_phone_number) }}" required>

	                            @if ($errors->has('client_phone_number'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('client_phone_number') }}</strong>
	                                </span>
	                            @endif
	                        </div>
	                    </div>

	                    <div class="form-group row col-md-12">

	                        <div class="col-md-6 required">
		                        <label for="image_url">{{ __('Personal Image (Picture) 234px by 234px') }}</label>
								<input id="image_url" type="file" class="form-control{{ $errors->has('image_url') ? ' is-invalid' : '' }}" name="image" value="{{ old('image_url') }}" required>

		                        @if ($errors->has('image_url'))
		                            <span class="invalid-feedback" role="alert">
		                                <strong class="alert alert-danger">{{ $errors->first('image_url') }}</strong>
		                            </span>
		                        @endif
		                    </div>

	                        <div class="col-md-6 required">
	                            <label for="dish_type">{{ __('Dish Type') }}</label>
	                            <select class="form-control" name="dish_type" required>
	                            	<option value="Domestic Meals">Domestic Meals</option>
	                            	<option value="Intercontinental Meals">Intercontinental Meals</option>
	                            	<option value="Both (Intercontinental & Domestic Meals)">Both</option>
	                            </select>
	                            @if ($errors->has('dish_type'))
	                                <span class="invalid-feedback" role="alert">
	                                    <strong class="alert alert-danger">{{ $errors->first('dish_type') }}</strong>
	                                </span>
	                            @endif
	                        </div>
                        </div>
	                        
                        <div class="form-group row col-md-12 required">
                            
                            <label for="specialty" class="col-md-4 col-form-label text-md-right">{{ __('Specialty e.g Rice, Egusi, Indian Chimps etc') }}</label>

                            <textarea id="specialty" name="specialty" class="form-control{{ $errors->has('specialty') ? ' is-invalid' : '' }}" rows="6" placeholder="e.g White Rice, Egusi Soup, Indian Chimps etc" required> {{ ucwords(Auth::guard('chef')->user()->specialty) }}</textarea>

                            @if ($errors->has('specialty'))
                                <span class="invalid-feedback" role="alert">
                                    <strong class="alert alert-danger">{{ $errors->first('specialty') }}</strong>
                                </span>
                            @endif
                        </div>
                        <span id="add_chef_loader" style="display:none"><img src="{{ url('img/ajax-loader.gif') }}" alt=""></span>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary" id="btnBecomeCook">
                                    {{ __('Become a Mobilecook') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include("partials.footer") 
@endsection
