@extends('layouts.app')

@section('content')
@section('title')
  Register
@endsection

<div class="container" id="register">
    <div class="row justify-content-center">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel">
                <div class="panel-heading">
                    <p style="font-size: 15px; font-weight: 600;">Create Account to Start Receiving Calls and Mails From Clients For Cooking Opportunities </p>
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{ url('chef/register') }}" aria-label="{{ __('Chef Register') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-6 required">
                                <label for="firstname" class="col-form-label text-md-right">{{ __('Firstname') }}</label>

                                <input id="firstname" type="text" class="form-control{{ $errors->has('firstname') ? ' is-invalid' : '' }}" name="firstname" value="{{ old('firstname') }}" placeholder="Firstname" required autofocus>

                                @if ($errors->has('firstname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong class="alert alert-danger">{{ $errors->first('firstname') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-6 required">
                                <label for="lastname" class="col-form-label text-md-right">{{ __('Lastname') }}</label>

                                <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" placeholder="Lastname" required>

                                @if ($errors->has('lastname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong class="alert alert-danger">{{ $errors->first('lastname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 required">
                                <label for="email" class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong class="alert alert-danger">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-6 required">
                                <label for="password" class="col-form-label text-md-right">{{ __('Password - Minimum of 6 characters') }}</label>

                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password - Minimum of 6 characters" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong class="alert alert-danger">{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 required">
                                <label for="phone_number" class="col-form-label text-md-right">{{ __('Your Phone Number') }}</label>

                                <input id="phone_number" type="number" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" placeholder="Your Phone Number" required>

                                @if ($errors->has('phone_number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong class="alert alert-danger">{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-6 required">

                                <label for="image" class="col-form-label text-md-right">{{ __('Personal Image') }}</label>

                                <input id="image" type="file" class="form-control{{ $errors->has('image') ? ' is-invalid' : '' }}" name="image" value="{{ old('image') }}" required>

                                @if ($errors->has('image'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong class="alert alert-danger">{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 required">
                                <label for="state_residence" class="col-form-label text-md-right">{{ __('State of Residence') }}</label>
                                <select class="form-control" name="state_residence" required>
                                    <option value="">Select an Option</option>
                                    @foreach($states as $state)
                                        <option value="{{$state->name}}">{{$state->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('state_residence'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong class="alert alert-danger">{{ $errors->first('state_residence') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6 required">
                                <label for="state_origin" class="col-form-label text-md-right">{{ __('State of Origin') }}</label>
                                <select class="form-control" name="state_origin" required>
                                    <option value="">Select an Option</option>
                                    @foreach($states as $state)
                                        <option value="{{$state->name}}">{{$state->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('state_origin'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong class="alert alert-danger">{{ $errors->first('state_origin') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 required">
                                <label for="guarantor1_name" class="col-form-label text-md-right">{{ __('Who Knows you cook Well?') }}</label>

                                <input id="guarantor1_name" type="text" class="form-control{{ $errors->has('guarantor1_name') ? ' is-invalid' : '' }}" name="guarantor1_name" placeholder="Who Knows you cook Well" required>

                                @if ($errors->has('guarantor1_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong class="alert alert-danger">{{ $errors->first('guarantor1_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-6 required">
                                <label for="guarantor1_phone_number" class="col-form-label text-md-right">{{ __('Person’s Phone Number') }}</label>

                                <input id="guarantor1_phone_number" type="number" class="form-control{{ $errors->has('guarantor1_phone_number') ? ' is-invalid' : '' }}" name="guarantor1_phone_number" placeholder="Person’s Phone Number" required>

                                @if ($errors->has('guarantor1_phone_number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong class="alert alert-danger">{{ $errors->first('guarantor1_phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 required">
                                <label for="guarantor2_name" class="col-form-label text-md-right">{{ __('Who can guarantee you?') }}</label>

                                <input id="guarantor2_name" type="text" class="form-control{{ $errors->has('guarantor2_name') ? ' is-invalid' : '' }}" name="guarantor2_name" placeholder="Who can guarantee you?" required>

                                @if ($errors->has('guarantor2_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong class="alert alert-danger">{{ $errors->first('guarantor2_name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-6 required">
                                <label for="guarantor2_phone_number" class="col-form-label text-md-right">{{ __('Person’s Phone Number') }}</label>

                                <input id="guarantor2_phone_number" type="number" class="form-control{{ $errors->has('guarantor2_phone_number') ? ' is-invalid' : '' }}" name="guarantor2_phone_number" placeholder="Person’s Phone Number" required>

                                @if ($errors->has('guarantor2_phone_number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong class="alert alert-danger">{{ $errors->first('guarantor2_phone_number') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                    
                        <div class="form-group row required">
                            <div class="col-md-12">
                                <label for="specialty" class=" col-form-label text-md-right">{{ __('Write all food you can prepare e.g Egusi Soup, Indian Chimps etc') }}</label>

                                <textarea id="specialty" name="specialty" class="form-control{{ $errors->has('specialty') ? ' is-invalid' : '' }}" rows="6" placeholder="e.g White Rice, Egusi Soup, Indian Chimps etc" required></textarea>

                                @if ($errors->has('specialty'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong class="alert alert-danger">{{ $errors->first('specialty') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-10">
                                <div class="form-check">
                                    
                                    <label class="form-check-label" for="remember">
                                        {{ __('Confirm') }}
                                    </label>

                                    <input class="form-check-input" type="checkbox" name="confirm" id="confirm_terms" {{ old('remember') ? 'checked' : '' }}>
                                    <p>I confirm that the information I have have given is true and adhere to the <a href="{{url('terms')}}">Terms & Conditions</a> of mobilecooks.com.ng</p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary" id="registerBtn" disabled="disabled">
                                    {{ __('Become a Chef') }}
                                </button>

                                <a class="btn btn-link" href="{{ url('login') }}">
                                    {{ __('Have an Account Already?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include("partials.footer") 
@endsection
