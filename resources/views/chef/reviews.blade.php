@extends('chef.layouts.app')

@section('content')

@section('title')
	User's Review
@endsection

<div class="chef_list_detail">
    <div class="container">
      	<div class="row"> 
        	<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">         
      			<div class="box_style_2">
              		
              		<span class="detail_con_text"><h2>Specialty</h2> {!!$chef->specialty!!}</span>
              		<div id="summary_review">
                		<div id="general_rating"> {{$total_reviews}} Reviews
                  			<div class="rating"> 
			                    @for($x = 0; $x < 5; $x++)
			                  
			                    @if($x < $chef->review_avg)
			                      <i class="fa fa-star"></i>
			                    @else
			                      <i class="fa fa-star fa fa-star-o"></i>
			                    @endif
			                   
			                    @endfor
                  			</div>
                		</div>
                		<div id="rating_summary" class="row">
                  			<div class="col-md-12">
                    			<ul>
                      				<li>Expertise
                        				<div class="rating"> 
					                        @for($x = 0; $x < 5; $x++)
					                  
						                        @if($x < DB::table('reviews')->where('chef_id', $chef->id)->avg('expertise'))
						                            <i class="fa fa-star"></i>
						                        @else
						                            <i class="fa fa-star fa fa-star-o"></i>
						                        @endif
					                         
					                        @endfor 
                        				</div>
                      				</li>
                      				<li>Punctuality
                        				<div class="rating"> 
                           					@for($x = 0; $x < 5; $x++)
                  
                          						@if($x < DB::table('reviews')->where('chef_id', $chef->id)->avg('punctuality'))
                            						<i class="fa fa-star"></i>
                          						@else
                            						<i class="fa fa-star fa fa-star-o"></i>
                          						@endif
                         
                          					@endfor  
                        				</div>
                      				</li>
                      				<li>Courtesy
                        				<div class="rating"> 
                           					@for($x = 0; $x < 5; $x++)
                  
                          						@if($x < DB::table('reviews')->where('chef_id', $chef->id)->avg('courtesy'))
                            						<i class="fa fa-star"></i>
                          						@else
                            						<i class="fa fa-star fa fa-star-o"></i>
                          						@endif
                          					@endfor
                        				</div>
                      				</li>
                    			</ul>
                  			</div>
                		</div>
                		<hr class="styled">
              
                 		{{-- @if(Auth::check() and \App\Review::checkUserReview(Auth::id(),$chef->id)=='') 
            
              				<a href="#" class="btn_1 add_bottom_15" data-toggle="modal" data-target="#myReview"> Leave a review</a>
            
            			@elseif(\App\Review::checkUserReview(Auth::id(),$chef->id)!='')

              				<a href="#" class="btn_1 add_bottom_15"> Already reviewed</a>

            			@else

               				<a href="{{ URL::to('login')}}" class="btn_1 add_bottom_15"> Leave a review</a> 
            			@endif --}}

              		</div>
               		@foreach($reviews as $i => $review)
        				<div class="review_strip_single">
          					<h4>{{ \App\User::userName($review->user_id) }} </h4>
          					<p> {{$review->review_text or ''}} </p>
          					<div class="row">
            					<div class="col-md-3">
              						<div class="rating"> 
                  						@for($x = 0; $x < 5; $x++)
                  
                  							@if($x < $review->expertise)
                    							<i class="fa fa-star"></i>
                  							@else
                    							<i class="fa fa-star fa fa-star-o"></i>
                  							@endif
                  						@endfor
              						</div>
              						Expertise 
            					</div>
            					<div class="col-md-3">
              						<div class="rating"> 
               							@for($x = 0; $x < 5; $x++)
                  
                  							@if($x < $review->punctuality)
                    							<i class="fa fa-star"></i>
                  							@else
                    							<i class="fa fa-star fa fa-star-o"></i>
                  							@endif
                  						@endfor 
              						</div>
              						Punctuality 
            					</div>
            					<div class="col-md-3">
              						<div class="rating"> 
                						@for($x = 0; $x < 5; $x++)
                  
                  							@if($x < $review->courtesy)
                    							<i class="fa fa-star"></i>
                  							@else
                    							<i class="fa fa-star fa fa-star-o"></i>
                  							@endif
                 
                  						@endfor
              						</div>
              						Courtesy 
            					</div>
          					</div>
          					<!-- End row --> 
        				</div>
        				<!-- End review strip -->
        			@endforeach     

              {{$paginations->links()}}   
            	</div>
           	</div>
      	</div>
    </div>
</div>
@include("partials.footer") 
@endsection
