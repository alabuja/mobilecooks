@extends('chef.layouts.app')

@section('content')
@section('title')
  My Bookings
@endsection

    <div class="container" id="myBooking">
      	<div class="row"> 
        	<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">
        		@include("alerts")   

		        <div class="alert alert-success">The user needs their account to be verified for you to see bookings they have made to you.</div>

		        <div class="panel">
	                <div class="panel-body">
	                	<table id="example2" class="table table-bordered table-striped">
				            <thead>
				                <tr>
				                    <th>S/N</th>
				                    <th>Client Name</th>
				                    <th>Client Email</th>
				                    <th>Client Phone Number</th>
				                    <th></th>

				                </tr>
				            </thead>
				            <tbody>
					            @foreach($orders as $key => $order)
					              	<tr>
						            	<td>{{++$key}}</td>
						            	<td>{{$order->user->firstname." ".$order->user->lastname}}</td>
						            	<td>{{$order->user->email}}</td>
						            	<td>{{$order->user->phone_number}}</td>
						            	<td><a data-toggle="modal" href="#deliveryStatus" class="btn btn-success">Job Delivered </a></td>
						            	{{-- <td><form action="{{url('chef/deliver/'.$order->id)}}" method="POST">
			                            	@csrf
			                            	<input class="hidden" name="deliver" value="1">
			                            	<button class="btn btn-success" type="submit">Job Delivered</button>
			                            	</form>
			                            </td> --}}
			                            
						            </tr>

						            <!-- Modal -->
                        <div class="modal fade" id="deliveryStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Job Delivered</h4>
                                    </div>
                                    <div class="modal-body">
                                    	<p style="text-align: justify;">Has the booking been delivered? If yes, Press Ok to continue and you wont see the details of the users on your dashboard</p>
                                    	<form action="{{url('chef/deliver/'.$order->id)}}" method="POST">
			                            	@csrf
			                            	<input class="hidden" name="deliver" value="1">
	                                    <div class="modal-footer">
	                                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
	                                        <button class="btn btn-success" type="submit">Job Delivered</button>
	                                    </div>
	                                </div>
                                	</form>
                                </div>
                            </div>
                        </div>
                        <!-- modal -->
					            @endforeach
				            </tbody>
				        </table>

				        {{$paginations->links()}}
			        </div>
			    </div>
           	</div>
      	</div>
    </div>

@include("partials.footer") 
@endsection
