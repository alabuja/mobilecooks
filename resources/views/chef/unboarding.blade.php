@extends('chef.layouts.app')

@section('content')

@section('title')
	Chef Unboarding
@endsection

<!-- <section class="pricing-page"> -->
        <div class="container" id="terms">

            <div class="row">
            	<div class="col-md-11 col-md-offset-1">
            		@include('alerts')

            		<ol>
            			<li class="lead">You have to be verified on mobilecooks.com.ng before you can start getting freelance cooking jobs.</li>
            			<li class="lead">Criteria for verification are listed below:
            				<ul>
                                <li class="lead">You need to update you profile with the necessary details. <a href="{{url('chef/profile')}}" style="color: #c41c3d;">CLICK HERE</a></li>
            					<li class="lead">You must have a mobilecook Apron and headcap to serve users, employers, or clients on our platform.</li>
            					<li class="lead">The verification covers BVN verification which comes with a cost.</li>
            					<li class="lead">The total amount for both Apron, headcap is &#8358 5000 and it will be delivered to you.</li>
            					<li class="lead">All Payment should be made to <strong>Zenith Bank, Audu Daniel, 2009886900</strong> before collection is made</li>
            					<li class="lead">Send Name, Phone Number and Proof of payment to info@mobilecooks.com.ng</li>
            				</ul>
            			</li>
            			<li class="lead">Ensure you complete the verification criteria & check back to your dashboard in 24 hours time to know if you have been verified.</li>
            		</ol>
            		
            	</div>
            </div>
        </div><!--/container-->
    <!-- </section> --><!--/pricing-page-->
@include("partials.footer") 
@endsection
