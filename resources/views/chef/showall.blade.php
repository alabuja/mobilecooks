@extends('layouts.app')

@section('content')

@section('title')
	Chefs
@endsection
 	
	    <div class="container" id="chefs">
	    	<div class="row" id="hrUnderLine">
	    		<h2 style="margin-top: 20px; margin-bottom: 40px; font-size: 20px; text-transform: capitalize; text-align: center; font-weight: 700;">How- Pick-Chef-Myself-Works</h2>
	    		<div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="media-body">
                            <!-- <h3 class="media-heading"><i class="fa fa-user-plus" aria-hidden="true"></i></h3> -->
                            <h3 class="media-heading">1</h3>
                            <p>SEARCH  (search by location or food type on the kind of chef that best fit your obejctives</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="media-body">
                            <!-- <h3 class="media-heading"><i class="fa fa-check" aria-hidden="true"></i></h3> -->
                            <h3 class="media-heading">2</h3>
                            <p>SELECT(Select or cart the chef into waiting list).</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="media services-wrap wow fadeInDown">
                        <div class="media-body">
                            <!-- <h3 class="media-heading"><i class="fa fa-money" aria-hidden="true"></i></h3> -->
                            <h3 class="media-heading">3</h3>
                            <p>PAY (Pay only NGN1,500 through your active card to access whatever amount of chefs you which to get)”</p>
                        </div>
                    </div>
                </div>
	    		<hr align="center">
	    	</div>
	      	<div class="row">

		        @if(count($chefs) > 0)
		          	@foreach($chefs as $i => $chef) 
		          		<div class="col-md-6 col-sm-6 col-xs-12">  
		          			<div class="chef_list_detail">
		          
						        <div data-wow-delay="0.{{$i}}s" class="strip_list wow fadeIn animated" style="visibility: visible; animation-delay: 0s; animation-name: fadeIn;">
						            <div class="row">         
							            <div class="col-md-8 col-sm-12">
							              	<div class="desc">
								                <div class="thumb_strip"> 
									                <a href="#">

									                	<img id="myImg-{{++$i}}" src="{{$chef->image_url or ''}}" alt="Chef {{ ucwords($chef->lastname) }}" >

									                </a>  
								                </div>         
								              	<h3 style="font-size: 13px;">Chef {{ strlen($chef->lastname) > 12 ? substr(ucwords(strtolower($chef->lastname)), 0, 11) : ucwords(strtolower($chef->lastname))}} |
								              	<span class="state_residence" style="font-size: 11px;">
								              		@if(empty($chef->state_residence))
								              			Nigeria
								              		@else
								              			{{ucwords($chef->state_residence)}}
								              		@endif
								              	</span></h3>
								              	<hr>
								              	<h4>Specialty</h4>
								              	<p class="specialty">{{strlen($chef->specialty) > 25 ? substr(ucwords(strtolower($chef->specialty)), 0, 22).'...' : ucwords(strtolower($chef->specialty))}}</p>

									            <div class="rating"> 
									                @for($x = 0; $x < 5; $x++)
	                    
									                @if($x < $chef->review_avg)
									                  <i class="fa fa-star"></i>
									                @else
									                  <i class="fa fa-star fa fa-star-o"></i>
									                @endif
									                
									                @endfor
									                (<small><a href="{{URL::to('chef/'.$chef->id.'-'.$chef->slug)}}">Read {{\App\Review::getChefTotalReview($chef->id)}} reviews</a></small>)
									            </div>
							            	</div>
							            </div>
							            <div class="col-md-4 col-sm-12">
								            <div class="go_to">
								                <div>
								                	@if(Auth::check())
									                	<form >

    														<script src="https://js.paystack.co/v1/inline.js"></script>
									                		<input type="hidden" name="chef_id" id="chef_id" value="{{$chef->id}}">
									                		<input type="hidden" name="email" id="email" value="{{Auth::user()->email}}">
									                		<input type="hidden" name="amount" id="amount" value="150000">
									                		<input type="hidden" name="reference" id="reference" value="{{ Paystack::genTranxRef() }}">
            												<input type="hidden" name="key" id="key" value="{{ config('paystack.secretKey') }}">

            												<span id="payment_loader" style="display:none"><img src="{{url('img/ajax-loader.gif')}}" alt=""></span>

									                		<p><input type="button" id="paynow" class="btn_1" onclick="payWithPaystack()" value="See Contact"/></p>
									                	</form> 
									                	<p>
									                		<a href="{{url('cart/'. $chef->id)}}"><i class="fa fa-plus-square-o"></i> Add to Cart</a>
									                	</p>
								                	@else
									                	<p><a class="btn_1" href="{{url('login')}}">See Contact</a></p> 
									                	<p><a href="{{url('login')}}"><i class="fa fa-plus-square-o"></i> Add to Cart</a></p>
								                	@endif
								                	<a href="{{url('chef/'. $chef->id.'-'.$chef->slug)}}">View Review</a>
								                	
								                </div>
								            </div>
								        </div>
							        </div>
							    </div>
						    </div>
		    			</div>

					@endforeach

					<div id="myModal" class="modal">
					  <span class="close">&times;</span>
					  <img class="modal-content" id="largeChefImage">
					  <div id="caption"></div>
					</div>

					{{$chefs->links()}}
				@else
					<h5 class="not_available">No Chef Available Now!!</h5>
		        @endif
		        	
		    </div>
	    </div>
@include("partials.footer") 
@endsection
