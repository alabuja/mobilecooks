@extends('layouts.app')

@section('content')

@section('title')
  Chef's Review
@endsection

<div class="chef_list_detail">
    <div class="container">
      	<div class="row"> 
        	<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">         
      			<div class="box_style_2">
                  <p class="detail_con_text"><strong>Name:: </strong>Chef {{ucwords($chef->lastname)}}</p>
                  <p class="detail_con_text"><strong>Specialty:: </strong>{!!$chef->specialty or '' !!}</p>
              		<p class="detail_con_text"><strong>State of Residence:: </strong>{{ucwords($chef->state_residence)}}</p>
              		<div id="summary_review">
                		<div id="general_rating"> {{$total_reviews}} Reviews
                  			<div class="rating"> 
			                    @for($x = 0; $x < 5; $x++)
			                  
			                    @if($x < $chef->review_avg)
			                      <i class="fa fa-star"></i>
			                    @else
			                      <i class="fa fa-star fa fa-star-o"></i>
			                    @endif
			                   
			                    @endfor
                  			</div>
                		</div>
                		<div id="rating_summary" class="row">
                  			<div class="col-md-12">
                    			<ul>
                      				<li>Expertise
                        				<div class="rating"> 
					                        @for($x = 0; $x < 5; $x++)
					                  
						                        @if($x < DB::table('reviews')->where('chef_id', $chef->id)->avg('expertise'))
						                            <i class="fa fa-star"></i>
						                        @else
						                            <i class="fa fa-star fa fa-star-o"></i>
						                        @endif
					                         
					                        @endfor 
                        				</div>
                      				</li>
                      				<li>Punctuality
                        				<div class="rating"> 
                           					@for($x = 0; $x < 5; $x++)
                  
                          						@if($x < DB::table('reviews')->where('chef_id', $chef->id)->avg('punctuality'))
                            						<i class="fa fa-star"></i>
                          						@else
                            						<i class="fa fa-star fa fa-star-o"></i>
                          						@endif
                         
                          					@endfor  
                        				</div>
                      				</li>
                      				<li>Courtesy
                        				<div class="rating"> 
                           					@for($x = 0; $x < 5; $x++)
                  
                          						@if($x < DB::table('reviews')->where('chef_id', $chef->id)->avg('courtesy'))
                            						<i class="fa fa-star"></i>
                          						@else
                            						<i class="fa fa-star fa fa-star-o"></i>
                          						@endif
                          					@endfor
                        				</div>
                      				</li>
                    			</ul>
                  			</div>
                		</div>
                		<hr class="styled">
              
                 		@if(Auth::check() and \App\Review::checkUserReview(Auth::id(),$chef->id)=='') 
            
              				<div class="inline-add_bottom_15"><a href="#" class="btn_1 add_bottom_15" data-toggle="modal" data-target="#myReview"> Leave a review</a></div>
            
            			@elseif(\App\Review::checkUserReview(Auth::id(),$chef->id)!='')

              				<div class="inline-add_bottom_15"><a href="#" class="btn_1 add_bottom_15"> Already reviewed</a></div>

            			@else

               				<div class="inline-add_bottom_15"><a href="{{ URL::to('login')}}" class="btn_1 add_bottom_15"> Leave a review</a> </div>
            			@endif
                  @if(Auth::check())
                      <form style="display: inline-block;">
                          <script src="https://js.paystack.co/v1/inline.js"></script>
                          <input type="hidden" name="chef_id" id="chef_id" value="{{$chef->id}}">
                          <input type="hidden" name="email" id="email" value="{{Auth::user()->email}}">
                          <input type="hidden" name="amount" id="amount" value="150000">
                          <input type="hidden" name="reference" id="reference" value="{{ Paystack::genTranxRef() }}">
                          <input type="hidden" name="key" id="key" value="{{ config('paystack.secretKey') }}">
                          <span id="payment_loader" style="display:none"><img src="{{url('img/ajax-loader.gif')}}" alt=""></span>
                          <div class="inline-add_bottom_15"><input type="button" id="paynow" class="btn_1 add_bottom_15" onclick="payWithPaystack()" value="See Contact"></div>
                      </form> 
                      {{-- <div class="inline-add_bottom_15"><a href="#" class="btn_1 add_bottom_15"> Pay Now</a></div> --}}
                  @else
                        <div class="inline-add_bottom_15"><a class="btn_1 add_bottom_15" href="{{url('login')}}">See Contact</a></div> 
                  @endif

              		</div>
               		@foreach($reviews as $i => $review)
        				<div class="review_strip_single">
          					<h4>{{ \App\User::userName($review->user_id) }} </h4>
          					<p> {{$review->review_text or ''}} </p>
          					<div class="row">
            					<div class="col-md-3">
              						<div class="rating"> 
                  						@for($x = 0; $x < 5; $x++)
                  
                  							@if($x < $review->expertise)
                    							<i class="fa fa-star"></i>
                  							@else
                    							<i class="fa fa-star fa fa-star-o"></i>
                  							@endif
                  						@endfor
              						</div>
              						Expertise 
            					</div>
            					<div class="col-md-3">
              						<div class="rating"> 
               							@for($x = 0; $x < 5; $x++)
                  
                  							@if($x < $review->punctuality)
                    							<i class="fa fa-star"></i>
                  							@else
                    							<i class="fa fa-star fa fa-star-o"></i>
                  							@endif
                  						@endfor 
              						</div>
              						Punctuality 
            					</div>
            					<div class="col-md-3">
              						<div class="rating"> 
                						@for($x = 0; $x < 5; $x++)
                  
                  							@if($x < $review->courtesy)
                    							<i class="fa fa-star"></i>
                  							@else
                    							<i class="fa fa-star fa fa-star-o"></i>
                  							@endif
                 
                  						@endfor
              						</div>
              						Courtesy 
            					</div>
          					</div>
          					<!-- End row --> 
        				</div>
        				<!-- End review strip -->
        			@endforeach        
            	</div>
           	</div>
      	</div>
    </div>
</div>

<!-- Register modal -->
<div class="modal fade" id="myReview" tabindex="-1" role="dialog" aria-labelledby="review" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content modal-popup"> <a href="#" class="close-link"><i class="fa fa-times-circle-o"></i></a>
      
        <form class="popup-form" method="POST" action="{{url('chef/'.$chef->id.'-'.$chef->slug)}}">
          @csrf
        <div class="login_icon"></div>
        <input name="chef_id" id="chef_id" type="hidden" value="{{$chef->id}}">
          
        <div class="row">
          <div class="col-md-4">
            <select class="form-control form-white" name="expertise" id="expertise" required>
              <option>Expertise</option>
              <option value="1">Low</option>
              <option value="2">Sufficient</option>
              <option value="3">Good</option>
              <option value="4">Excellent</option>
              <option value="5">Super</option>
              
            </select>
          </div>

          <div class="col-md-4">
            <select class="form-control form-white"  name="punctuality" id="punctuality" required>
              <option>Punctuality</option>
              <option value="1">Low</option>
              <option value="2">Sufficient</option>
              <option value="3">Good</option>
              <option value="4">Excellent</option>
              <option value="5">Super</option>
              
            </select>
          </div>
          <div class="col-md-4">
            <select class="form-control form-white"  name="courtesy" id="courtesy" required>
              <option>Courtesy</option>
              <option value="1">Low</option>
              <option value="2">Sufficient</option>
              <option value="3">Good</option>
              <option value="4">Excellent</option>
              <option value="5">Super</option>
              
            </select>
          </div>
        </div>
        <!--End Row -->
        <textarea name="review_text" id="review_text" class="form-control form-white" style="height:100px" placeholder="Write your review"></textarea>
        
        <button type="submit" class="review_btn-submit" id="submit-review">Submit</button>
      </form>
      <div id="message-review"></div>
    </div>
  </div>
</div>
<!-- End Register modal -->


@include("partials.footer") 
@endsection
