@extends('chef.layouts.app')

@section('content')

@section('title')
Dashboard - {{ucfirst(Auth::guard('chef')->user()->firstname).' '.ucfirst(Auth::guard('chef')->user()->lastname)}}
@endsection

<div class="container" id="dashboard">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel">
                <div class="panel-body">
                	<div class="col-md-4 text-center">
		        		<div id="dashboard-grey">
		        			<h5>My Profile</h5>
		        			<a class="btn btn-primary" href="{{url('chef/profile')}}">My Profile</a>
		        		</div>
		        	</div>

		        	<div class="col-md-4 text-center">
		        		<div id="dashboard-grey">
		        			<h5>My Reviews</h5>
			        		<a class="btn btn-primary" href="{{url('chef/review')}}">My Reviews</a>
			        	</div>
		        	</div>

		        	<div class="col-md-4 text-center">
		        		<div id="dashboard-grey">
		        			<h5>My Bookings</h5>
		        			<a class="btn btn-primary" href="{{url('chef/booking')}}">My Bookings</a>
		        		</div>
		        	</div>
		        </div>
		    </div>
        </div>
    </div>
</div>

@include("partials.footer") 
@endsection


