<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if (App::environment('production')) {
    URL::forceScheme('https');
    URL::forceRootUrl('https://mobilecooks.com.ng');
}

Route::get('/', 'IndexController@index');

Auth::routes();
 

Route::group(['middleware' => ['chef.guest']], function(){
//Chefs 

    Route::get('chef/login', 'Chef\Auth\ChefLoginController@getLoginForm');
    Route::get('chef/register', 'Chef\Auth\ChefRegisterController@showRegistrationForm');
    Route::post('chef/register', 'Chef\Auth\ChefRegisterController@register');

    Route::get('chef', 'Chef\Auth\ChefLoginController@getLoginForm');
    Route::post('chef/chef_authenticate', 'Chef\Auth\ChefLoginController@authenticate');
    Route::post('chef/password/email', 'Chef\Auth\ChefForgotPasswordController@sendResetLinkEmail');
    Route::post('chef/password/reset', 'Chef\Auth\ChefResetPasswordController@reset');
    Route::get('chef/password/reset', 'Chef\Auth\ChefForgotPasswordController@showLinkRequestForm');
    Route::get('chef/password/reset/{token}', 'Chef\Auth\ChefResetPasswordController@showResetForm');
});

Route::group(['middleware' => ['admin.guest']], function(){

	// ADMIN
    Route::get('admin/login', 'Admin\Auth\AdminLoginController@getLoginForm');
    Route::get('admin', 'Admin\Auth\AdminLoginController@getLoginForm');
    Route::post('admin/authenticate', 'Admin\Auth\AdminLoginController@authenticate');
    Route::post('admin/password/email', 'Admin\Auth\AdminForgotPasswordController@sendResetLinkEmail');
  	Route::post('admin/password/reset', 'Admin\Auth\AdminResetPasswordController@reset');
    Route::get('admin/password/reset', 'Admin\Auth\AdminForgotPasswordController@showLinkRequestForm');
    Route::get('admin/password/reset/{token}', 'Admin\Auth\AdminResetPasswordController@showResetForm');
	
});

// Route::get('apply', 		'GuestController@apply');

Route::get('faq', 			                'GuestController@faq');
Route::get('terms',                         'GuestController@term');
Route::get('event',                         'EventController@index');
Route::get('investors',                     'GuestController@investors');
Route::get('support',                       'GuestController@support');
Route::post('events',                       'EventController@store');
Route::get('pricing', 			            'GuestController@pricing');
Route::get('payment', 		                'GuestController@payment');
Route::get('chefs',                         'ChefController@getAllChef');
Route::get('chef/{id}-{slug}', 			    'ChefController@chef_details');
Route::get('profile', 			            'UserController@profile');
Route::get('dashboard',                     'UserController@dashboard');
Route::get('update', 			            'UserController@changePassword');
Route::get('package', 			            'UserController@package');
Route::post('profile', 			            'UserController@Updateprofile');
Route::post('update',                       'UserController@updatepassword');
Route::get('payment/chefs',                 'UserController@myChefs');

Route::get('chef-search',                   'ChefController@chefSearch');
Route::get('book-chef',                     'UserController@book');
Route::get('order',                         'UserController@order');
Route::get('bookings',                      'UserController@bookings');
Route::get('cart/',                         'CartController@viewCart');
Route::get('checkout',                      'CartController@order_details');
Route::get('callback',                      'OrderController@verify');
Route::get('thank-you',                     'OrderController@thankyou');
Route::get('payment-bookings',              'PaymentBookingController@getBookingsPage');
Route::post('book-now',                     'PaymentBookingController@bookNow');
Route::post('payment/book-new',              'PaymentBookingController@payForBooking');
Route::post('chef/{id}-{slug}',             'ReviewController@store');
Route::get('cart/{id}',                     'CartController@addToCart');
Route::get('remove/{id}',                   'CartController@deleteCart');
Route::get('subscription',                  'PackageController@subscription');
Route::post('subscribe',                    'OrderController@store');
Route::post('paynow',                       'PayController@paynow');
Route::post('subscription/pay',             'PayController@subscription');
Route::get('{id}',                          'OrderController@remove');
Route::get('chefs-in-{location}',           'GuestController@location');
Route::get('full-details/{id}',             'UserController@getSingleChef');


Route::post('subscription',                 'BookingController@store');
Route::post('subscription/package',         'BookingController@getPayment');
Route::post('subscription/pricing',         'BookingController@getPackage');
Route::post('subscription/bookings',        'BookingController@getPaymentPage');

Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function(){

    Route::get('home', 				     'AdminController@index');
    Route::get('update', 			     'AdminController@update');
    Route::get('profile', 			     'AdminController@profile');
    Route::get('verified-chefs', 	     'AdminController@verifiedChefs');
    Route::get('verified-users', 	     'AdminController@verifiedUsers');
    Route::get('unverified-chefs', 	     'AdminController@unVerifiedChefs');
    Route::get('unverified-users',       'AdminController@unVerifiedUsers');
    Route::get('payment',                'AdminController@payment');
    Route::get('order', 	             'AdminController@order');
    Route::get('subscriptions',          'AdminController@subscriptions');
    Route::get('events',                 'AdminController@events');
    Route::get('new/chefs',              'AdminController@getNewChef');
    Route::get('payments/booking',       'AdminController@paymentBooking');

    Route::post('new/chefs',             'AdminController@addChef');
    Route::post('update', 			     'AdminController@updatePassword');
    Route::post('logout',                'Admin\Auth\AdminLoginController@getLogout');

    Route::get('delete-chef/{id}',       'AdminController@deleteChef');
    Route::get('update/chef-{id}',       'AdminController@getUpdateChef');
    Route::get('chef-{id}',              'AdminController@getChef');
    Route::post('verify/{id}/{slug}',    'AdminController@verifyChef');
    Route::post('unverify-{id}-{slug}',  'AdminController@unverifyChef');
    Route::post('verify/{id}',           'AdminController@verifyUser');
    Route::post('update/chef-{id}',      'AdminController@updateChef');


});

Route::group(['prefix' => 'chef', 'middleware' => ['chef']], function(){

    Route::get('home',                      'ChefController@index');

    Route::get('review',                    'ChefFrontController@review');
    Route::get('update',                    'ChefFrontController@update');
    Route::get('profile',                   'ChefFrontController@profile');
    Route::get('booking',                   'ChefFrontController@bookings');
    Route::get('unboarding',                'ChefFrontController@unboarding');
    Route::get('dashboard',                 'ChefFrontController@dashboard');
    Route::post('update',                   'ChefFrontController@updatePassword');
    Route::post('profile',                  'ChefFrontController@updateProfile');
    Route::post('new',                      'ChefFrontController@store');
    Route::post('deliver/{id}',              'ChefFrontController@deliver');

    Route::post('logout',                   'Chef\Auth\ChefLoginController@getLogout');
    

});